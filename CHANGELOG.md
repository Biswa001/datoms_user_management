## v0.2.0 (14-08-2020)

### Features

1. Added TableList component.

## v0.1.1 (26-06-2020)

### Bugfixes

1. Added required authentication for npm publish.

## v0.1.0 (26-06-2020)

### Features

1. Published first version with basic setup & CalendarHeatmap component.
