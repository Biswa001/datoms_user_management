## FORM_CONTROLLER REACT COMPONENT

### FUNCTIONALITY REQUIREMENT SHEET

**Objective :**
This Component shall take in a static json and render a form out of it.
The formcontroller component shall be able to display the initial values if provided.
The FormController shall also be able to take in functions to determine the behaviour of form.

**Requirements:**

-   Convert Static Json to Form
-   Initial Values if provided shall be displayed

**Only the Following functions will work in the Static Json**

-   onValuesChange
-   onFieldsChange
-   onFinish
-   onFinishFailed
-   onChange
-   onClick
-   Form Instance APIs

**The following Components have been included**

-   Form
-   Form.Item
-   Checkbox
-   Button
-   Input
-   Input.Password
-   Radio.Group
-   Radio.Button
-   Select
-   Select.Option
