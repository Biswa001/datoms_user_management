## TABLE_LIST REACT COMPONENT

### TEST CASES

The Following Props shall be used:

1. showLegend
2. horizontalScroll
3. breakPoint
4. subShowLegend
5. subBreakPoint
6. subMargin
7. tableName

```
| Case |                   PROPS                     |                                                                                OUTPUTS                                                                                                                                                                                             |
|   #  | showLegend | horizontalScroll |  breakPoint |                      Table Output [ If W > Viewport Width (VW) ]                 |                                                                              List Output [ If W < Viewport Width ]                                                                              |
|:----:|:----------:|:----------------:|:-----------:|:--------------------------------------------------------------------------------:|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
|   1  |    TRUE    |       TRUE       | INTEGER - W | Table with Column Names. if Table Width > VW, Horizontal Scroll will be Visible  |                                                                                              -----                                                                                              |
|   2  |    TRUE    |       FALSE      | INTEGER - W |                 Table with Column Names and No Horizontal Scroll                 | List with All Column Names Visible [ Except for Columns with subShowLegend = false ]. Column Name and Data shall be inline unless subBreakPoint is Specified and subBreakPoint > ViewPort width |
|   3  |    FALSE   |       TRUE       | INTEGER - W | Table with Column Names. if Table Width > VW, Horizontal Scroll will be Visible  |                                                                                              -----                                                                                              |
|   4  |    FALSE   |       FALSE      | INTEGER - W |                 Table with Column Names and No Horizontal Scroll                 |      List Without Column Names  [ Except for Columns with subShowLegend = true ]. Column Name and Data shall be inline unless subBreakPoint is Specified and subBreakPoint > ViewPort width     |
```

For Cases 3 and 4, When W < Viewport Width:
if Column Data is displayed in new line then the corresponding subMargin value of the column is to be neglected.

#### Default Values

Automatically initialised in the constructor if not passed explicitly.

-   tableName : "Table" [ Pass String / Number or both - To be shown on top if W < Viewport Width ]
-   showLegend : TRUE
-   horizontalScroll : TRUE
-   subShowLegend : TRUE
-   breakPoint : 450
-   subBreakPoint : null
-   subMargin : 0 [ Any Other +ve Number is equivalent to number of spaces preceeding the Column ]

#### Cases Not Listed

-   Any case, which hasn't been listed above, shall throw error. For Eg. if breakPoint is initialised with negative integer/decimal/non integer or showLegend/horizontalScroll have non boolean values.
