## TABLE_LIST REACT COMPONENT

### FUNCTIONALITY REQUIREMENT SHEET

Objective : <br>To Develop a React Component which behaves
like a Table if entire Table fits View Port
other wise it behaves like a List.

Requirements:

1. The Table and List shall be Responsive and the Component should be Responsive On the go.
2. Column Names can be Enabled or Disabled for Display in the List using Props.
3. Specific Column Names can be Enabled or Disabled for Display in the List using Props.
4. BreakPoint Pixel input should determine whether List will be Displayed or Table should be displayed.
5. Horizontal Scroll can be enabled or disabled in case of Table.
6. Sub Breakpoints can be used to determine if a column should stay in the same line or move to next line.
7. Between the columns, spacing can be controlled.
