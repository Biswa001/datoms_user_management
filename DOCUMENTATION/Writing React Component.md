# React Component Writing Guidelines / Checklist

-   This document mentions the guidelines for writing a React App Component for Datoms WebApp.
-   Any proposed component must pass through these criteria before it's added to the library of all components.
-   If any change is ever made to any of the components, then also it must pass through this checklist before approval.

The overall criteria are as mentioned below:

## Docs & Files of the Component

1. The use case / requirement of the component must be very clear & it shouldn't clash with any other existing components (belonging to AntD framework or Datoms), i.e. there shouldn't be more than one component for a single / similar use case.

2. The following Docs for the component must be written prior to writing the component & the docs must stick to the guidelines suggested for the same:

| Sl. No. | Document     | Guidelines                                                                                                        | Sample                                                                                                                   |
| ------- | ------------ | ----------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------ |
| 1       | FRS          | `DOCUMENTATION/Guidelines/Document Preparation Guidelines with Samples/Documents/React Component FRS.md`          | `DOCUMENTATION/Guidelines/Document Preparation Guidelines with Samples/Documents/React Component FRS.sample.md`          |
| 2       | SRS          | `DOCUMENTATION/Guidelines/Document Preparation Guidelines with Samples/Documents/React Component SRS.md`          | `DOCUMENTATION/Guidelines/Document Preparation Guidelines with Samples/Documents/React Component SRS.sample.md`          |
| 3       | Test Cases   | `DOCUMENTATION/Guidelines/Document Preparation Guidelines with Samples/Documents/React Component Test Cases.md`   | `DOCUMENTATION/Guidelines/Document Preparation Guidelines with Samples/Documents/React Component Test Cases.sample.md`   |
| 4       | Logical Flow | `DOCUMENTATION/Guidelines/Document Preparation Guidelines with Samples/Documents/React Component Logical Flow.md` | `DOCUMENTATION/Guidelines/Document Preparation Guidelines with Samples/Documents/React Component Logical Flow.sample.md` |

3. The above mentioned docs must be committed to the Git Repo inside the `DOCUMENTATION/Components/{NAME_OF_COMPONENT}/` folder in .md format.

4. The component code must be written sticking to all the code guidelines mentioned in the React Code Rules doc `DOCUMENTATION/Guidelines/Code Quality/React Code Rules.md`.

5. The component should have 4 files as mentioned below:
    - Source Code File of the component ( `src/js/Components/{NAME_OF_COMPONENT}.js` )
    - Default Configs of the component ( `src/js/Components/{NAME_OF_COMPONENT}.DefaultConfigs.js` )
    - Style File of the component ( `src/styles/Components/{NAME_OF_COMPONENT}.less` )
    - Stories of the component ( `src/stories/{NAME_OF_COMPONENT}.stories.js` )

## Writing the Component Code

1. The Component Class must include the following information in JSDoc / Docgen format:

    - description for the component
    - use cases
    - prop-types with description & default values of every props.

    The documentation in the code should be able to generate the precise documentation for the component with the above information in StoryBook Docs without any extra effort.

2. Description of the component class mentioning exact purpose of the component should be there.

3. Clear use cases of the component must be mentioned point-wise in the class description.

4. Proper Prop-types definition for all props of the component should be there.

5. The prop types must mark all the required props as "required".

6. In case a props accepts only selective values the same should be mentioned clearly.

7. In case there is an object or an array of objects then prop-type & description for each of the properties / keys of the object must be provided.

8. The description for every props should be there.

9. Default values must be provided (via React defaultProps) for all the props which are optional (not "required" or "mandatory" & the component can work just fine without the same).

10. The default props shouldn't be mentioned in the component itself in order not to make the code too much cluttered. But at the same time they should be reflected clearly in the StoryBook Doc of the Component.

11. The prop-types & default props should be defined as static props before the constructor in the component class.

12. No default values should be defined for the props which are mandatory.

13. The data required to render the component must be passed with the prop named `data` & the same name shouldn't be used for any other purpose where data is not there.

14. No default values should be passed for `data` props & it must be mandatory.

15. All the configs must be passed as separate props to the component & no data should be mixed with the configs.

16. No two different configs should be merged into a single object. This has been done in this way so that prop-types can be defined & default values can be passed for the props as necessary.

17. If there is a props controlling "how data is displayed", i.e. the table row to highlight or a search text etc., then the same must be passed as a separate props only & shouldn't mix with data / configs.

18. There should be facility to overwrite component default style via CSS-Class-Name wherever applicable.

19. If a function is passed via props then the validity of the same must be checked properly ( `typeof(propsName) === 'function'` ) before calling the function.

20. The style of the component must be included as a less file in the component itself.

21. No styling should be applied globally like body etc. Styling can only be applied for the component itself & not for anything else.

22. In CSS !important to be avoided wherever possible.

23. Whenever a component doesn't have its own state & is controlled via its props only, then Pure Component ( `React.PureComponent` ) must be used for the same.

24. If a component is not a Pure Component, `shouldComponentUpdate` should be used (as long as possible) for the same in order to prevent unnecessary re-renders of the component & thereby improving the performance of the application.

25. In case `shouldComponentUpdate` is used, the same must be controlled by another props named `optimizeWithShouldComponentUpdate`. The default value of `optimizeWithShouldComponentUpdate` should be set as `true` & the value of `optimizeWithShouldComponentUpdate` set during the 1st mounting of the component is final (i.e. it can't be changed throughout the lifecycle of the component).

26. No Errors / Warnings should be there from any of ESLint / React / Static Code Analysis Tool.

27. No variables inside the component should have the same name as that of any props of the component.

## Writing Component Stories

1. The component should have appropriate stories, each describing a single use case of the component under a different scenario. These stories should act as Unit Tests for the component each passing different possible values of various props.

2. The stories of the component must be written in the `Component Story Format (CSF)` format as described in the StoryBook official website.

3. The test data (props of the component in the stories) shouldn't include any random values & static data / config must be provided for the same. Because snapshot testing is not possible with random values.

4. There must be one story (test case) for the component only passing the required props & the component must render just fine with the same. This should always be the first story of the component & should be called as `Zero Config`. All the other stories should be mentioned after this only.

5. The component must have at least one story other than the Zero Config.

6. The component must have enough stories describing how the component is rendered with every possible values of the props.

7. There should be a story for every test case of the component mentioned in the Test Cases Sheet & vice-versa.

8. If the component has actions of any kind, e.g. onClick / onHover etc., then appropriate actions for the same must be provided & every action must be demonstrated properly with the help of consoles appearing under the "Actions" tab of the StoryBook (not via browser console or any other means).

9. Appropriate knobs must be provided for all the props of the component in the first story, i.e. `Zero Config` & the user must be able to change the props from the knobs & render the component accordingly in StoryBook.

10. The knob title should be exactly same as that of the props name.

11. All stories must be rendered properly in each of the possible screen sizes.

12. All stories must pass the accessibility checks (shouldn't show any Error / Warning).

13. The snapshots of all the stories of the component must be committed to the repo.
