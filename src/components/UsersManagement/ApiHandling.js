import { notification } from 'antd';
/**
 * 
 * @param {Object} configData Object for API Request
 * @param {String} configData.url Url of the api
 * @param {String} configData.method API request method
 * @param {Object} configData.body API request body
 * 
 * @example
 * configData={
 * 	url:"dummy_url",
 * 	method:"POST",
 * 	body:{
 * 		key: data
 * 	}
 * }
 * 
 * callFetch(configData)
 * 
 * 
 * Respone
 * {"sttaus": "success"}
 * 
 */
async function callFetch(configData){
	return new Promise((resolve, reject) => {
		fetch(process.env.REACT_APP_IOT_PLATFORM_API_BASEPATH + configData.url, {
      method: configData.method,
      headers: {
				'Content-Type': 'application/json'
			},
      credentials: 'include',
      body: configData.body !== undefined && Object.keys(configData.body).length ? configData.body : undefined
		}).then(function(Response) {
			console.log('Response -> ', Response.status);
			if (Response.status == 401) {
				let replaceUrl = 'https://accounts.datoms.io/login?rd='+window.location.href;
				window.location.replace(replaceUrl);
			}
			return Response.json();
		}).then(function(json) {
			resolve(json);
		}).catch(function(err) {
			console.error(err + ' : ' + configData.url);
			reject('Unable to load data!');
		});
  });
}

/**
 * 
 * @param {Object} configData Object for API Request
 * @param {String} configData.url Url of the api
 * @param {String} configData.method API request method
 * @param {Object} configData.body API request body
 * 
 * @example
 * configData={
 * 	url:"dummy_url",
 * 	method:"POST",
 * 	body:{
 * 		key: data
 * 	}
 * }
 * 
 * callFetch(configData)
 * 
 * 
 * Respone
 * {"sttaus": "success"}
 * 
 */
async function temperatureCallFetch(configData){
	return new Promise((resolve, reject) => {
		fetch(process.env.REACT_APP_TEMPERATURE_MONITORING_API_BASEPATH + configData.url, {
      method: configData.method,
      headers: {
				'Content-Type': 'application/json'
			},
      credentials: 'include',
      body: configData.body !== undefined && Object.keys(configData.body).length ? configData.body : undefined
		}).then(function(Response) {
			let status_code = Response.status,
				response = Response.json();
			if (status_code == 401) {
				let replaceUrl = 'https://accounts.datoms.io/login?rd='+window.location.href;
				window.location.replace(replaceUrl);
			}
			console.log('status_code -> '. status_code);
			response.status_code = status_code;
			return response;
		}).then(function(json) {
			console.log('json -> ', json);
			resolve(json);
		}).catch(function(err) {
			console.error(err + ' : ' + configData.url);
			reject('Unable to load dataa!');
		});
  });
}

/**
 * 
 * @param {String} type Message type
 * @param {String} msg Message for notification popup
 * 
 * @example
 * type="success";
 * message="Demo Message";
 * 
 * openNotification(type, message)
 * 
 */
function openNotification(type, msg){
	notification[type]({
		message: msg,
		// description: msg,
		placement: 'bottomLeft',
		className: 'alert-' + type,
	});
}

export {callFetch, temperatureCallFetch};