import React from 'react';
import { Form } from '@ant-design/compatible';
import '@ant-design/compatible/assets/index.css';
import {
	Drawer,
	Row,
	Col,
	Alert,
	notification,
	Input,
	Select,
	Tree,
	Checkbox,
	TreeSelect,
} from 'antd';
import AntButton from '../AntButton/index';
import _ from 'lodash';
import { addRole, editRole } from './UsersAPI';

const { Option } = Select;
const TreeNode = Tree.TreeNode;
const CheckboxGroup = Checkbox.Group;
const SHOW_PARENT = TreeSelect.SHOW_PARENT;

const AddUserDrawer = Form.create()(
	class AddUserDrawer extends React.Component {
		constructor(props) {
			super(props);
			this.state = {};

			// console.log('AddRoleDrawer_', this.props);
		}

		componentDidMount() {
			this.onCheck();
			if (
				this.props.edit_role &&
				this.props.selected_role_data &&
				this.props.selected_access_list
			) {
				this.fillRoleForm(
					this.props.selected_role_data,
					this.props.selected_access_list
				);
			}
		}

		componentDidUpdate(prevProps) {
			// console.log('prevprops', prevProps);
			if (
				prevProps.edit_role != this.props.edit_role &&
				this.props.selected_role_data &&
				this.props.selected_access_list
			) {
				this.fillRoleForm(
					this.props.selected_role_data,
					this.props.selected_access_list
				);
			}
		}

		checkRoleForm() {
			// console.log('checkRoleForm', this.state.master_access);
			// console.log('checkRoleForm checkedKeys', this.state.checkedKeys);
			if (
				!this.state.selected_role_name ||
				this.state.selected_role_name === null ||
				this.state.selected_role_name === undefined ||
				this.state.selected_role_name === ''
			) {
				this.openNotification('error', 'Please enter role name');
				return false;
			} else {
				return true;
			}
		}

		/**
		 * This function sets the current value of the selected role to be edited.
		 * @param  {Object} role                     Selected role.
		 */
		fillRoleForm(role, access_list = false) {
			// console.log('all_access_lists', role);
			// console.log('all_access_lists access_list', access_list);

			let checkedKeys = [],
				new_checked_keys = [];
			let master_access = false;
			if (
				role.access &&
				role.access != '*' &&
				Object.keys(role.access).length
			) {
				if (role.access.indexOf(this.state.master_access_string) > -1) {
					master_access = true;
				} else {
					checkedKeys = role.access;
				}
			}
			if (role.access && role.access == '*') {
				master_access = true;
			}

			new_checked_keys = checkedKeys;

			if (access_list && access_list.access_groups) {
				access_list.access_groups.map((access_grp, index) => {
					if (
						access_grp.access_list &&
						access_grp.access_list.length
					) {
						if (access_grp.access_list.length == 1) {
							if (checkedKeys.length) {
								checkedKeys.map((keys) => {
									if (keys == access_grp.access_list[0].key) {
										new_checked_keys.push(index.toString());
									}
								});
							}
						}
					}
				});
			}
			// console.log('checkedKeys1__', new_checked_keys);

			if (
				new_checked_keys != '*' &&
				this.props.location.pathname.includes('/user-management')
			) {
				if (!new_checked_keys.includes('0')) {
					new_checked_keys.push('0');
				}
			}

			this.setState(
				{
					drawRoleVisible: true,
					master_access: master_access,
					selected_created_by: role.created_by,
					selected_role_id: role.id,
					selected_role_name: role.name,
					selected_role_description: role.description,
					selected_role_access: role.access,
					checkedKeys: _.uniq(new_checked_keys),
					selected_role_master_access: master_access,
					add_role: false,
					role_id: role.id,
				},
				() => {
					// console.log('master_access_', this.state.master_access);
					if (
						this.props.location.pathname.search(
							'/customer-management/'
						) > -1
					) {
						this.props.history.push(
							this.props.platform_slug +
								'/customer-management/' +
								this.props.match.params.customer_id +
								'/applications/' +
								parseInt(this.state.app_drop) +
								'/roles/' +
								role.id +
								'/edit'
						);
					} else {
						this.props.history.push(
							this.props.platform_slug +
								this.props.user_slug +
								'/roles/' +
								role.id +
								'/edit'
						);
					}
				}
			);
		}

		/**
		 * This function call the API to add a new role.
		 */

		async handleSubmitAdd() {
			let that = this;
			let access = this.setAccessFormat();
			let valid_form = this.checkRoleForm();
			if (valid_form) {
				let data_obj = {
					client_id: that.props.client_id,
					application_id: that.state.application_id,
					name: that.state.selected_role_name,
					description: that.state.selected_role_description,
					access: access,
					acl_version: that.state.acl_version,
				};

				let response = await addRole(
					data_obj,
					that.props.client_id,
					that.props.app_drop
				);
				if (response.status === 403) {
					that.setState({
						unauthorised_access: true,
						unauthorised_access_msg: response.message,
					});
				} else if (response.status === 'success') {
					that.setState({
						unauthorised_role_access: false,
						loading: false,
						drawRoleVisible: false,
					});
					that.openNotification('success', 'Role added successfully');
					that.closeDrawer();
					that.props.fetchCustomerDetails();
					that.props.fetchRoleList();
				} else {
					that.openNotification('error', response.message);
					that.setState({
						unauthorised_access: false,
						loading: true,
						error_API: true,
						error_API_msg: response.message,
					});
				}
			}
		}

		setAccessFormat() {
			let access = [],
				access_name = '';
			if (this.state.checkedKeys && this.state.checkedKeys.length) {
				this.state.checkedKeys.map((keys) => {
					// console.log('keys__', keys);
					if (
						this.props.all_access_list &&
						this.props.all_access_list.access_groups &&
						this.props.all_access_list.access_groups.length
					) {
						if (!isNaN(keys)) {
							// console.log('access_fromat', this.props.all_access_list);
							if (
								this.props.all_access_list.access_groups[keys]
									.access_list.length == 1
							) {
								access.push(
									this.props.all_access_list.access_groups[
										keys
									].access_list[0].key
								);
							}
						} /*else {
							access.push(keys);
						}*/
					}
				});
			}
			// console.log('setAccessFormat access', access);

			if (this.state.master_access) {
				return '*';
			} else {
				return access;
			}
		}

		/**
		 * This function call the API to edit the role.
		 */
		async handleSubmitEdit() {
			let that = this;
			let access = this.setAccessFormat();
			// console.log('access1_', access);
			let valid_form = this.checkRoleForm();
			if (valid_form) {
				let data_obj = {
					client_id: that.props.client_id,
					application_id: that.state.application_id,
					roles_id: that.state.role_id,
					name: that.state.selected_role_name,
					description: that.state.selected_role_description,
					access: access,
					acl_version: that.state.acl_version,
				};

				let response = await editRole(
					data_obj,
					that.props.client_id,
					that.props.app_drop,
					that.state.role_id
				);
				if (response.status === 403) {
					that.setState({
						unauthorised_access: true,
						unauthorised_access_msg: response.message,
					});
				} else if (response.status === 'success') {
					that.setState({
						unauthorised_role_access: false,
						loading: false,
						drawRoleVisible: false,
					});
					that.openNotification(
						'success',
						'Role updated successfully'
					);
					that.props.fetchCustomerDetails();
					that.closeDrawer();
					that.props.fetchRoleList();
				} else {
					that.openNotification('error', response.message);
					that.setState({
						unauthorised_access: false,
						loading: true,
						error_API: true,
						error_API_msg: response.message,
					});
				}
			}
		}

		/**
		 * This function sets the selected keys.
		 */
		onCheck(checkedKeys, info) {
			// console.log('onCheck', checkedKeys);
			// console.log('onCheck info', info);
			let checkedKeys_temp = checkedKeys;
			if (this.props.location.pathname.includes('/user-management')) {
				if (!checkedKeys_temp) {
					checkedKeys_temp = [];
				}
				if (!checkedKeys_temp.includes('0')) {
					checkedKeys_temp.push('0');
				}
			}

			this.setState(
				{
					checkedKeys: _.uniq(checkedKeys_temp),
				},
				() => {
					// console.log('checkedKeys__', this.state.checkedKeys);
				}
			);
		}

		/**
		 * This function is called to set the selected keys.
		 */
		onSelect(selectedKeys, info) {
			// console.log('onSelect', selectedKeys);
			// console.log('onSelect info', info);
			this.setState(
				{
					selectedKeys: selectedKeys,
				},
				() => {
					// console.log('selectedKeys_', selectedKeys);
				}
			);
		}

		/**
		 * This function is called when the description or the name changes.
		 * @param  {Object} e   Event
		 * @param  {string} key role_name or role_desc.
		 */
		handleChange(e, key) {
			if (key == 'role_name') {
				this.setState({
					selected_role_name: e.target.value,
				});
			} else if (key == 'role_desc') {
				this.setState({
					selected_role_description: e.target.value,
				});
			}
		}

		/**
		 * This function toggles the master key selection.
		 * @param  {Object} e Event
		 */
		/*toggleMasterCheck(e) {
			if (e.target.checked) {
				this.setState({
					master_access: true
				});
			} else {
				this.setState({
					master_access: false
				});
			}
		}*/

		closeDeleteModal() {
			this.setState({
				visible_delete_modal: false,
			});
		}

		/**
		 * This function opens the notification in render.
		 */
		openNotification(type, msg) {
			notification[type]({
				message: msg,
				// description: msg,
				placement: 'bottomLeft',
				className: 'alert-' + type,
			});
		}

		/**
		 * This function closes the slider and sets the sets the values to default.
		 */
		closeDrawer() {
			// this.props.history.push(this.platform_slug + '/settings/roles/' + this.props.history.location.search);
			this.setState(
				{
					role_id: '',
					delete_role: false,
					selected_role_id: null,
					selected_role_name: null,
					selected_role_description: null,
					selected_role_access: {},
					master_access: false,
					checkedKeys: this.props.location.pathname.includes(
						'/user-management'
					)
						? ['0']
						: [],
					selected_role_master_access: false,
					selected_created_by: null,
					unauthorised_role_access: false,
					unauthorised_role_access_msg: '',
				},
				() => {
					// this.props.form.resetFields();
					this.props.closeAddEditDrawer();
				}
			);
		}

		/**
		 * This function render the access list in tree view
		 * @param  {Array} data
		 */
		renderTreeNodes(data) {
			return data.map((item) => {
				if (item.children) {
					return (
						<TreeNode
							title={item.title}
							key={item.key}
							dataRef={item}
						>
							{this.renderTreeNodes(item.children)}
						</TreeNode>
					);
				}
				return <TreeNode {...item} />;
			});
		}

		render() {
			// console.log('add_drawer', this.props);
			const { getFieldDecorator } = this.props.form;
			return (
				<Drawer
					title={
						this.props.add_role
							? this.props.t('add_new_role')
							: this.props.edit_role
							? this.props.t('edit_role')
							: this.props.t('add_new_role')
					}
					width={720}
					placement="right"
					className="add-edit-role-drawer"
					visible={true}
					onClose={() => this.closeDrawer()}
					maskClosable={false}
					closable={false}
					destroyOnClose={true}
					style={{
						paddingBottom: 53,
					}}
				>
					<Form autoComplete="off" layout="vertical" hideRequiredMark>
						<Row gutter={16}>
							<Col span={12} className="wid-100">
								<Form.Item
									label={this.props.t('role_name') + ' *'}
								>
									{getFieldDecorator('roleName', {
										rules: [
											{
												required: true,
												message: this.props.t(
													'please_enter_role_name'
												),
											},
										],
										initialValue: this.state
											.selected_role_name,
										onChange: (e) =>
											this.handleChange(e, 'role_name'),
									})(
										<Input
											placeholder={this.props.t(
												'please_enter_role_name'
											)}
										/>
									)}
								</Form.Item>
							</Col>
						</Row>
						<Row gutter={16}>
							<Col span={24}>
								<Form.Item label={this.props.t('description')}>
									<Input.TextArea
										onChange={(e) =>
											this.handleChange(e, 'role_desc')
										}
										defaultValue={
											this.state.selected_role_description
										}
										rows={4}
										placeholder={this.props.t(
											'please_enter_role_description'
										)}
									/>
								</Form.Item>
							</Col>
						</Row>
						<Row gutter={16}>
							<Col span={24} className="wid-100 access-tree">
								<Form.Item
									label={this.props.t('access') + ' *'}
								>
									{/*<Checkbox
										onChange={(e) => this.toggleMasterCheck(e)}
										defaultChecked={this.state.master_access}
										>{this.props.t('master_access')}</Checkbox>*/}
									<Tree
										// disabled={this.state.master_access}
										checkedKeys={this.state.checkedKeys}
										checkable
										selectable={false}
										defaultExpandAll={true}
										onCheck={(a, b) => this.onCheck(a, b)}
										onSelect={(a, b) => this.onSelect(a, b)}
									>
										{this.renderTreeNodes(
											this.props.tree_data
										)}
									</Tree>
								</Form.Item>
							</Col>
						</Row>
					</Form>
					<div
						style={{
							display: 'flex',
							padding: '5px 16px',
							float: 'right',
						}}
					>
						<AntButton
							buttons={
								this.props.add_role_form.draw_buttons.cancel
							}
							onButtonClick={() => this.closeDrawer()}
						/>
						<AntButton
							buttons={
								this.props.add_role_form.draw_buttons.submit
							}
							onButtonClick={
								this.props.add_role
									? () => this.handleSubmitAdd()
									: () => this.handleSubmitEdit()
							}
						/>
					</div>
				</Drawer>
			);
		}
	}
);

export default AddUserDrawer;