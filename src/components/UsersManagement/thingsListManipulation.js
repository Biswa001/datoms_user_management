import { callFetch } from './ApiHandling';
import _ from 'lodash';
const convert = require('convert-seconds');

/*
*  list of things subscribed by customer for an application
*  basic properties of thing : configuration, parameter key
*
*/


function getThingsAndParameterData(dataSource={}) {
    let data = {};

    // Things parameters list
    let parameterData = getParametersOfAllThings(dataSource.things);

    // Category Id
    let categoryData = getThingsCategoriesData(dataSource.things_categories);

    // Things list data
    let thingsAndParametersLatestData = getAllThingsAndParametersLatestData(dataSource.things);

    // Data Aggregation Allowed Intervals
    let thingsAggrPeriod = getThingsAggregrationPeriod(dataSource.data_aggregation_allowed_intervals);

    data['category_list_tree'] = categoryData['category_list_tree'];
    data['sub_category_object'] = categoryData['sub_category_object'];
    data['things_list'] = thingsAndParametersLatestData['things_list'];
    data['thing_name_list'] = thingsAndParametersLatestData['thing_name_list'];
    data['latest_parameter_data'] = thingsAndParametersLatestData['latest_parameter_data'];
    data['all_thing_ids'] = thingsAndParametersLatestData['all_thing_ids'];
    data['data_aggregation_allowed_intervals'] = thingsAggrPeriod['data_aggregation_allowed_intervals'];
    data['param_key_data'] = parameterData['param_key_data'];
    data['param_data'] = parameterData['param_data'];
    return(data);
}

function getThingsAggregrationPeriod(thingsAggrData=[]) {
    let data = {}, data_aggregation_allowed_intervals = [];

    if (thingsAggrData && thingsAggrData.length) {
        thingsAggrData.map((interval, index) => {
            let time_data = convert(interval);
            let time_data_string = time_data['hours'] > 0 ? time_data['hours'] + (time_data['hours'] == 1 ? ' Hour' : ' Hours') : '';
            time_data_string += time_data['minutes'] > 0 ? time_data['minutes'] + (time_data['minutes'] == 1 ? ' Minute' : ' Minutes') : '';
            time_data_string += time_data['seconds'] > 0 ? time_data['seconds'] + (time_data['seconds'] == 1 ? ' Second' : ' Seconds') : '';
            data_aggregation_allowed_intervals.push({
                name: time_data_string,
                value: interval.toString()
            });
        });
    }

    data['data_aggregation_allowed_intervals'] = data_aggregation_allowed_intervals;
    return(data);
}

function getAllThingsAndParametersLatestData(thingsListData=[]) {
    let data = {};
    let thing_ids = [], thing_list = [], thing_name_list = {}, latestParameterData = [], offliTimeoutArray = [];

    // Things list data
    thingsListData.map((thing) => {
        console.log('thing12345', thing);
        thing_ids.push(thing.id);
        thing_list.push({
            id: thing.id,
            value: thing.id,
            name: thing.name,
            offline_timeout: thing.offline_timeout,
            category: thing.category
        });
        
        // Get Parameter Latest Data
        let parameterDataKeyValueObject = {};
        thing.parameters.map((parameterData, index) => {
            if (!parameterDataKeyValueObject[parameterData.key]) {
                parameterDataKeyValueObject[parameterData.key] = '';
            }

            parameterDataKeyValueObject[parameterData.key] = parameterData.value;
        });

        latestParameterData.push({
            thing_id: thing.id,
            time: thing.last_data_received_time,
            type: 'raw',
            data: parameterDataKeyValueObject
        });

        if (!thing_name_list[thing.id]) {
            thing_name_list[thing.id] = thing.name;
        } else {
            thing_name_list[thing.id] = thing.name;
        }
    });

    data['all_thing_ids'] = thing_ids;
    data['things_list'] = thing_list;
    data['thing_name_list'] = thing_name_list;
    data['latest_parameter_data'] = latestParameterData;
    return(data);
}

function getThingsCategoriesData(thingsCategories=[]) {
    let data = {};
    let treeData = [], category_list = [], sub_cat_list_obj = {}, sub_category_object = {};

    // Category Id
    if (thingsCategories && thingsCategories.length) {
        thingsCategories.map((category) => {
            let obj1 = [];
            if (category.parent_id > 0) {
                let sub_cat_array = _.filter(thingsCategories, {parent_id: category.parent_id});
                if (sub_cat_array && sub_cat_array.length) {
                    sub_cat_array.map((sub_cat) => {
                        obj1.push({
                            'name': sub_cat.name,
                            'id': sub_cat.id
                        });
                    });
                    sub_cat_list_obj[category.parent_id] = obj1;
                }
            }
        });
        thingsCategories.map((category) => {
            if (category.parent_id == 0) {
                category_list.push({
                    id: category.id,
                    name: category.name,
                    sub_category: sub_cat_list_obj[category.id]
                });
            }
        });
    }
    if (category_list && category_list.length) {
        category_list.map((category) => {
            let obj1 = [];
            if (category.sub_category && category.sub_category.length) {
                category.sub_category.map((sub_cat) => {
                    obj1.push({
                        'title': sub_cat.name,
                        'value': sub_cat.id,
                        'key': sub_cat.id,
                    });
                });
            }
            treeData.push({
                title: category.name,
                value: '0-' +  category.id,
                key: '0-' +  category.id,
                children: obj1
            });
        });
    }
    if (category_list && category_list.length) {
        category_list.map((cat) => {
            if (cat.sub_category && cat.sub_category.length) {
                cat.sub_category.map((sub) => {
                    if (!sub_category_object[sub.id]) {
                        sub_category_object[sub.id] = sub.name;
                    } else {
                        sub_category_object[sub.id] = sub.name;
                    }
                })
            }
        });
    }

    data['category_list_tree'] = treeData;
    data['sub_category_object'] = sub_category_object;
    return(data);
}

function getParametersOfAllThings(thingsListData=[]) {
    let data = {};
    let parameters_arr = [], parameters_arr_key = [];

    // Things list ids and parameters list
    thingsListData.map((thing, ind) => {
        parameters_arr = _.concat(parameters_arr, thing.parameters);
    });

    parameters_arr = _.uniqBy(parameters_arr, 'key');
    
    if (parameters_arr && parameters_arr.length) {
        parameters_arr.map((param, indx) => {
            parameters_arr_key.push(param.key);
        });
    }
    
    data['param_key_data'] = parameters_arr_key;
    data['param_data'] = parameters_arr;
    return(data);
}

export { getThingsAndParameterData, getThingsAggregrationPeriod, getAllThingsAndParametersLatestData, getThingsCategoriesData, getParametersOfAllThings };