import React from 'react';
import { Form } from '@ant-design/compatible';
import '@ant-design/compatible/assets/index.css';
import { Drawer, Row, Col, Alert, notification, Input, Select } from 'antd';
import AntButton from '../AntButton/index';
import AntSelect from '../AntSelect/index';
import Loading from '../Loading/index';
import _ from 'lodash';
import { addUser, editUser } from './UsersAPI';
import InputPhone from '../InputPhone/index';

const { Option } = Select;

const AddUserDrawer = Form.create()(
	class AddUserDrawer extends React.Component {
		constructor(props) {
			super(props);
			this.state = {
				add_user_form: this.props.add_user_form,
				contact_id: [],
				show_add_btn: true,
				selected_user_first_name: '',
				selected_user_last_name: '',
				selected_user_phone: '',
				selected_user_email: '',
				selected_user_designation: '',
				all_selected_app: false,
				all_selected_thing: false,
				application_id_arr: props.location.pathname.includes(
					'/customer-management/'
				)
					? []
					: props.location.pathname.includes('/datoms-x/')
					? [12]
					: props.location.pathname.includes('/delivery-tracking/')
					? [20]
					: props.location.pathname.includes('/dg-monitoring')
					? [16]
					: [17],
			};

			// console.log('AddTemplateDrawer_', this.props);
		}

		componentDidMount() {
			if (!this.props.show_add_draw && this.props.selected_user_data) {
				this.fillEditForm(this.props.selected_user_data);
			}
		}

		componentDidUpdate(prevProps) {
			// console.log('prevprops', prevProps);
			if (
				prevProps.show_add_draw != this.props.show_add_draw &&
				this.props.selected_user_data
			) {
				this.fillEditForm(this.props.selected_user_data);
			}
		}

		/**
		 * This function closes the slider and and sets the values to defaults.
		 */
		onUserDrawerClose() {
			// this.props.history.push(this.props.platform_slug + '/users/' + this.props.history.location.search);
			let add_user_form = this.state.add_user_form;
			add_user_form.customer_details_form[0].initial_value = undefined;
			add_user_form.customer_details_form[1].initial_value = undefined;
			add_user_form.customer_details_form[2].initial_value = undefined;
			add_user_form.customer_details_form[3].initial_value = undefined;
			add_user_form.customer_details_form[4].initial_value = undefined;
			// console.log('closeAddEditModal');
			this.setState(
				{
					user_id: '',
					add_user: false,
					edit_user: false,
					set_status_user: false,
					add_user_form: add_user_form,
					selected_user_id: null,
					selected_user_first_name: undefined,
					selected_user_last_name: undefined,
					selected_user_email: undefined,
					selected_user_phone: undefined,
					selected_user_status: null,
					selected_user_designation: '',
					contact_id: null,
					application_id_arr: this.state.selected_application_id_arr,
					role_id: null,
					selected_user_group: [],
					unauthorised_access: false,
					unauthorised_access_msg: '',
					show_add_form: false,
					block_add_form: false,
					application_details_arr: [],
					application_access_arr: [],
					customer_list: [],
					customer_set_list: [],
					all_selected_app: false,
					all_selected_thing: false,
					show_add_btn: true,
					all_selected_industry: false,
					all_selected_set: false,
					super_admin: false,
				},
				() => {
					this.props.form.resetFields();
					this.props.onClose();
				}
			);
		}

		/*
		 * This function fills the edit form with existing data.
		 * @param  {Object} user user data.
		 */
		fillEditForm(user) {
			// console.log('table_1', user);
			// this.props.history.push(this.platform_slug + '/users/' + user.id + '/edit/' + this.props.history.location.search);
			let app_id_arr = [],
				application_access_arr = user.applications,
				customer_list = user.industries,
				super_admin = false,
				things_list = [];
			if (this.props.location.pathname.includes('/customer-management')) {
				if (user.all_role_details && user.all_role_details.length) {
					user.all_role_details.map((role) => {
						if (role.application_id) {
							app_id_arr.push(role.application_id);
						}
					});
				}
			} else {
				if (user.role_details && user.role_details.length) {
					user.role_details.map((role) => {
						if (role.application_id) {
							app_id_arr.push(role.application_id);
						}
					});
				}
			}

			if (
				(this.props.location.pathname.includes('/datoms-x') ||
					this.props.location.pathname.includes('/iot-platform')) &&
				!this.props.location.pathname.includes('/customer-management')
			) {
				if (user.all_role_details && user.all_role_details.length) {
					if (
						this.props.all_roles_list &&
						this.props.all_roles_list.length
					) {
						let found = _.find(this.props.all_roles_list, {
							id: parseInt(user.all_role_details[0].role_id),
						});
						// console.log('found1_', found);
						if (found) {
							if (
								found.access == '*' &&
								found.is_deletable == false
							) {
								super_admin = true;
								customer_list = ['*'];
								if (
									this.props.all_app_list &&
									this.props.all_app_list.length
								) {
									application_access_arr = [];
									this.props.all_app_list.map((apps) => {
										application_access_arr.push(apps.id);
									});
								}
							}
						}
					}
				}
			}

			if (user.role_details[0].role_id === 1) {
				super_admin = true;
			}

			let disabled = false;

			if (user.things) {
				things_list = user.things;
				if (user.things == '*') {
					disabled = true;
				}
			}
			let add_user_form = this.state.add_user_form;
			add_user_form.customer_details_form[0].initial_value =
				user.first_name;
			add_user_form.customer_details_form[1].initial_value =
				user.last_name;
			add_user_form.customer_details_form[2].initial_value =
				user.designation;
			add_user_form.customer_details_form[3].initial_value =
				user.email_id;
			add_user_form.customer_details_form[4].initial_value =
				user.mobile_no[0];

			// console.log('app_id_arr', app_id_arr);
			this.setState(
				{
					show_add_draw: false,
					show_add_form: true,
					drawCreateVisible: true,
					selected_user_id: user.id,
					add_user_form: add_user_form,
					all_selected_thing: disabled,
					selected_user_first_name: user.first_name,
					selected_user_last_name: user.last_name,
					selected_user_email: user.email_id,
					selected_user_phone: user.mobile_no[0],
					selected_things_arr: things_list,
					selected_user_designation: user.designation,
					application_id_arr: app_id_arr,
					role_edit_details: user.all_role_details,
					role_id: this.props.location.pathname.includes(
						'/user-management'
					)
						? user.role_details[0].role_id
						: user.all_role_details[0].role_id,
					application_details_arr: this.props.location.pathname.includes(
						'/user-management'
					)
						? JSON.parse(JSON.stringify(user.role_details))
						: JSON.parse(JSON.stringify(user.all_role_details)),
					application_access_arr: application_access_arr,
					customer_list: customer_list,
					customer_set_list: user.industry_sets,
					all_selected_industry: user.industries.includes('*')
						? true
						: false,
					all_selected_set: user.industry_sets.includes('*')
						? true
						: false,
					super_admin: super_admin,
				},
				() => {
					this.isAdmin();
					// console.log('selected_things_arr_', this.state.super_admin);
				}
			);
		}

		addNewForm() {
			this.props.form.resetFields();
			this.setState({
				show_add_form: true,
				block_add_form: false,
				show_add_btn: false,
				selected_user_first_name: '',
				selected_user_last_name: '',
				selected_user_email: '',
				selected_user_phone: '',
				selected_user_designation: '',
				application_details_arr: [],
				application_id_arr: this.props.location.pathname.includes(
					'/customer-management'
				)
					? []
					: this.state.application_id_arr,
				application_access_arr: [],
				customer_list: [],
				customer_set_list: [],
				all_selected_industry: false,
				all_selected_set: false,
			});
		}

		contact_select(cont_id, index) {
			// console.log('contact_select', cont_id);
			let that = this;
			let block = false;
			if (cont_id) {
				block = true;
			}

			this.setState(
				{
					contact_id: [cont_id],
					show_add_form: block,
					block_add_form: block,
					show_add_btn: !block,
					application_id_arr: this.props.location.pathname.includes(
						'/customer-management'
					)
						? []
						: this.state.application_id_arr,
				},
				() => {
					// console.log('contact_id_', this.state.contac_id);
					this.fillCustomerForm(cont_id);
				}
			);
		}

		fillCustomerForm(contact_id) {
			if (
				this.props.all_contact_details &&
				this.props.all_contact_details.length
			) {
				let contact_details = _.find(this.props.all_contact_details, {
					contact_id: parseInt(contact_id),
				});
				let add_user_form = this.state.add_user_form;
				if (contact_details) {
					add_user_form.customer_details_form[0].initial_value =
						contact_details.first_name;
					add_user_form.customer_details_form[1].initial_value =
						contact_details.last_name;
					add_user_form.customer_details_form[2].initial_value =
						contact_details.designation;
					add_user_form.customer_details_form[3].initial_value =
						contact_details.email;
					add_user_form.customer_details_form[4].initial_value =
						contact_details.mobile_no[0];
					this.setState({
						selected_user_first_name: contact_details.first_name,
						selected_user_last_name: contact_details.last_name,
						selected_user_email: contact_details.email,
						selected_user_phone: contact_details.mobile_no,
						selected_user_designation: contact_details.designation,
						add_user_form: add_user_form,
					});
				} else {
					add_user_form.customer_details_form[0].initial_value = '';
					add_user_form.customer_details_form[1].initial_value = '';
					add_user_form.customer_details_form[2].initial_value = '';
					add_user_form.customer_details_form[3].initial_value = '';
					add_user_form.customer_details_form[4].initial_value = '';
					this.setState({
						selected_user_first_name: '',
						selected_user_last_name: '',
						selected_user_email: '',
						selected_user_phone: '',
						selected_user_designation: '',
						add_user_form: add_user_form,
					});
				}
			}
		}

		openNotification(type, msg) {
			notification[type]({
				message: msg,
				// description: msg,
				placement: 'bottomLeft',
				className: 'alert-' + type,
			});
		}

		handleChange(e, key) {
			let add_user_form = this.state.add_user_form;

			if (key == 'first_name') {
				add_user_form.customer_details_form[0].initial_value =
					e.target.value;
				this.setState({
					selected_user_first_name: e.target.value,
					add_user_form: add_user_form,
				});
			} else if (key == 'last_name') {
				add_user_form.customer_details_form[1].initial_value =
					e.target.value;
				this.setState({
					selected_user_last_name: e.target.value,
					add_user_form: add_user_form,
				});
			} else if (key == 'email') {
				add_user_form.customer_details_form[3].initial_value =
					e.target.value;
				this.setState({
					selected_user_email: e.target.value,
					add_user_form: add_user_form,
				});
			} else if (key == 'phone_no') {
				add_user_form.customer_details_form[4].initial_value = e;
				this.setState({
					selected_user_phone: e,
					add_user_form: add_user_form,
				});
			} else if (key == 'designation') {
				add_user_form.customer_details_form[2].initial_value =
					e.target.value;
				this.setState({
					selected_user_designation: e.target.value,
					add_user_form: add_user_form,
				});
			}

			// console.log('add_user_form', add_user_form);
		}

		accessApplicationSelect(app_id_arr) {
			// console.log('app_id_arr', app_id_arr);
			let app_ids = app_id_arr,
				disabled = false;
			if (app_id_arr.includes('*')) {
				app_ids = ['*'];
				disabled = true;
			}
			this.setState(
				{
					application_access_arr: app_ids,
					all_selected_app: disabled,
				},
				() => {
					// console.log('all_selected_app_', this.state.all_selected_app);
				}
			);
		}

		thingsSelect(thing_id_arr) {
			// console.log('thing_id_arr', thing_id_arr);
			let thing_ids = thing_id_arr,
				disabled = false;
			if (thing_id_arr && thing_id_arr.includes('*')) {
				thing_ids = ['*'];
				disabled = true;
			}
			this.setState(
				{
					selected_things_arr: thing_ids,
					all_selected_thing: disabled,
				},
				() => {
					// console.log('all_selected_thing_', this.state.all_selected_thing);
				}
			);
		}

		customerSelect(cust_id_arr) {
			let cust_ids = cust_id_arr,
				disabled = false;
			if (cust_id_arr.includes('*')) {
				cust_ids = ['*'];
				disabled = true;
			}
			this.setState({
				customer_list: cust_ids,
				all_selected_industry: disabled,
			});
		}

		submitUserForm() {
			this.props.form.validateFieldsAndScroll((err, values) => {
				if (!err) {
					if (this.props.show_add_draw) {
						this.handleSubmitAdd();
					} else {
						this.handleSubmitEdit();
					}
					// console.log('Received values of form: ', values);
				}
			});
		}

		role_select(role_id, app_id) {
			console.log('role_select role_id', role_id);
			// console.log('role_select app_id', app_id);
			let arr = [],
				prev_arr_new = [],
				application_access_arr = [],
				customer_list = this.state.customer_list,
				super_admin = false;
			// application_access_arr = this.state.application_access_arr;
			if (
				(this.props.location.pathname.search('/datoms-x') > -1 ||
					this.props.location.pathname.search('/iot-platform') >
						-1) &&
				this.props.location.pathname.includes('/customer-management')
			) {
				arr.push({
					application_id: app_id,
					role_id: role_id,
					reports_to: 0,
				});

				// console.log('arr__', arr);
				let prev_arr = JSON.parse(
					JSON.stringify(this.state.application_details_arr)
				);
				// console.log('prev_arr', prev_arr);
				if (_.find(prev_arr, { application_id: app_id })) {
					// console.log('application_details_arr', _.findIndex(prev_arr,{application_id: app_id}));
					if (
						prev_arr &&
						prev_arr[
							_.findIndex(prev_arr, { application_id: app_id })
						]
					) {
						prev_arr[
							_.findIndex(prev_arr, { application_id: app_id })
						]['role_id'] = role_id;
					}
				} else {
					prev_arr = prev_arr.concat(arr);
				}

				prev_arr_new = [];
				if (
					this.state.application_id_arr &&
					this.state.application_id_arr.length
				) {
					this.state.application_id_arr.map((app_id, index) => {
						let app_dtl_arr_indx = _.findIndex(prev_arr, {
							application_id: app_id,
						});
						if (app_dtl_arr_indx > -1) {
							prev_arr_new.push(prev_arr[app_dtl_arr_indx]);
						}
					});
				}

				/*if (prev_arr_new && prev_arr_new.length) {
					prev_arr_new.map((arr) => {
						if (arr.application_id == 17) {
							application_access_arr = [1,4,6,16,18,19,20,23,24,17];
						}
					});
				}*/
			} else {
				prev_arr_new.push({
					application_id: app_id,
					role_id: role_id,
					reports_to: 0,
				});

				if (role_id != null || role_id != '') {
					if (
						this.props.all_roles_list &&
						this.props.all_roles_list.length
					) {
						let found = _.find(this.props.all_roles_list, {
							id: parseInt(role_id),
						});
						// console.log('foundd_', found);
						if (found) {
							if (
								found.access == '*' &&
								found.is_deletable == false
							) {
								super_admin = true;
								customer_list = ['*'];
								if (
									this.props.all_app_list &&
									this.props.all_app_list.length
								) {
									this.props.all_app_list.map((apps) => {
										application_access_arr.push(apps.id);
									});
								}
							} else {
								application_access_arr = this.state
									.application_access_arr;
							}
						}
					}
				}
			}

			// prev_arr = prev_arr.concat(arr);
			this.setState(
				{
					role_id: role_id,
					application_details_arr: prev_arr_new,
					super_admin: super_admin,
					application_access_arr: application_access_arr,
					customer_list: customer_list,
					// application_access_arr: application_access_arr
				},
				() => {
					this.isAdmin();
					// console.log('application_details_arr', this.state.application_details_arr);
				}
			);
		}

		reportsTo(user_id, app_id) {
			// console.log('reportsTo user_id', user_id);
			// console.log('reportsTo app_id', app_id);
			let applicationDetailsArr = this.state.application_details_arr;
			if (applicationDetailsArr.length) {
				let index = _.findIndex(applicationDetailsArr, {
					application_id: app_id,
				});
				if (index != -1) {
					applicationDetailsArr[index].reports_to =
						user_id != undefined ? user_id : 0;
				}
			}
			this.setState(
				{
					// selected_user: user_id,
					application_details_arr: applicationDetailsArr,
				},
				() => {
					// console.log('application_details_arr__', this.state.application_details_arr);
				}
			);
		}

		isAdmin() {
			if (this.state.role_id === 1) {
				this.setState({
					selected_things_arr: ['*'],
					all_selected_thing: true,
				});
			} else {
				this.setState(
					{
						selected_things_arr: this.state.selected_things_arr,
						all_selected_thing: false,
					},
					() => {
						this.thingsSelect(this.state.selected_things_arr);
					}
				);
			}
		}

		formatAddData() {
			if (
				(this.props.location.pathname.includes('/datoms-x') ||
					this.props.location.pathname.includes('/iot-platform')) &&
				this.props.location.pathname.includes('/user-management')
			) {
				console.log('role_id_', this.state.role_id);
				// console.log('formatAddData true');
				if (
					!this.state.selected_user_first_name ||
					this.state.selected_user_first_name === null ||
					this.state.selected_user_first_name === undefined ||
					this.state.selected_user_first_name === ''
				) {
					this.openNotification('error', 'Please enter first name');
					return false;
				} else if (
					!this.state.selected_user_email ||
					this.state.selected_user_email === null ||
					this.state.selected_user_email === undefined ||
					this.state.selected_user_email === ''
				) {
					this.openNotification('error', 'Please enter email id');
					return false;
				} else if (
					!this.state.role_id ||
					this.state.role_id === null ||
					this.state.role_id === undefined ||
					this.state.role_id === ''
				) {
					this.openNotification('error', 'Please select a role');
					return false;
				} else if (
					!this.state.application_access_arr ||
					(this.state.application_access_arr &&
						!(
							this.state.application_access_arr instanceof Array
						)) ||
					this.state.application_access_arr.length == 0
				) {
					this.openNotification(
						'error',
						'Please select an application'
					);
					return false;
				} else {
					return true;
				}
			} else {
				// console.log('formatAddData false');
				if (!this.state.show_add_form) {
					this.openNotification('error', 'Please select a contact');
					return false;
				} else if (
					!this.state.selected_user_first_name ||
					this.state.selected_user_first_name === null ||
					this.state.selected_user_first_name === undefined ||
					this.state.selected_user_first_name === ''
				) {
					this.openNotification('error', 'Please enter first name');
					return false;
				} else if (
					!this.state.selected_user_email ||
					this.state.selected_user_email === null ||
					this.state.selected_user_email === undefined ||
					this.state.selected_user_email === ''
				) {
					this.openNotification('error', 'Please enter email id');
					return false;
				} else if (
					!this.state.application_id_arr ||
					(this.state.application_id_arr &&
						!(this.state.application_id_arr instanceof Array)) ||
					this.state.application_id_arr.length == 0
				) {
					this.openNotification(
						'error',
						'Please select an application'
					);
					return false;
				} else if (
					!this.state.role_id ||
					this.state.role_id === null ||
					this.state.role_id === undefined ||
					this.state.role_id === ''
				) {
					this.openNotification('error', 'Please select role');
					return false;
				} else {
					return true;
				}
			}
		}

		/**
		 * This function calls the API to add a new user.
		 */
		async handleSubmitAdd(user) {
			let that = this;
			// console.log('user_del', user);
			let application_arr = [];
			let cont_id = null;
			if (this.state.contact_id != '') {
				cont_id = parseInt(this.state.contact_id);
			}

			let valid_all_field = this.formatAddData();
			if (valid_all_field) {
				let application_access = [],
					cust_list;
				if (
					that.state.application_id_arr.includes(17) &&
					that.props.location.pathname.includes(
						'/customer-management'
					)
				) {
					let found = _.find(that.props.selected_all_app_list, {
						application_id: 17,
					});
					if (found) {
						application_access = found.applications;
					}
				} else {
					application_access =
						that.state.application_access_arr &&
						that.state.application_access_arr.length
							? that.state.application_access_arr
							: undefined;
				}

				if (
					that.state.customer_list &&
					that.state.customer_list.length
				) {
					if (that.state.customer_list.includes('*')) {
						cust_list = '*';
					} else {
						cust_list = that.state.customer_list;
					}
				} else {
					cust_list = [];
				}

				let industry_sets = that.state.customer_set_list
						? that.state.customer_set_list
						: [],
					things = that.state.selected_things_arr
						? that.state.selected_things_arr
						: [];

				if (industry_sets.includes('*')) {
					industry_sets = '*';
				}

				if (things.includes('*')) {
					things = '*';
				}

				console.log('things_', things);

				that.setState({
					loading: true,
				});
				let data_obj = {
					contact_id: cont_id,
					first_name: that.state.selected_user_first_name,
					last_name: that.state.selected_user_last_name,
					email: that.state.selected_user_email,
					mobile_no: that.state.selected_user_phone,
					designation: that.state.selected_user_designation,
					application_details: that.state.application_details_arr,
					applications: application_access
						? application_access
						: [this.props.application_id],
					industries: cust_list,
					industry_sets: industry_sets,
					things: things,
				};
				// console.log('data_obj_', data_obj);

				let response = await addUser(data_obj, that.props.client_id);
				if (response.status === 403) {
					that.setState({
						loading: false,
						unauthorised_access: true,
						unauthorised_access_msg: response.message,
					});
				} else if (response.status === 'success') {
					that.setState(
						{
							loading: false,
							unauthorised_access: false,
						},
						() => {
							that.openNotification(
								'success',
								'User added successfully'
							);
							that.props.fetchUserData(
								that.props.client_id,
								that.props.app_drop
							);
							that.onUserDrawerClose();
						}
					);
				} else {
					that.openNotification('error', response.message);
					that.setState({
						unauthorised_access: false,
						loading: false,
						error_API: true,
						error_API_msg: response.message,
					});
				}
			}
		}

		/**
		 * This function calls the API to edit an existing user.
		 */
		async handleSubmitEdit(user) {
			let that = this;
			let response_status,
				user_phone = '',
				cust_list;
			// console.log('application_details_arr_', this.state.application_details_arr);
			if (Array.isArray(this.state.selected_user_phone)) {
				user_phone = this.state.selected_user_phone[0];
			} else {
				user_phone = this.state.selected_user_phone;
			}
			if (that.state.customer_list && that.state.customer_list.length) {
				if (that.state.customer_list.includes('*')) {
					cust_list = '*';
				} else {
					cust_list = that.state.customer_list;
				}
			} else {
				cust_list = [];
			}

			let valid_all_field = this.formatAddData();
			if (valid_all_field) {
				let application_access = [];
				// console.log('application_id_arr', that.state.application_id_arr);
				if (
					that.state.application_id_arr.includes(17) &&
					that.props.location.pathname.includes(
						'/customer-management'
					)
				) {
					let found = _.find(that.props.selected_all_app_list, {
						application_id: 17,
					});
					if (found) {
						application_access = found.applications;
					}
				} else {
					application_access = that.state.application_access_arr;
				}
				// console.log('application_access', application_access);
				that.setState({
					loading: true,
				});

				let industry_sets = that.state.customer_set_list
						? that.state.customer_set_list
						: [],
					things = that.state.selected_things_arr
						? that.state.selected_things_arr
						: [];

				if (industry_sets.includes('*')) {
					industry_sets = '*';
				}

				if (things.includes('*')) {
					things = '*';
				}

				console.log('things_', things);

				let data_obj = {
					contact_id: that.state.selected_user_id,
					first_name: that.state.selected_user_first_name,
					last_name: that.state.selected_user_last_name,
					email: that.state.selected_user_email,
					mobile_no: user_phone,
					designation: that.state.selected_user_designation,
					application_details: that.state.application_details_arr,
					applications: application_access
						? application_access
						: [this.props.application_id],
					industries: cust_list,
					industry_sets: industry_sets,
					things: things,
				};

				let response = await editUser(
					data_obj,
					that.props.client_id,
					that.state.selected_user_id
				);
				if (response.status === 403) {
					that.setState({
						unauthorised_access: true,
						unauthorised_access_msg: response.message,
					});
				} else if (response.status === 'success') {
					that.setState(
						{
							unauthorised_access: false,
							loading: false,
						},
						() => {
							that.openNotification(
								'success',
								'User details updated successfully'
							);
							that.props.fetchUserData(
								that.props.client_id,
								that.props.app_drop
							);
							that.onUserDrawerClose();
						}
					);
				} else {
					that.openNotification('error', response.message);
					that.setState({
						unauthorised_access: false,
						loading: false,
						error_API: true,
						error_API_msg: response.message,
					});
				}
			}
		}

		render() {
			// console.log('add_drawer', this.props);
			const { getFieldDecorator } = this.props.form;
			return (
				<Drawer
					title={
						this.props.show_add_draw != undefined
							? this.props.show_add_draw != false
								? this.props.t('add_new_user')
								: this.props.t('edit_user')
							: ''
					}
					className="add-edit-user-drawer"
					width={820}
					placement="right"
					closable={false}
					onClose={() => this.onUserDrawerClose()}
					visible={true}
					destroyOnClose={true}
					mask={true}
					maskClosable={false}
					getContainer={true}
				>
					{(() => {
						if (this.state.add_user_form) {
							let other_contact = this.other_contact,
								total_users = 0,
								app_list_arr = [],
								role_list = [],
								role_arr = [];

							if (
								this.props.location.pathname.includes(
									'/user-management'
								) &&
								(this.props.location.pathname.includes(
									'/datoms-x'
								) ||
									this.props.location.pathname.includes(
										'/iot-platform'
									))
							) {
								// console.log('checked__');
								let role_details = [];
								if (
									this.props.all_roles_list &&
									this.props.all_roles_list.length
								) {
									this.props.all_roles_list.map((roles) => {
										// console.log('rolesss', roles);
										role_details.push({
											role_id: roles.id,
											role_name: roles.name,
										});
									});
								}

								if (
									this.props.location.pathname.includes(
										'/datoms-x'
									)
								) {
									role_arr.push({
										application_id: 12,
										role_details: role_details,
									});
								} else if (
									this.props.location.pathname.includes(
										'/iot-platform'
									)
								) {
									role_arr.push({
										application_id: 17,
										role_details: role_details,
									});
								}

								if (
									this.props.all_app_list &&
									this.props.all_app_list.length
								) {
									this.props.all_app_list.map((app) => {
										app_list_arr.push({
											id: app.id,
											name: app.name,
										});
									});
								}
							} else {
								if (
									this.props.selected_all_app_list &&
									this.props.selected_all_app_list.length > 0
								) {
									this.props.selected_all_app_list.map(
										(application_detail) => {
											// console.log('application_detail_', application_detail);
											let appl_list = _.find(
												this.props.all_app_list,
												{
													id:
														application_detail.application_id,
												}
											);
											// console.log('appl_list_', appl_list);
											if (appl_list) {
												if (
													application_detail.application_id !=
													12
												) {
													app_list_arr.push({
														id: appl_list.id,
														name: appl_list.name,
													});
												}
											}

											role_arr.push({
												application_id:
													application_detail.application_id,
												role_details:
													application_detail.role_details,
											});

											role_list.push({
												application_id:
													application_detail.application_id,
												role_details:
													application_detail.role_details,
											});
										}
									);

									app_list_arr = _.uniqBy(app_list_arr, 'id');
								}
							}

							return (
								<div className="content-body">
									<Form
										autoComplete="off"
										layout="vertical"
										hideRequiredMark
									>
										{(() => {
											if (this.props.show_add_draw) {
												return (
													<Row
														gutter={16}
														className="border-bot"
													>
														<Col
															span={14}
															className="wid-100 select-contact"
														>
															<Form.Item label="">
																<span className="mar-rt-20 bold">
																	{this.props.t(
																		'contact'
																	)}
																</span>
																<AntSelect
																	className="contact-select"
																	select_value={
																		this
																			.state
																			.contact_id
																	}
																	onSelectChange={(
																		value,
																		index
																	) =>
																		this.contact_select(
																			value,
																			index
																		)
																	}
																	select_option={
																		this
																			.state
																			.add_user_form
																			.contact_select
																	}
																/>
															</Form.Item>
														</Col>
														<Col
															span={6}
															className="wid-100 select-contact"
														>
															{(() => {
																if (
																	this.state
																		.show_add_btn
																) {
																	return (
																		<span
																			className="show-form-link"
																			onClick={() =>
																				this.addNewForm()
																			}
																		>
																			{'+ ' +
																				this.props.t(
																					'add_new_user'
																				)}
																		</span>
																	);
																}
															})()}
														</Col>
													</Row>
												);
											}
										})()}
										{(() => {
											if (this.state.show_add_form) {
												// console.log('selected_user_first_name', this.state.selected_user_first_name);
												return (
													<div className="form-container mar-top-20">
														<Row gutter={16}>
															{(() => {
																if (
																	this.state
																		.add_user_form
																		.customer_details_form &&
																	this.state
																		.add_user_form
																		.customer_details_form
																		.length
																) {
																	return this.state.add_user_form.customer_details_form.map(
																		(
																			form
																		) => {
																			console.log(
																				'phone_no',
																				form
																			);
																			if (
																				form.key ===
																				'phone_no'
																			) {
																				// console.log('form.initial_value', form.initial_value);
																				// let form_value = form.initial_value.includes('+') ? form.initial_value : '+91 ' + form.initial_value;
																				return (
																					<Col
																						span={
																							12
																						}
																						className="wid-100"
																					>
																						<Form.Item
																							className="font-600"
																							label={
																								form.label
																							}
																						>
																							{getFieldDecorator(
																								form.key,
																								{
																									rules: [
																										{
																											required:
																												form.required,
																											message:
																												form.message,
																										},
																									],
																									initialValue:
																										form.initial_value,
																									onChange: (
																										e
																									) =>
																										this.handleChange(
																											e,
																											form.key
																										),
																								}
																							)(
																								<InputPhone
																									country={
																										'in'
																									}
																									keyName={
																										'phone_no'
																									}
																									value={
																										form.initial_value
																									}
																									onChange={(
																										e
																									) =>
																										this.handleChange(
																											e,
																											form.key
																										)
																									}
																									placeholder={
																										form.message
																									}
																								/>
																							)}
																						</Form.Item>
																					</Col>
																				);
																			} else {
																				return (
																					<Col
																						span={
																							12
																						}
																						className="wid-100"
																					>
																						<Form.Item
																							className="font-600"
																							label={
																								form.label
																							}
																						>
																							{getFieldDecorator(
																								form.key,
																								{
																									rules: [
																										{
																											required:
																												form.required,
																											message:
																												form.message,
																										},
																									],
																									initialValue:
																										form.initial_value,
																									onChange: (
																										e
																									) =>
																										this.handleChange(
																											e,
																											form.key
																										),
																								}
																							)(
																								<Input
																									type={
																										form.type
																											? form.type
																											: 'text'
																									}
																									disabled={
																										this
																											.state
																											.block_add_form
																											? true
																											: false
																									}
																									placeholder={
																										form.message
																									}
																								/>
																							)}
																						</Form.Item>
																					</Col>
																				);
																			}
																		}
																	);
																}
															})()}
														</Row>
														{/*<Row gutter={16}>
												<Col span={12} className="wid-100">
													<Form.Item label="Designation">
														{getFieldDecorator('designation', {
															initialValue: this.state.selected_user_designation,
															onChange: (e) => this.handleChange(e, 'designation')
														})(<Input disabled={this.state.block_add_form ? true : false} placeholder="Please enter designation" />
														)}
													</Form.Item>
												</Col>
											</Row>
											<Row gutter={16} className="border-bot mar-bot-30">
												<Col span={12} className="wid-100">
													<Form.Item label="Email *">
														{getFieldDecorator('email', {
															rules: [{ required: true, message: 'Please enter email id' }],
															initialValue: this.state.selected_user_email,
															onChange: (e) => this.handleChange(e, 'email')
														})(<Input disabled={this.state.block_add_form ? true : false} placeholder="Please enter Email id" />)}
													</Form.Item>
												</Col>
												<Col span={12} className="wid-100">
													<Form.Item label="Contact No">
														{getFieldDecorator('phone_no', {
															rules: [{ required: true, message: 'Please enter contact no.' }],
															initialValue: this.state.selected_user_phone,
															onChange: (e) => this.handleChange(e, 'phone_no')
														})(<Input type="number" disabled={this.state.block_add_form ? true : false} placeholder="Please enter contact no." />)}
													</Form.Item>
												</Col>
											</Row>*/}
														{/*(() => {
												if (((this.props.location.pathname.includes('/datoms-x') || this.props.location.pathname.includes('/iot-platform')) && this.props.location.pathname.includes('/customer-management'))) {
													return <Row gutter={16}>
														<Col span={14} className="wid-100">
															<Form.Item label="Applications *">
																{getFieldDecorator('select', {
											            rules: [{ required: true, message: 'Please select your country!' }],
											            initialValue: this.state.application_id_arr,
											            onChange: (e) => this.application_select(e)
											          })(<Select
																		mode="multiple"
																		showSearch
																		optionFilterProp="title"
																	>
																		{(() => {
																			if (app_list_arr.length > 0) {
																				return app_list_arr.map((app) => {
																					return <Option key={app.id} title={app.name} value={app.id}>{app.name}</Option>
																				});
																			}
																		})()}
																	</Select>
											          )}
															</Form.Item>
														</Col>
													</Row>
												}
											})()*/}
														{(() => {
															if (
																this.state
																	.application_id_arr &&
																this.state
																	.application_id_arr
																	.length
															) {
																return this.state.application_id_arr.map(
																	(
																		app_id
																	) => {
																		let app_details = _.find(
																			this
																				.props
																				.all_app_list,
																			{
																				id: app_id,
																			}
																		);
																		let role_det = {};
																		let role_id =
																				'',
																			user_id = undefined;
																		role_det = _.find(
																			this
																				.state
																				.application_details_arr,
																			{
																				application_id: app_id,
																			}
																		);
																		if (
																			role_det
																		) {
																			role_id =
																				role_det.role_id;
																			user_id =
																				role_det.reports_to !=
																				0
																					? role_det.reports_to
																					: undefined;
																		}

																		let user_lists = [];
																		if (
																			this
																				.props
																				.user_table_data &&
																			this
																				.props
																				.user_table_data
																				.length
																		) {
																			this.props.user_table_data.map(
																				(
																					user
																				) => {
																					// console.log('user_table_data_', user);
																					let found_app = _.find(
																						user.all_role_details,
																						{
																							application_id: app_id,
																						}
																					);
																					// console.log('found_app_', found_app);
																					if (
																						found_app
																					) {
																						if (
																							this
																								.props
																								.match
																								.params
																								.user_id
																						) {
																							if (
																								this
																									.props
																									.match
																									.params
																									.user_id !=
																								user.id
																							) {
																								user_lists.push(
																									{
																										id:
																											user.id,
																										name:
																											user.name,
																									}
																								);
																							}
																						} else {
																							user_lists.push(
																								{
																									id:
																										user.id,
																									name:
																										user.name,
																								}
																							);
																						}
																					}
																				}
																			);
																		}

																		return (
																			<Row
																				gutter={
																					16
																				}
																			>
																				<Col
																					span={
																						14
																					}
																					className="wid-100 select-role"
																				>
																					<Form.Item label="">
																						{(() => {
																							if (
																								(this.props.location.pathname.includes(
																									'/datoms-x'
																								) ||
																									this.props.location.pathname.includes(
																										'/iot-platform'
																									)) &&
																								this.props.location.pathname.includes(
																									'/customer-management'
																								)
																							) {
																								return (
																									<span className="lable-txt mar-rt-20 font-600">
																										{app_details.name +
																											' *'}
																									</span>
																								);
																							} else {
																								return (
																									<span className="lable-txt mar-rt-20 font-600">
																										{this.props.t(
																											'role'
																										) +
																											' *'}
																									</span>
																								);
																							}
																						})()}
																						{getFieldDecorator(
																							'selected_role',
																							{
																								rules: [
																									{
																										required: true,
																										message:
																											'Please select a role',
																									},
																								],
																								initialValue: role_id,
																								onChange: (
																									e
																								) =>
																									this.role_select(
																										e,
																										app_id
																									),
																							}
																						)(
																							<Select
																								showSearch
																								value={
																									role_id
																								}
																								optionFilterProp="title"
																								placeholder="Select a Role"
																								onChange={(
																									e
																								) =>
																									this.role_select(
																										e,
																										app_id
																									)
																								}
																							>
																								{(() => {
																									return role_arr.map(
																										(
																											role
																										) => {
																											if (
																												role.application_id ==
																												app_id
																											) {
																												if (
																													role
																														.role_details
																														.length >
																													0
																												) {
																													return role.role_details.map(
																														(
																															role_detail
																														) => {
																															return (
																																<Option
																																	key={
																																		role_detail.role_id
																																	}
																																	title={
																																		role_detail.role_name
																																	}
																																	value={
																																		role_detail.role_id
																																	}
																																>
																																	{
																																		role_detail.role_name
																																	}
																																</Option>
																															);
																														}
																													);
																												}
																											}
																										}
																									);
																								})()}
																							</Select>
																						)}
																					</Form.Item>
																				</Col>

																				<Col
																					span={
																						14
																					}
																					className="wid-100 select-role"
																				>
																					<Form.Item label="">
																						<span className="lable-txt mar-rt-20 font-600">
																							{this.props.t(
																								'reports_to'
																							)}
																						</span>
																						<Select
																							showSearch
																							allowClear
																							optionFilterProp="title"
																							value={
																								user_id
																							}
																							placeholder="Select User"
																							onChange={(
																								e
																							) =>
																								this.reportsTo(
																									e,
																									app_id
																								)
																							}
																						>
																							{(() => {
																								if (
																									user_lists &&
																									user_lists.length >
																										0
																								) {
																									return user_lists.map(
																										(
																											user
																										) => {
																											return (
																												<Option
																													key={
																														user.id
																													}
																													title={
																														user.name
																													}
																													value={
																														user.id
																													}
																												>
																													{
																														user.name
																													}
																												</Option>
																											);
																										}
																									);
																								}
																							})()}
																						</Select>
																					</Form.Item>
																				</Col>
																			</Row>
																		);
																	}
																);
															}
														})()}
														{(() => {
															if (
																(this.props.location.pathname.includes(
																	'/datoms-x'
																) ||
																	this.props.location.pathname.includes(
																		'/iot-platform'
																	)) &&
																!this.props.location.pathname.includes(
																	'/customer-management'
																)
															) {
																return (
																	<Row
																		gutter={
																			16
																		}
																	>
																		<Col
																			span={
																				14
																			}
																			className="wid-100"
																		>
																			<Form.Item
																				className="font-600"
																				label="Applications *"
																			>
																				<Select
																					mode="multiple"
																					showSearch
																					disabled={
																						this
																							.state
																							.super_admin
																					}
																					optionFilterProp="title"
																					value={
																						this
																							.state
																							.application_access_arr
																					}
																					placeholder="Select Applications"
																					onChange={(
																						e
																					) =>
																						this.accessApplicationSelect(
																							e
																						)
																					}
																				>
																					{(() => {
																						if (
																							app_list_arr.length >
																							0
																						) {
																							return app_list_arr.map(
																								(
																									app
																								) => {
																									return (
																										<Option
																											key={
																												app.id
																											}
																											title={
																												app.name
																											}
																											disabled={
																												this
																													.state
																													.all_selected_app
																											}
																											value={
																												app.id
																											}
																										>
																											{
																												app.name
																											}
																										</Option>
																									);
																								}
																							);
																						}
																					})()}
																				</Select>
																			</Form.Item>
																		</Col>
																	</Row>
																);
															}
														})()}
														{(() => {
															let label =
																'Things';
															if (
																this.props.location.pathname.includes(
																	'/delivery-tracking/'
																)
															) {
																label = this.props.t(
																	'loggers'
																);
															}
															return (
																<Row
																	gutter={16}
																>
																	<Col
																		span={
																			14
																		}
																		className="wid-100"
																	>
																		<Form.Item
																			className="font-600"
																			label={
																				label
																			}
																		>
																			<Select
																				mode="multiple"
																				showSearch
																				value={
																					this
																						.state
																						.selected_things_arr
																				}
																				optionFilterProp="title"
																				showArrow={
																					true
																				}
																				onChange={(
																					e
																				) =>
																					this.thingsSelect(
																						e
																					)
																				}
																			>
																				{(() => {
																					if (
																						this
																							.props
																							.things_list &&
																						this
																							.props
																							.things_list
																							.length >
																							1
																					) {
																						return (
																							<Option
																								key="*"
																								title="all"
																								disabled={
																									this
																										.state
																										.super_admin
																								}
																								value="*"
																							>
																								All
																							</Option>
																						);
																					}
																				})()}
																				{(() => {
																					if (
																						this
																							.props
																							.things_list &&
																						this
																							.props
																							.things_list
																							.length >
																							0
																					) {
																						return this.props.things_list.map(
																							(
																								things
																							) => {
																								return (
																									<Option
																										key={
																											things.id
																										}
																										title={
																											things.name
																										}
																										disabled={
																											this
																												.state
																												.all_selected_thing
																										}
																										value={
																											things.id
																										}
																									>
																										{
																											things.name
																										}
																									</Option>
																								);
																							}
																						);
																					}
																				})()}
																			</Select>
																		</Form.Item>
																	</Col>
																</Row>
															);
															/*if (!this.props.location.pathname.includes('/datoms-x') && !this.props.location.pathname.includes('/iot-platform') && this.props.location.pathname.includes('/user-management')) {
												}*/
														})()}
														{(() => {
															if (
																(this.props.location.pathname.includes(
																	'/datoms-x'
																) ||
																	this.props.location.pathname.includes(
																		'/iot-platform'
																	)) &&
																!this.props.location.pathname.includes(
																	'/customer-management'
																)
															) {
																return (
																	<Row
																		gutter={
																			16
																		}
																	>
																		<Col
																			span={
																				14
																			}
																			className="wid-100"
																		>
																			<Form.Item
																				className="font-600"
																				label="Industries"
																			>
																				<Select
																					mode="multiple"
																					showSearch
																					disabled={
																						this
																							.state
																							.super_admin
																					}
																					optionFilterProp="title"
																					value={
																						this
																							.state
																							.customer_list
																					}
																					placeholder="Select Industry"
																					onChange={(
																						e
																					) =>
																						this.customerSelect(
																							e
																						)
																					}
																				>
																					{(() => {
																						if (
																							this
																								.state
																								.customer_list_arr &&
																							this
																								.state
																								.customer_list_arr
																								.length >
																								1
																						) {
																							return (
																								<Option
																									key="*"
																									title="all"
																									disabled={
																										this
																											.state
																											.super_admin
																									}
																									value="*"
																								>
																									All
																								</Option>
																							);
																						}
																					})()}
																					{(() => {
																						if (
																							this
																								.state
																								.customer_list_arr &&
																							this
																								.state
																								.customer_list_arr
																								.length >
																								0
																						) {
																							return this.state.customer_list_arr.map(
																								(
																									cust
																								) => {
																									return (
																										<Option
																											key={
																												cust.id
																											}
																											disabled={
																												this
																													.state
																													.all_selected_industry
																											}
																											title={
																												cust.name
																											}
																											value={
																												cust.id
																											}
																										>
																											{
																												cust.name
																											}
																										</Option>
																									);
																								}
																							);
																						}
																					})()}
																				</Select>
																			</Form.Item>
																		</Col>
																	</Row>
																);
															}
														})()}
														{/*(() => {
												if ((this.props.location.pathname.includes('/datoms-x') || this.props.location.pathname.includes('/iot-platform')) && !this.props.location.pathname.includes('/customer-management')) {
													return <Row gutter={16}>
														<Col span={14} className="wid-100">
															<Form.Item label="Industry Sets">
																<Select
																	mode="multiple"
																	showSearch
																	optionFilterProp="title"
																	value={this.state.customer_set_list}
																	placeholder="Select Industry Set"
																	onChange={(e) => this.customer_set_select(e)}
																>
																	{(() => {
																		if (this.state.customer_list_arr && this.state.customer_list_arr.length > 0) {
																			return <Option key='*' title='all' value='*'>All</Option>
																		}
																	})()}
																	{(() => {
																		if (this.state.customer_set_list_arr && this.state.customer_set_list_arr.length > 0) {
																			return this.state.customer_set_list_arr.map((set) => {
																				return <Option key={set.id} disabled={this.state.all_selected_set} title={set.name} value={set.id}>{set.name}</Option>
																			});
																		}
																	})()}
																</Select>
															</Form.Item>
														</Col>
													</Row>
												}
											})()*/}
													</div>
												);
											}
										})()}
									</Form>
								</div>
							);
						} else if (this.state.unauthorised_access) {
							return (
								<Row
									type="flex"
									justify="space-around"
									className="device-details"
								>
									<div className="no-data-text">
										<Alert
											message="Access Denied"
											description={
												this.state
													.unauthorised_access_msg
											}
											type="error"
										/>
									</div>
								</Row>
							);
						} else {
							return (
								<div className="content-body">
									<Loading />
								</div>
							);
						}
					})()}
					<div
						style={{
							display: 'flex',
							padding: '5px 16px',
							float: 'right',
						}}
					>
						<AntButton
							buttons={
								this.state.add_user_form.draw_buttons.cancel
							}
							onButtonClick={() => this.onUserDrawerClose()}
						/>
						<AntButton
							buttons={
								this.state.add_user_form.draw_buttons.submit
							}
							onButtonClick={() => this.submitUserForm()}
						/>
					</div>
				</Drawer>
			);
		}
	}
);

export default AddUserDrawer;