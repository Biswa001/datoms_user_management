import { callFetch } from './ApiHandling';
import _ from 'lodash';
import  { getThingsAndParameterData, getThingsAggregrationPeriod, getAllThingsAndParametersLatestData, getThingsCategoriesData, getParametersOfAllThings } from './thingsListManipulation';

async function retriveThingsList(data={}) {
    try{
        let configData = {
            url : '/clients/' + parseInt(data.client_id) +'/applications/'+ parseInt(data.application_id) + '/things/list',
            method : 'GET',
        }
        return await callFetch(configData);
    }catch(err){
        throw err;
    }
}

async function retriveVendorThingsList(data={}) {

    try{
        let configData = {
            url : '/vendors/' + parseInt(data.vendor_id) +'/applications/'+ parseInt(data.application_id) + '/things/list',
            method : 'GET',
        }
        return await callFetch(configData);
    }catch(err){
        throw err;
    }
}

async function getThingsData(data={}, client_id, app_id) {
    try{
        let configData ={
            url :  '/clients/'+ client_id +'/applications/'+ app_id + '/things/data',
            method : 'POST',
            body : JSON.stringify({
                "data_type": data.data_type,
                "aggregation_period": data.aggregation_period,
                "parameters": data.parameters,
                "parameter_attributes": data.parameter_attributes,
                "things": data.things,
                "from_time": data.from_time,
                "upto_time": data.upto_time
            })
        }
        return await callFetch(configData);
    }
    catch(err){
        throw err;
    }
}

async function getThingsParameterUnitConversion(data=[], client_id, app_id) {
    try{
        let configData ={
            url : '/client/'+ client_id +'/application/'+ app_id +'/things/get-parameter-unit-conversion-functions',
            method : 'POST',
            body : JSON.stringify({
                parameter_details: data
            })
        }
        return await callFetch(configData);
    } catch(err){
        throw err;
    }
}

async function retriveThingsListWithThingsAndParameterData(data={}) {
    let response_data = await retriveThingsList(data);
    let modify_data = getThingsAndParameterData(response_data);
    return new Promise((resolve, reject) => {
        let dataValue = {};
        dataValue['response'] = response_data;
        dataValue['manipulate_data'] = modify_data;
        resolve(dataValue);
    });
}

async function getThingConfigs(clientId, appId, thingId) {
    try {
        let configData ={
            url : '/clients/'+ clientId +'/applications/'+ appId +'/things/'+ thingId,
            method : 'GET',
        }
        return await callFetch(configData);
    } catch(err) {
        throw err;
    }
}

async function saveThingConfigs(clientId, appId, thingId, thingDetails) {
    try {
        let configData ={
            url : '/clients/'+ clientId +'/applications/'+ appId +'/things/'+ thingId,
            method : 'PUT',
            body: JSON.stringify(thingDetails)
        }
        return await callFetch(configData);
    } catch(err) {
        throw err;
    }
}

async function addThing(thingDetails={}, clientId, appId) {
    try {
        let configData ={
            url : '/clients/'+ clientId +'/applications/'+ appId +'/things/add',
            method : 'POST',
            body: JSON.stringify(thingDetails)
        }
        return await callFetch(configData);
    } catch(err) {
        throw err;
    }
}

async function getThingCategoryDetails(clientId, appId, thingCategoryId) {
    try {
        let configData ={
            url : '/clients/'+ clientId +'/applications/'+ appId +'/thing-categories/'+ thingCategoryId,
            method : 'GET'
        }
        return await callFetch(configData);
    } catch(err) {
        throw err;
    }
}

async function getProtocolsList() {
    try {
        let configData ={
            url : '/protocols/list',
            method : 'GET'
        }
        return await callFetch(configData);
    } catch(err) {
        throw err;
    }
}

async function getTemplateData(thingCategoryId, templateId) {
    try {
        let configData ={
            url : '/thing-category/'+thingCategoryId+'/templates/'+templateId,
            method : 'GET'
        }
        return await callFetch(configData);
    } catch(err) {
        throw err;
    }
}

async function addNewTemplate(thingCategoryId, templateData) {
    try {
        let configData ={
            url: '/thing-category/'+ thingCategoryId + '/templates',
            method : 'POST',
            body: JSON.stringify(templateData)
        }
        return await callFetch(configData);
    } catch(err) {
        throw err;
    }
}

async function thingDebug(client_id, application_id, thing_id, from_time, upto_time, debug_type, page, pageSize, filter_type) {
    try {
        let configData ={
            url: '/clients/'+client_id+'/applications/'+application_id+'/things/'+thing_id+'/debug?from_time='+from_time+'&upto_time='+upto_time+'&page_no='+page+'&results_per_page='+pageSize+'&debug_type='+debug_type+'&filter_type='+filter_type,
            method : 'GET'
        }
        return await callFetch(configData);
    } catch(err) {
        throw err;
    }
}

async function thingHourlyDataAvailability(client_id, application_id, thing_id, from_time, upto_time) {
    try {
        let configData ={
            url: '/clients/'+client_id+'/applications/'+application_id+'/things/'+thing_id+'/data-availability',
            method : 'POST',
            body : JSON.stringify({
                "from_time": from_time,
                "upto_time": upto_time
            })
        }
        return await callFetch(configData);
    } catch(err) {
        throw err;
    }
}
export { retriveThingsList, retriveVendorThingsList, getThingsData, getThingsParameterUnitConversion, retriveThingsListWithThingsAndParameterData, getThingConfigs, saveThingConfigs, addThing, getThingCategoryDetails, getProtocolsList, getTemplateData, addNewTemplate, thingDebug, thingHourlyDataAvailability };
