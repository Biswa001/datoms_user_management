import React from 'react';
import {
	DeleteOutlined,
	EditOutlined,
	ExclamationCircleOutlined,
} from '@ant-design/icons';
import {
	Drawer,
	Row,
	Col,
	Alert,
	notification,
	Select,
	Collapse,
	TreeSelect,
	Tooltip,
	Menu,
	Dropdown,
	Modal,
	Tree,
} from 'antd';
import Loading from '../Loading/index';
import AntButton from '../AntButton/index';
import AddRoleDrawer from './AddRoleDrawer';
import _ from 'lodash';
import moment from 'moment';
import { retriveRoles, deleteRole } from './UsersAPI';

const { Option } = Select;
const { Panel } = Collapse;
const TreeNode = Tree.TreeNode;
const confirm = Modal.confirm;
const SHOW_PARENT = TreeSelect.SHOW_PARENT;

let treeData = [];

export default class RoleListDrawer extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			master_access_string: '*',
			role_id: '',
			drawRoleVisible: false,
			checkedKeys: this.props.location.pathname.includes(
				'/user-management'
			)
				? ['0']
				: [],
			add_role: false,
			access_key: [
				'UserManagement:UserManagement',
				'UserManagement:ManageRole',
			],
			visible_delete_modal: false,
			filter_list:
				this.parsed_search &&
				Object.values(this.parsed_search).length &&
				this.parsed_search.filter
					? this.parsed_search.filter.split(',')
					: [],
			table_search:
				this.parsed_search &&
				Object.values(this.parsed_search).length &&
				this.parsed_search.search
					? this.parsed_search.search
					: '',
		};
		// console.log('roles_props_', this.props);
	}

	componentDidMount() {
		this.fetchRoleList();
	}

	/**
	 * This function shows nofifications.
	 */
	openNotification(type, msg) {
		notification[type]({
			message: msg,
			// description: msg,
			placement: 'bottomLeft',
			className: 'alert-' + type,
		});
	}

	/**
	 * This function closes the slider and sets the sets the values to default.
	 */
	closeAddEditDrawer() {
		// this.props.history.push(this.platform_slug + '/settings/roles/' + this.props.history.location.search);
		this.setState(
			{
				drawRoleVisible: false,
				add_role: false,
				edit_role: false,
			},
			() => {
				this.props.form.resetFields();
				if (
					this.props.location.pathname.search(
						'/customer-management/'
					) > -1
				) {
					if (this.state.app_drop == 'all') {
						this.props.history.push(
							this.platform_slug +
								'/customer-management/' +
								this.props.match.params.customer_id +
								'/applications/all/roles/view'
						);
					} else {
						this.props.history.push(
							this.platform_slug +
								'/customer-management/' +
								this.props.match.params.customer_id +
								'/applications/' +
								parseInt(this.state.app_drop) +
								'/roles/view'
						);
					}
				} else {
					this.props.history.push(
						this.platform_slug + this.user_slug + '/roles/view'
					);
				}
			}
		);
	}

	/**
	 * This function calls the API to get the roles list.
	 * @param  {Boolean} update
	 */
	async fetchRoleList() {
		let that = this;
		let response = await retriveRoles(
			that.props.client_id,
			that.props.app_drop
		);
		if (response.status === 403) {
			that.setState({
				unauthorised_access: true,
				unauthorised_access_msg: response.message,
			});
		} else if (response.status === 'success') {
			// console.log('roles_data', response);

			treeData = [];
			let sub_access_object = {};
			let sub_access_name_object = {};
			let access_object = {};
			let access_list_options = {};
			let access_options = {};
			let access_options_keys = {};
			let sub_access_options = {};
			let sub_access_options_keys = {};
			if (
				response.access_list &&
				Object.values(response.access_list).length &&
				response.access_list.access_groups &&
				response.access_list.access_groups.length
			) {
				response.access_list.access_groups.map((access, main_index) => {
					let temp = [];
					let temp2 = [];
					// console.log('accesss', access);
					// console.log('accesss', access);
					if (access.access_list && access.access_list.length) {
						access.access_list.map((sub_access) => {
							// console.log('sub_access_', sub_access);
							temp.push({
								title: sub_access.name,
								value: sub_access.key,
								key: sub_access.key,
								disabled:
									sub_access.key == 'View:View'
										? true
										: false,
							});

							temp2.push(sub_access.key);
							if (!sub_access_name_object[sub_access.key]) {
								sub_access_name_object[sub_access.key] =
									sub_access.name;
							} else {
								sub_access_name_object[sub_access.key] =
									sub_access.name;
							}

							if (!sub_access_object[sub_access.key]) {
								sub_access_object[sub_access.key] =
									access.group_name;
							} else {
								sub_access_object[sub_access.key] =
									access.group_name;
							}
						});

						// console.log('temp__', temp);

						if (temp.length > 1) {
							treeData.push({
								title:
									access.group_name === 'View'
										? this.props.t('view')
										: access.group_name ===
										  'Create, edit missions.'
										? this.props.t('create_edit_misions')
										: access.group_name ===
										  'Create, edit, delete users.'
										? this.props.t(
												'create_edit_delete_user'
										  )
										: access.group_name ===
										  'Create, edit, delete user roles.'
										? this.props.t(
												'create_edit_delete_user_roles'
										  )
										: access.group_name,
								value: main_index,
								key: main_index,
								children: temp,
							});
						} else {
							treeData.push({
								title:
									access.group_name === 'View'
										? this.props.t('view')
										: access.group_name ===
										  'Create, edit missions.'
										? this.props.t('create_edit_misions')
										: access.group_name ===
										  'Create, edit, delete users.'
										? this.props.t(
												'create_edit_delete_user'
										  )
										: access.group_name ===
										  'Create, edit, delete user roles.'
										? this.props.t(
												'create_edit_delete_user_roles'
										  )
										: access.group_name,
								value: main_index,
								key: main_index,
								// 'disabled': temp[0].disabled
							});
						}

						if (!access_object[access.group_name]) {
							access_object[access.group_name] = temp2;
						} else {
							access_object[access.group_name] = temp2;
						}
					}
				});
			}

			let roleValue = [];
			roleValue = that.state.filter_list;

			if (
				that.props.history.location &&
				that.props.history.location.pathname.search('/edit') > -1
			) {
				let role_id = that.props.match.params.role_id;
				if (response.roles_list && response.roles_list.length) {
					response.roles_list.map((st_gr) => {
						if (st_gr.id == role_id) {
							that.editRole(
								st_gr,
								response.access_list,
								access_options_keys,
								access_list_options,
								sub_access_options_keys
							);
						}
					});
				}
			}

			if (
				that.props.history.location &&
				that.props.history.location.pathname.search('/delete') > -1
			) {
				let role_id = that.props.match.params.role_id;
				if (response.roles_list && response.roles_list.length) {
					response.roles_list.map((st_gr) => {
						if (st_gr.id == role_id && st_gr.is_deletable) {
							that.showDeleteConfirm(st_gr);
						}
					});
				}
			}

			if (roleValue.includes(that.state.master_access_string)) {
				roleValue = [that.state.master_access_string];
			}

			// console.log('treeData', treeData);
			// console.log('access_options', access_options);
			// console.log('access_list_options', access_list_options);
			that.setState(
				{
					unauthorised_role_access: false,
					all_data: response,
					acl_version: response.access_list.version,
					sub_access_object: sub_access_object,
					sub_access_name_object: sub_access_name_object,
					access_object: access_object,
					all_access_list: response.access_list,
					all_roles_list: response.roles_list,
					roleValue: roleValue,
					filter_list: roleValue,
					treeData: treeData,
					access_list_options: access_list_options,
					access_options: access_options,
					sub_access_options: sub_access_options,
					access_options_keys: access_options_keys,
					sub_access_options_keys: sub_access_options_keys,
					edit_role:
						that.props.history.location &&
						that.props.history.location.pathname.search('/edit') >
							-1
							? true
							: false,
					add_role:
						that.props.history.location &&
						that.props.history.location.pathname.search('/add') > -1
							? true
							: false,
					delete_role:
						that.props.history.location &&
						that.props.history.location.pathname.search('/delete') >
							-1
							? true
							: false,
				},
				() => {
					that.queryCreate();
				}
			);
		} else {
			that.openNotification('error', response.message);
			that.setState({
				unauthorised_access: false,
				loading: true,
				error_API: true,
				error_API_msg: response.message,
			});
		}
	}

	/**
	 * This function sets the current value of the selected role to be edited.
	 * @param  {Object} role                     Selected role.
	 */
	editRole(role, access_list = false) {
		this.setState(
			{
				drawRoleVisible: true,
				selected_role_data: role,
				selected_access_list: access_list,
				add_role: false,
				edit_role: true,
			},
			() => {
				if (
					this.props.location.pathname.search(
						'/customer-management/'
					) > -1
				) {
					this.props.history.push(
						this.props.platform_slug +
							'/customer-management/' +
							this.props.match.params.customer_id +
							'/applications/' +
							parseInt(this.props.app_drop) +
							'/roles/' +
							role.id +
							'/edit'
					);
				} else {
					this.props.history.push(
						this.props.platform_slug +
							this.props.user_slug +
							'/roles/' +
							role.id +
							'/edit'
					);
				}
			}
		);
	}

	/**
	 * This funtion creates the query to be pushed int he url.
	 */
	queryCreate() {
		// console.log('in queryCreate', this.props.history.location.pathname);
		let query_string =
			'?' +
			(this.state.filter_list && this.state.filter_list.length
				? 'filter=' + this.state.filter_list
				: '') +
			(this.state.table_search && this.state.table_search != ''
				? this.state.filter_list && this.state.filter_list.length
					? '&search=' + this.state.table_search
					: 'search=' + this.state.table_search
				: '');

		// console.log('queryCreate state', this.state);
		if (
			((this.state.role_id && this.state.role_id != '') ||
				this.props.match.params.role_id) &&
			this.state.edit_role
		) {
			// console.log('queryCreate 1', this.state.role_id);
		} else if (this.state.add_role) {
			// console.log('queryCreate 2', this.state.role_id);
		} else if (
			((this.state.role_id && this.state.role_id != '') ||
				this.props.match.params.role_id) &&
			this.state.delete_role
		) {
			// console.log('queryCreate 3', this.state.role_id);
		} else {
			// console.log('queryCreate 4', this.state.role_id);
		}

		let filtered_stations = [];
		if (
			this.state.all_roles_list &&
			this.state.all_roles_list.length &&
			this.state.filter_list &&
			this.state.filter_list.length
		) {
			// console.log('debug 1');
			this.state.all_roles_list.map((station) => {
				let c = 0;
				if (station.access == this.state.master_access_string) {
					c++;
				} else if (
					station.access &&
					station.access instanceof Array &&
					station.access.length
				) {
					station.access.map((st) => {
						this.state.filter_list.map((filter) => {
							if (st == filter) {
								// console.log('in filter');
								c++;
							} else if (st == this.state.master_access_string) {
								c++;
							}
						});
					});
				}
				if (c > 0) {
					filtered_stations.push(station);
				}
			});
		} else if (this.state.filter_list.length == 0) {
			// console.log('debug 2');
			filtered_stations = this.state.all_roles_list;
		}

		this.setState({
			filtered_stations: filtered_stations,
		});
	}

	/**
	 * This function closes the slider and sets the sets the values to default.
	 */
	closeAddEditDrawer() {
		// this.props.history.push(this.platform_slug + '/settings/roles/' + this.props.history.location.search);
		this.setState(
			{
				role_id: '',
				drawRoleVisible: false,
				add_role: false,
				edit_role: false,
				delete_role: false,
				selected_role_id: null,
				selected_role_name: null,
				selected_role_description: null,
				selected_role_access: {},
				master_access: false,
				checkedKeys: this.props.location.pathname.includes(
					'/user-management'
				)
					? ['0']
					: [],
				selected_role_master_access: false,
				selected_created_by: null,
				unauthorised_role_access: false,
				unauthorised_role_access_msg: '',
			},
			() => {
				// this.props.form.resetFields();
				if (
					this.props.location.pathname.search(
						'/customer-management/'
					) > -1
				) {
					if (this.state.app_drop == 'all') {
						this.props.history.push(
							this.props.platform_slug +
								'/customer-management/' +
								this.props.match.params.customer_id +
								'/applications/all/roles/view'
						);
					} else {
						this.props.history.push(
							this.props.platform_slug +
								'/customer-management/' +
								this.props.match.params.customer_id +
								'/applications/' +
								parseInt(this.state.app_drop) +
								'/roles/view'
						);
					}
				} else {
					this.props.history.push(
						this.props.platform_slug +
							this.props.user_slug +
							'/roles/view'
					);
				}
			}
		);
	}

	/**
	 * This function sets the delete confirmation popup.
	 * @param  {Object} role Role data.
	 */
	showDeleteConfirm(role, event) {
		event.stopPropagation();
		// console.log('showDeleteConfirm', role);
		let that = this;
		this.setState({
			role_details: role,
		});
		confirm({
			title: this.props.t('do_you_want_to_delete'),
			content: role.name,
			zIndex: 2000,
			onOk() {
				that.deleteRoleFunction(role);
			},
			onCancel() {
				that.closeAddEditDrawer();
			},
		});
	}

	/**
	 * This function calls the API to delete the role.
	 * @param  {Object} role ROle data.
	 */
	async deleteRoleFunction(role) {
		let that = this;

		let response = await deleteRole(
			that.props.client_id,
			that.props.app_drop,
			role.id
		);
		if (response.status === 403) {
			that.setState({
				unauthorised_access: true,
				unauthorised_access_msg: response.message,
			});
		} else if (response.status === 'success') {
			that.setState({
				unauthorised_role_access: false,
			});
			that.openNotification('success', 'Role deleted Successfully');
			that.props.fetchCustomerDetails();
			that.fetchRoleList();
			that.closeAddEditDrawer();
		} else {
			that.openNotification('error', response.message);
			that.setState({
				unauthorised_access: false,
				loading: true,
				error_API: true,
				error_API_msg: response.message,
			});
		}
	}

	/**
	 * This function opens the Slider to Add or Edit the role.
	 */
	addNewRole() {
		// console.log('addNewRole');
		this.setState(
			{
				drawRoleVisible: true,
				add_role: true,
				edit_role: false,
			},
			() => {
				if (
					this.props.location.pathname.includes(
						'/customer-management/'
					)
				) {
					if (this.state.app_drop == 'all') {
						this.props.history.push(
							this.props.platform_slug +
								'/customer-management/' +
								this.props.match.params.customer_id +
								'/applications/all/roles/add'
						);
					} else {
						this.props.history.push(
							this.props.platform_slug +
								'/customer-management/' +
								this.props.match.params.customer_id +
								'/applications/' +
								parseInt(this.state.app_drop) +
								'/roles/add'
						);
					}
				} else {
					this.props.history.push(
						this.props.platform_slug +
							this.props.user_slug +
							'/roles/add'
					);
				}
			}
		);
	}

	render() {
		// console.log('add_drawer', this.props);
		return (
			<Drawer
				title={this.props.t('roles')}
				className="role-details"
				width={945}
				placement="right"
				visible={true}
				onClose={() => this.props.closeRoleDrawer(this.state.all_data)}
				destroyOnClose={true}
				maskClosable={false}
				style={{
					paddingBottom: 53,
				}}
			>
				{(() => {
					if (
						this.state.all_data &&
						Object.keys(this.state.all_data).length
					) {
						return (
							<div>
								<div className="top-section">
									{(() => {
										if (
											this.props.role_view_draw.add_button
										) {
											return (
												<AntButton
													type={'add'}
													access_key={
														this.state.access_key
													}
													getViewAccess={(
														access_key
													) =>
														this.props.getViewAccess(
															access_key
														)
													}
													add_button={
														this.props
															.role_view_draw
															.add_button
													}
													onButtonClick={() =>
														this.addNewRole()
													}
												/>
											);
										}
									})()}
								</div>
								<div className="features collapse">
									{(() => {
										if (
											this.state.all_roles_list &&
											this.state.all_roles_list.length
										) {
											// console.log('role list', this.state.all_roles_list);
											return (
												<Collapse
													defaultActiveKey={['1']}
													expandIconPosition="right"
												>
													{(() => {
														return this.state.all_roles_list.map(
															(roles) => {
																// console.log('rolesss', roles);
																let app_name =
																	'';
																if (
																	this.props
																		.details_customer_data &&
																	this.props
																		.details_customer_data
																		.application_details &&
																	this.props
																		.details_customer_data
																		.application_details
																		.length
																) {
																	this.props.details_customer_data.application_details.map(
																		(
																			application,
																			index
																		) => {
																			// console.log('rolesss application', application);
																			if (
																				application.role_details &&
																				application
																					.role_details
																					.length
																			) {
																				application.role_details.map(
																					(
																						role_detls
																					) => {
																						// console.log('rolesss role_detls', role_detls);
																						if (
																							role_detls.role_id ===
																							roles.id
																						) {
																							let app_detls = _.find(
																								this
																									.props
																									.all_app_list,
																								{
																									id:
																										application.application_id,
																								}
																							);
																							if (
																								app_detls
																							) {
																								app_name =
																									app_detls.name;
																							}
																						}
																					}
																				);
																			}
																		}
																	);
																}
																// console.log('roless_', roles);
																return (
																	<Panel
																		header={
																			<div className="collapse-header-sec">
																				<div>
																					<div>
																						{
																							roles.name
																						}
																					</div>
																					<div className="small-text medium-text-col">
																						{this.props.t(
																							'created_by'
																						) +
																							' '}
																						<span>
																							{
																								roles.created_by
																							}
																						</span>{' '}
																						{this.props.t(
																							'on'
																						)}{' '}
																						<span>
																							{moment
																								.unix(
																									roles.created_on
																								)
																								.format(
																									'Do MMM HH:mm'
																								)}
																						</span>
																					</div>
																				</div>
																				<div className="divider"></div>
																			</div>
																		}
																		extra={
																			<div className="collapse-header-sec">
																				{(() => {
																					if (
																						roles.description !=
																						''
																					) {
																						return (
																							<Tooltip
																								title={
																									roles.description
																								}
																							>
																								<ExclamationCircleOutlined className="desc-icon" />
																							</Tooltip>
																						);
																					} else {
																						return (
																							<Tooltip
																								title={this.props.t(
																									'no_description'
																								)}
																							>
																								<ExclamationCircleOutlined className="desc-icon" />
																							</Tooltip>
																						);
																					}
																				})()}

																				<div className="action-buttons">
																					{(() => {
																						if (
																							this
																								.props
																								.show_add_edit_role
																						) {
																							if (
																								roles.is_deletable
																							) {
																								if (
																									this.props.getViewAccess(
																										this
																											.state
																											.access_key
																									)
																								) {
																									return (
																										<EditOutlined
																											className="action-icon"
																											onClick={(
																												e
																											) => {
																												this.editRole(
																													roles,
																													this
																														.state
																														.all_access_list
																												);
																												e.stopPropagation();
																											}}
																										/>
																									);
																								} else {
																									return (
																										<EditOutlined className="action-icon block" />
																									);
																								}
																							}
																						}
																					})()}
																					{(() => {
																						if (
																							this
																								.state
																								.app_drop !==
																								'all' &&
																							roles.is_deletable
																						) {
																							if (
																								this.props.getViewAccess(
																									this
																										.state
																										.access_key
																								)
																							) {
																								return (
																									<DeleteOutlined
																										className="action-icon"
																										onClick={(
																											e
																										) =>
																											this.showDeleteConfirm(
																												roles,
																												e
																											)
																										}
																									/>
																								);
																							} else {
																								return (
																									<DeleteOutlined className="action-icon block" />
																								);
																							}
																						}
																					})()}

																					{(() => {
																						if (
																							this
																								.state
																								.app_drop ===
																								'all' &&
																							app_name !==
																								'' &&
																							roles.is_deletable
																						) {
																							return (
																								<span className="app-tag">
																									{
																										app_name
																									}
																								</span>
																							);
																						}
																					})()}
																				</div>
																			</div>
																		}
																		key={
																			roles.id
																		}
																	>
																		{(() => {
																			if (
																				roles.access ==
																				'*'
																			) {
																				return (
																					<ul>
																						<li>
																							{this.props.t(
																								'master_access'
																							)}
																						</li>
																					</ul>
																				);
																			} else {
																				let access_data_main_array = [];
																				// roles.access
																				if (
																					roles.access &&
																					roles
																						.access
																						.length
																				) {
																					let access_data_array = [];
																					let access_list_man = [];
																					roles.access.map(
																						(
																							role_key,
																							indx
																						) => {
																							if (
																								this
																									.state
																									.all_access_list
																									.access_groups &&
																								this
																									.state
																									.all_access_list
																									.access_groups
																									.length
																							) {
																								access_list_man = [];
																								this.state.all_access_list.access_groups.map(
																									(
																										access_grp,
																										ind
																									) => {
																										// console.log('access_data_array', access_grp);
																										if (
																											_.find(
																												access_grp.access_list,
																												{
																													key: role_key,
																												}
																											)
																										) {
																											access_list_man.push(
																												_.find(
																													access_grp.access_list,
																													{
																														key: role_key,
																													}
																												)
																											);
																											access_data_array.push(
																												{
																													group_name:
																														access_grp.group_name,
																													access_list: access_list_man,
																												}
																											);
																										}
																									}
																								);
																							}
																						}
																					);
																					let access_data_array_new_data = [];
																					if (
																						this
																							.state
																							.all_access_list
																							.access_groups &&
																						this
																							.state
																							.all_access_list
																							.access_groups
																							.length
																					) {
																						this.state.all_access_list.access_groups.map(
																							(
																								acs_grp,
																								ind
																							) => {
																								let access_data_array_new = _.filter(
																									access_data_array,
																									{
																										group_name:
																											acs_grp.group_name,
																									}
																								);
																								if (
																									access_data_array_new &&
																									access_data_array_new.length
																								) {
																									access_data_array_new_data.push(
																										access_data_array_new
																									);
																								}
																							}
																						);
																					}
																					access_data_main_array = [];
																					let access_data_main_list = {};
																					if (
																						access_data_array_new_data &&
																						access_data_array_new_data.length
																					) {
																						access_data_array_new_data.map(
																							(
																								acs_dta_arr,
																								indxx
																							) => {
																								if (
																									!access_data_main_list[
																										indxx
																									]
																								) {
																									access_data_main_list[
																										indxx
																									] = [];
																								}
																								if (
																									!access_data_main_array[
																										indxx
																									]
																								) {
																									access_data_main_array[
																										indxx
																									] = {};
																								}
																								acs_dta_arr.map(
																									(
																										acs_dta,
																										index
																									) => {
																										acs_dta.access_list.map(
																											(
																												acs_data,
																												indd
																											) => {
																												access_data_main_list[
																													indxx
																												].push(
																													acs_data
																												);
																											}
																										);
																										access_data_main_array[
																											indxx
																										][
																											'group_name'
																										] =
																											acs_dta.group_name;
																										access_data_main_array[
																											indxx
																										][
																											'access_list'
																										] =
																											access_data_main_list[
																												indxx
																											];
																									}
																								);
																							}
																						);
																					}
																					// console.log('access_data_array', access_data_main_array);
																				}

																				if (
																					access_data_main_array &&
																					access_data_main_array.length
																				) {
																					return (
																						<ul>
																							{(() => {
																								return access_data_main_array.map(
																									(
																										access_data
																									) => {
																										return (
																											<li>
																												<div>
																													{access_data.group_name ===
																													'View'
																														? this.props.t(
																																'view'
																														  )
																														: access_data.group_name ===
																														  'Create, edit missions.'
																														? this.props.t(
																																'create_edit_misions'
																														  )
																														: access_data.group_name ===
																														  'Create, edit, delete users.'
																														? this.props.t(
																																'create_edit_delete_user'
																														  )
																														: access_data.group_name ===
																														  'Create, edit, delete user roles.'
																														? this.props.t(
																																'create_edit_delete_user_roles'
																														  )
																														: access_data.group_name}
																												</div>
																												<div>
																													{(() => {
																														if (
																															access_data.access_list &&
																															access_data
																																.access_list
																																.length
																														) {
																															return (
																																<ul>
																																	{(() => {
																																		return access_data.access_list.map(
																																			(
																																				access_list1
																																			) => {
																																				return (
																																					<li>
																																						{access_list1.name ===
																																						'View'
																																							? this.props.t(
																																									'view'
																																							  )
																																							: access_list1.name ===
																																							  'Create, edit missions.'
																																							? this.props.t(
																																									'create_edit_misions'
																																							  )
																																							: access_list1.name ===
																																							  'Create, edit, delete users.'
																																							? this.props.t(
																																									'create_edit_delete_user'
																																							  )
																																							: access_list1.name ===
																																							  'Create, edit, delete users and user roles'
																																							? this.props.t(
																																									'create_edit_delete_user_and_user_roles'
																																							  )
																																							: access_list1.name}
																																					</li>
																																				);
																																			}
																																		);
																																	})()}
																																</ul>
																															);
																														}
																													})()}
																												</div>
																											</li>
																										);
																									}
																								);
																							})()}
																						</ul>
																					);
																				} else {
																					return (
																						<div>
																							No
																							Access
																						</div>
																					);
																				}
																			}
																		})()}
																	</Panel>
																);
															}
														);
													})()}
												</Collapse>
											);
										} else {
											return (
												<Row
													type="flex"
													justify="space-around"
													className="no-data-text-container"
												>
													<div className="no-data-text">
														No roles found!
													</div>
												</Row>
											);
										}
									})()}
								</div>
							</div>
						);
					} else if (this.state.unauthorised_role_access) {
						return (
							<Row
								type="flex"
								justify="space-around"
								className="no-data-text-container"
							>
								<div className="no-data-text">
									<Alert
										message="Access Denied"
										description={
											this.state
												.unauthorised_role_access_msg
										}
										type="error"
									/>
								</div>
							</Row>
						);
					} else {
						return <Loading />;
					}
				})()}
				{(() => {
					if (
						this.state.drawRoleVisible ||
						this.props.location.pathname.includes('/roles/add') ||
						this.props.location.pathname.includes(
							'/roles/' +
								this.props.match.params.role_id +
								'/edit'
						)
					) {
						return (
							<AddRoleDrawer
								t={this.props.t}
								location={this.props.location}
								match={this.props.match}
								history={this.props.history}
								tree_data={treeData}
								add_role_form={
									this.props.role_view_draw.add_role_form
								}
								closeAddEditDrawer={() =>
									this.closeAddEditDrawer()
								}
								add_role={this.state.add_role}
								edit_role={this.state.edit_role}
								fetchRoleList={() => this.fetchRoleList()}
								fetchCustomerDetails={() =>
									this.props.fetchCustomerDetails()
								}
								client_id={this.props.client_id}
								app_drop={this.props.app_drop}
								all_access_list={this.state.all_access_list}
								selected_role_data={
									this.state.selected_role_data
								}
								selected_access_list={
									this.state.selected_access_list
								}
								platform_slug={this.props.platform_slug}
								user_slug={this.props.user_slug}
							/>
						);
					}
				})()}
			</Drawer>
		);
	}
}
