import React from 'react';
import { Tooltip, Dropdown, Menu } from 'antd';
// import { ReactComponent as DotsVertical } from './dots_vertical.svg';
import { MoreOutlined } from '@ant-design/icons';

let change_status = [{
  text: 'Active',
  value: 'status',
  primary: true
}];

let iot_table_data = {
  action_button: {
    primary: true,
    text: 'Update Status',
    show_action_button: false
  },
  config: {
    pagination_data: {
      size: "small",
      total: 0,
      pageSize: 20,
      showSizeChanger: true,
      showQuickJumper: true,
      hideOnSinglePage: false
    },
    bordered: false,
    size: 'default',
    showHeader: true,
    scroll: {y: 240},
    loading: false,
    locale: {
        filterTitle: 'Select Filter',
        filterConfirm: 'Ok',
        filterReset: 'Reset',
        emptyText: 'No User',
    }
  },
  head_data: [
    {
      title: <span className="" dangerouslySetInnerHTML={{__html: 'Name'}} />,
      dataIndex: 'name',
      key: 'name',
      sorter: (a, b) => {
        return (a.first_name && b.first_name && isNaN(a.first_name) && isNaN(b.first_name) && a.first_name.toLowerCase() !== b.first_name.toLowerCase() ? (((a.first_name.toLowerCase() + ' ' + (a.last_name && a.last_name != '' && isNaN(a.last_name) ? a.last_name.toLowerCase() : '')) < (b.first_name.toLowerCase() + ' ' + (b.last_name && b.last_name != '' && isNaN(b.last_name) ? b.last_name.toLowerCase() : ''))) ? -1 : 1) : 0);
      },
      width: '18%',
      render:(value, row_data, index) => ( 
        <div>
          {(() => {
            let mob_no = '',
              email = '';
            if (row_data.mobile_no && row_data.mobile_no.length) {
              mob_no = row_data.mobile_no[0];
            }
            if (row_data.email_id != '' && row_data.mobile_no != '') {
              email = row_data.email_id + ', ' + row_data.mobile_no;
            } else if (row_data.email_id != '' && row_data.mobile_no == '') {
              email = row_data.email_id;
            } else if (row_data.email_id == '' && row_data.mobile_no != '') {
              email = row_data.mobile_no;
            }
            return <div>
              {(() => {
                if (row_data.status == 1) {
                  return <div className="font-orange dspl-inline-flx">{value}</div>  
                } else {
                  return <Tooltip title="Deactive">
                    <div className="dspl-inline-flx">{value}</div>
                  </Tooltip>
                }
              })()}
              <div>{row_data.designation}</div>  
              <div>{email}</div>  
              <div className="last-login">{"Last login: "+row_data.login}</div>
            </div>
          })()}
        </div>
      )
    },
    {
      title: <span className="" dangerouslySetInnerHTML={{__html: 'Role'}} />,
      dataIndex: 'role',
      key: 'role',
      align: 'center',
      width: '12%',
      render: text => <span className="" dangerouslySetInnerHTML={{__html: text}} />
    },
    {
      title: <span className="" dangerouslySetInnerHTML={{__html: 'Applications'}} />,
      key: 'application',
      width: '54%',
      render: text => <span className="" dangerouslySetInnerHTML={{__html: text}} />
    },
    {
      title: <span className="" dangerouslySetInnerHTML={{__html: 'Industry Count'}} />,
      key: 'industries',
      dataIndex: 'industries',
      align: 'center',
      width: '8%',
      render: text => <span className="" dangerouslySetInnerHTML={{__html: text}} />
    },
    {
      title: <span className="" dangerouslySetInnerHTML={{__html: 'Action'}} />,
      key: 'action',
      align: 'center',
      width: '8%',
      render: (a, b, c) => (
        <Dropdown 
          overlay={
            <Menu>
              <Menu.Item key="action-1">{(b.status == 1 ? 'Deactivate' : 'Activate')}</Menu.Item>
              <Menu.Item key="action-2">Edit</Menu.Item>
              <Menu.Item key="action-3">Delete</Menu.Item>
            </Menu>
          } trigger={['click']} placement="bottomLeft">
          {/*<DotsVertical className="action-img"/>*/}
          <MoreOutlined className="action-img"/>
        </Dropdown>
      )
    }
  ],
  row_data: undefined
}

let application_table_data = {
  action_button: {
    primary: true,
    text: 'Update Status',
    show_action_button: false
  },
  config: {
    pagination_data: {
      size: "small",
      total: 0,
      pageSize: 20,
      showSizeChanger: true,
      showQuickJumper: true,
      hideOnSinglePage: false
    },
    bordered: false,
    size: 'default',
    showHeader: true,
    scroll: {y: 240},
    loading: false,
    locale: {
        filterTitle: 'Select Filter',
        filterConfirm: 'Ok',
        filterReset: 'Reset',
        emptyText: 'No User',
    }
  },
  head_data: [
    {
      title: <span className="" dangerouslySetInnerHTML={{__html: 'Name'}} />,
      dataIndex: 'name',
      key: 'name',
      sorter: (a, b) => {
        return (a.first_name && b.first_name && isNaN(a.first_name) && isNaN(b.first_name) && a.first_name.toLowerCase() !== b.first_name.toLowerCase() ? (((a.first_name.toLowerCase() + ' ' + (a.last_name && a.last_name != '' && isNaN(a.last_name) ? a.last_name.toLowerCase() : '')) < (b.first_name.toLowerCase() + ' ' + (b.last_name && b.last_name != '' && isNaN(b.last_name) ? b.last_name.toLowerCase() : ''))) ? -1 : 1) : 0);
      },
      width: '20%',
      render:(value, row_data, index) => ( 
        <div>
          {(() => {
            let mob_no = '',
              email = '';
            if (row_data.mobile_no && row_data.mobile_no.length) {
              mob_no = row_data.mobile_no[0];
            }
            if (row_data.email_id != '' && row_data.mobile_no != '') {
              email = row_data.email_id + ', ' + row_data.mobile_no;
            } else if (row_data.email_id != '' && row_data.mobile_no == '') {
              email = row_data.email_id;
            } else if (row_data.email_id == '' && row_data.mobile_no != '') {
              email = row_data.mobile_no;
            }
            return <div>
              {(() => {
                if (row_data.status == 1) {
                  return <div className="font-orange dspl-inline-flx">{value}</div>  
                } else {
                  return <Tooltip title="Deactive">
                    <div className="dspl-inline-flx">{value}</div>
                  </Tooltip>
                }
              })()}
              <div>{row_data.designation}</div>  
              <div>{email}</div>  
              <div className="last-login">{"Last login: "+row_data.login}</div>
            </div>
          })()}
        </div>
      )
    },
    {
      title: <span className="" dangerouslySetInnerHTML={{__html: 'Role'}} />,
      dataIndex: 'role',
      key: 'role',
      align: 'center',
      width: '15%',
      render: text => <span className="" dangerouslySetInnerHTML={{__html: text}} />
    },
    {
      title: <span className="" dangerouslySetInnerHTML={{__html: 'Things'}} />,
      key: 'things',
      dataIndex: 'things',
      width: '40%',
      render: text => <span className="" dangerouslySetInnerHTML={{__html: text}} />
    },
    {
      title: <span className="" dangerouslySetInnerHTML={{__html: 'Reports To'}} />,
      key: 'report_to',
      dataIndex: 'report_to',
      width: '15%',
      render: text => <span className="" dangerouslySetInnerHTML={{__html: text}} />
    },
    {
      title: <span className="" dangerouslySetInnerHTML={{__html: 'Action'}} />,
      key: 'action',
      align: 'center',
      width: '10%',
      render: (a, b, c) => (
        <Dropdown
          overlay={
            <Menu>
              <Menu.Item key="action-1">{(b.status == 1 ? 'Deactivate' : 'Activate')}</Menu.Item>
              <Menu.Item key="action-2">Edit</Menu.Item>
              <Menu.Item key="action-3">Delete</Menu.Item>
            </Menu>
          } trigger={['click']} placement="bottomLeft">
          {/*<DotsVertical className="action-img"/>*/}
          <MoreOutlined className="action-img"/>
        </Dropdown>
      )
    }
  ],
  row_data: undefined
}

let table_data = {
  action_button: {
    primary: true,
    text: 'Update Status',
    show_action_button: false
  },
  config: {
    pagination_data: {
      size: "small",
      total: 0,
      pageSize: 20,
      showSizeChanger: true,
      showQuickJumper: true,
      hideOnSinglePage: false
    },
    bordered: false,
    size: 'default',
    showHeader: true,
    scroll: {y: 240},
    loading: false,
    locale: {
        filterTitle: 'Select Filter',
        filterConfirm: 'Ok',
        filterReset: 'Reset',
        emptyText: 'No User',
    }
  },
  head_data: [
    {
      title: <span className="" dangerouslySetInnerHTML={{__html: 'Name'}} />,
      dataIndex: 'name',
      key: 'name',
      sorter: (a, b) => {
        return (a.first_name && b.first_name && isNaN(a.first_name) && isNaN(b.first_name) && a.first_name.toLowerCase() !== b.first_name.toLowerCase() ? (((a.first_name.toLowerCase() + ' ' + (a.last_name && a.last_name != '' && isNaN(a.last_name) ? a.last_name.toLowerCase() : '')) < (b.first_name.toLowerCase() + ' ' + (b.last_name && b.last_name != '' && isNaN(b.last_name) ? b.last_name.toLowerCase() : ''))) ? -1 : 1) : 0);
      },
      width: '35%',
      render:(value, row_data, index) => ( 
        <div>
          {(() => {
            let mob_no = '',
              email = '';
            if (row_data.mobile_no && row_data.mobile_no.length) {
              mob_no = row_data.mobile_no[0];
            }
            if (row_data.email_id != '' && row_data.mobile_no != '') {
              email = row_data.email_id + ', ' + row_data.mobile_no;
            } else if (row_data.email_id != '' && row_data.mobile_no == '') {
              email = row_data.email_id;
            } else if (row_data.email_id == '' && row_data.mobile_no != '') {
              email = row_data.mobile_no;
            }
            return <div>
              {(() => {
                if (row_data.status == 1) {
                  return <div className="font-orange dspl-inline-flx">{value}</div>  
                } else {
                  return <Tooltip title="Deactive">
                    <div className="dspl-inline-flx">{value}</div>
                  </Tooltip>
                }
              })()}
              <div>{row_data.designation}</div>  
              <div>{email}</div>  
              <div className="last-login">{"Last login: "+row_data.login}</div>
            </div>
          })()}
        </div>
      )
    },
    {
      title: <span className="" dangerouslySetInnerHTML={{__html: 'Applications'}} />,
      key: 'application',
      width: '55%',
      render: text => <span className="" dangerouslySetInnerHTML={{__html: text}} />
    },
    {
      title: <span className="" dangerouslySetInnerHTML={{__html: 'Action'}} />,
      key: 'action',
      align: 'center',
      width: '10%',
      render: (a, b, c) => (
        <Dropdown 
          overlay={
            <Menu>
              <Menu.Item key="action-1">{(b.status == 1 ? 'Deactivate' : 'Activate')}</Menu.Item>
              <Menu.Item key="action-2">Edit</Menu.Item>
              <Menu.Item key="action-3">Delete</Menu.Item>
            </Menu>
          } trigger={['click']} placement="bottomLeft">
          {/*<DotsVertical className="action-img"/>*/}
          <MoreOutlined className="action-img"/>
        </Dropdown>
      )
    }
  ],
  row_data: undefined
}

export default {
  application_select_text: ['Application'],
  application_select_option:[{
    show_arrow: true,
    default_value: '',
    options: []
  }],
  /*add_button: {
    text: 'New User',
    icon: 'user-add'
  },*/
  role_button: [{
    text: 'Roles',
    value: 'open_role',
    primary: true
  }],
  user_add_button: [{
    text: 'Add User',
    value: 'add_user',
    primary: false,
    icon: 'user-add'
  }],
  subscription_text: [' (Total / Subscribed)'],
	iot_user_table: iot_table_data,
  user_table: table_data,
  application_user_table: application_table_data,
  add_user_form: {
    contact_select: [{
      show_search: true,
      allow_clear: true,
      show_arrow: true,
      options: []
    }],
    customer_details_form: [{
      key: 'first_name',
      label: 'First Name *',
      required: true,
      message: 'Please enter first name',
      initial_value: ''
    }, {
      key: 'last_name',
      label: 'Last Name',
      required: false,
      message: 'Please enter last name',
      initial_value: ''
    }, {
      key: 'designation',
      label: 'Designation',
      required: false,
      message: 'Please enter designation',
      initial_value: ''
    }, {
      key: 'email',
      label: 'Email *',
      required: true,
      message: 'Please enter email id',
      initial_value: ''
    }, {
      key: 'phone_no',
      type: 'number',
      label: 'Contact No',
      required: false,
      message: 'Please enter contact no.',
      initial_value: ''
    }],
    draw_buttons: {
      submit: [{
        text: 'Submit',
        value: 'submit',
        primary: 'true'
      }],
      cancel: [{
        text: 'Cancel',
        value: 'cancel'
      }]
    }
  },
  role_view_draw: {
    add_button: {
      text: 'New Role',
      icon: 'plus'
    },
    role_list_view: undefined,
    add_role_form: {
      draw_buttons: {
        submit: [{
          text: 'Submit',
          value: 'submit',
          primary: 'true'
        }],
        cancel: [{
          text: 'Cancel',
          value: 'cancel'
        }]
      }
    }
  }
}