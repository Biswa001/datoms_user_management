import { notification } from 'antd';

async function retriveUsers(client_id, app_id) {
	let path = '';
	if (app_id == 'all') {
		path = process.env.REACT_APP_IOT_PLATFORM_USERS_API_BASEPATH + '/clients/'+ client_id +'/applications/all/users/list';
	} else {
		path = process.env.REACT_APP_IOT_PLATFORM_USERS_API_BASEPATH + '/clients/'+ client_id +'/applications/'+ parseInt(app_id) +'/users/list';
	}
	
	return new Promise((resolve, reject) => {
		fetch(path, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json'
			},
			credentials: 'include'
		}).then(function(Response) {
			return Response.json();
		}).then(function(json) {
			resolve(json);
		}).catch(function(ex) {
			// console.log('parsing failed', ex);
			openNotification('error', ex.message);
			reject('Unable to load data!');
		});
	});
}

async function addUser(data={}, client_id) {
  return new Promise((resolve, reject) => {
	  fetch(process.env.REACT_APP_IOT_PLATFORM_USERS_API_BASEPATH + '/clients/' + client_id + '/users/new', {
		  method: 'POST',
		  headers: {
			  'Content-Type': 'application/json'
		  },
		  credentials: 'include',
		  body: JSON.stringify({
			  contact_id: data.contact_id,
				first_name: data.first_name,
				last_name: data.last_name,
				email: data.email,
				mobile_no: data.mobile_no,
				designation: data.designation,
				application_details: data.application_details,
				applications: data.applications,
				industries: data.industries,
				industry_sets: data.industry_sets,
				things: data.things
		  })
	  }).then(function(Response) {
		  return Response.json();
	  }).then(function(json) {
		  resolve(json);
	  }).catch((ex) => {
		  // console.log('parsing failed', ex);
		  openNotification('error', ex.message);
		  reject('Unable to load data!');
	  });
  });
}

async function editUser(data={}, client_id, user_id) {
  return new Promise((resolve, reject) => {
	  fetch(process.env.REACT_APP_IOT_PLATFORM_USERS_API_BASEPATH + '/clients/' + client_id + '/users/' + user_id + '/edit', {
		  method: 'POST',
		  headers: {
			  'Content-Type': 'application/json'
		  },
		  credentials: 'include',
		  body: JSON.stringify({
			  contact_id: data.contact_id,
				first_name: data.first_name,
				last_name: data.last_name,
				email: data.email,
				mobile_no: data.mobile_no,
				designation: data.designation,
				application_details: data.application_details,
				applications: data.applications,
				industries: data.industries,
				industry_sets: data.industry_sets,
				things: data.things
		  })
	  }).then(function(Response) {
		  return Response.json();
	  }).then(function(json) {
		  resolve(json);
	  }).catch((ex) => {
		  // console.log('parsing failed', ex);
		  openNotification('error', ex.message);
		  reject('Unable to load data!');
	  });
  });
}

async function deleteUser(data=[], client_id, user_id) {
  return new Promise((resolve, reject) => {
	  fetch(process.env.REACT_APP_IOT_PLATFORM_USERS_API_BASEPATH + '/clients/' + client_id + '/users/' + user_id + '/delete', {
		  method: 'POST',
		  headers: {
			  'Content-Type': 'application/json'
		  },
		  credentials: 'include',
		  body: JSON.stringify({
			  application_details: data
		  })
	  }).then(function(Response) {
		  return Response.json();
	  }).then(function(json) {
		  resolve(json);
	  }).catch((ex) => {
		  // console.log('parsing failed', ex);
		  openNotification('error', ex.message);
		  reject('Unable to load data!');
	  });
  });
}

async function updateUserStatus(data={}) {
	return new Promise((resolve, reject) => {
		fetch(process.env.REACT_APP_IOT_PLATFORM_USERS_API_BASEPATH + '/clients/' + data.client_id + '/users/' + data.user_id, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
			},
			credentials: 'include',
			body: JSON.stringify({
			  application_details: data.application_details,
			  is_active: data.is_active ? true : false
		  })
		}).then(function(Response) {
			return Response.json();
		}).then(function(json) {
			resolve(json);
		}).catch(function(ex) {
			// console.log('parsing failed', ex);
			openNotification('error', ex.message);
			reject('Unable to load data!');
		});
	});
}

async function retriveUserGroups(client_id, app_id) {
	return new Promise((resolve, reject) => {
		fetch(process.env.REACT_APP_IOT_PLATFORM_USERS_API_BASEPATH + '/clients/'+client_id+'/applications/'+parseInt(app_id)+'/user-groups/list', {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json'
			},
			credentials: 'include'
		}).then(function(Response) {
			return Response.json();
		}).then(function(json) {
			resolve(json);
		}).catch(function(ex) {
			// console.log('parsing failed', ex);
			openNotification('error', ex.message);
			reject('Unable to load data!');
		});
	});
}

async function addUserGroup(data={}, client_id, app_id) {
  return new Promise((resolve, reject) => {
	  fetch(process.env.REACT_APP_IOT_PLATFORM_USERS_API_BASEPATH + '/clients/'+client_id+'/applications/'+ parseInt(app_id) + '/user-groups/new', {
		  method: 'POST',
		  headers: {
			  'Content-Type': 'application/json'
		  },
		  credentials: 'include',
		  body: JSON.stringify({
			  name: data.name,
				description: data.description,
				user_ids: data.user_ids
		  })
	  }).then(function(Response) {
		  return Response.json();
	  }).then(function(json) {
		  resolve(json);
	  }).catch((ex) => {
		  // console.log('parsing failed', ex);
		  openNotification('error', ex.message);
		  reject('Unable to load data!');
	  });
  });
}

async function editUserGroup(data={}, client_id, app_id, group_id) {
  return new Promise((resolve, reject) => {
	  fetch(process.env.REACT_APP_IOT_PLATFORM_USERS_API_BASEPATH + '/clients/'+client_id+'/applications/'+ parseInt(app_id) + '/user-groups/'+group_id+'/edit', {
		  method: 'POST',
		  headers: {
			  'Content-Type': 'application/json'
		  },
		  credentials: 'include',
		  body: JSON.stringify({
			  name: data.name,
				description: data.description,
				user_ids: data.user_ids
		  })
	  }).then(function(Response) {
		  return Response.json();
	  }).then(function(json) {
		  resolve(json);
	  }).catch((ex) => {
		  // console.log('parsing failed', ex);
		  openNotification('error', ex.message);
		  reject('Unable to load data!');
	  });
  });
}

async function deleteUserGroup(client_id, app_id, group_id) {
  return new Promise((resolve, reject) => {
	  fetch(process.env.REACT_APP_IOT_PLATFORM_USERS_API_BASEPATH + '/clients/'+client_id+'/applications/'+ parseInt(app_id) + '/user-groups/'+group_id+'/delete', {
		  method: 'GET',
		  headers: {
			  'Content-Type': 'application/json'
		  },
		  credentials: 'include'
	  }).then(function(Response) {
		  return Response.json();
	  }).then(function(json) {
		  resolve(json);
	  }).catch((ex) => {
		  // console.log('parsing failed', ex);
		  openNotification('error', ex.message);
		  reject('Unable to load data!');
	  });
  });
}

async function retriveRoles(client_id, app_id) {
	let path = '';
	if (app_id == 'all') {
		path = process.env.REACT_APP_IOT_PLATFORM_USERS_API_BASEPATH + '/clients/' + client_id + '/applications/all/users/roles/list';
	} else {
		path = process.env.REACT_APP_IOT_PLATFORM_USERS_API_BASEPATH + '/clients/' + client_id + '/applications/' + parseInt(app_id) + '/users/roles/list';
	}
	
	return new Promise((resolve, reject) => {
		fetch(path, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json'
			},
			credentials: 'include'
		}).then(function(Response) {
			return Response.json();
		}).then(function(json) {
			resolve(json);
		}).catch(function(ex) {
			// console.log('parsing failed', ex);
			openNotification('error', ex.message);
			reject('Unable to load data!');
		});
	});
}

async function addRole(data={}, client_id, app_id) {
  return new Promise((resolve, reject) => {
	  fetch(process.env.REACT_APP_IOT_PLATFORM_USERS_API_BASEPATH + '/clients/' + client_id + '/applications/' + parseInt(app_id) + '/users/roles/new', {
		  method: 'POST',
		  headers: {
			  'Content-Type': 'application/json'
		  },
		  credentials: 'include',
		  body: JSON.stringify({
			  client_id: data.client_id,
				application_id: data.application_id,
				name: data.name,
				description: data.description,
				access: data.access,
				acl_version: data.acl_version
		  })
	  }).then(function(Response) {
		  return Response.json();
	  }).then(function(json) {
		  resolve(json);
	  }).catch((ex) => {
		  // console.log('parsing failed', ex);
		  openNotification('error', ex.message);
		  reject('Unable to load data!');
	  });
  });
}

async function editRole(data={}, client_id, app_id, role_id) {
  return new Promise((resolve, reject) => {
	  fetch(process.env.REACT_APP_IOT_PLATFORM_USERS_API_BASEPATH + '/clients/' + client_id + '/applications/' + parseInt(app_id) + '/users/roles/' + role_id + '/edit', {
		  method: 'POST',
		  headers: {
			  'Content-Type': 'application/json'
		  },
		  credentials: 'include',
		  body: JSON.stringify({
			  client_id: data.client_id,
				application_id: data.application_id,
				name: data.name,
				roles_id: data.roles_id,
				description: data.description,
				access: data.access,
				acl_version: data.acl_version
		  })
	  }).then(function(Response) {
		  return Response.json();
	  }).then(function(json) {
		  resolve(json);
	  }).catch((ex) => {
		  // console.log('parsing failed', ex);
		  openNotification('error', ex.message);
		  reject('Unable to load data!');
	  });
  });
}

async function deleteRole(client_id, app_id, role_id) {
  return new Promise((resolve, reject) => {
	  fetch(process.env.REACT_APP_IOT_PLATFORM_USERS_API_BASEPATH + '/clients/' + client_id + '/applications/' + parseInt(app_id) + '/users/roles/' + role_id + '/delete', {
		  method: 'POST',
		  headers: {
			  'Content-Type': 'application/json'
		  },
		  credentials: 'include',
		  body: JSON.stringify({
			  new_role_id: 'delete'
		  })
	  }).then(function(Response) {
		  return Response.json();
	  }).then(function(json) {
		  resolve(json);
	  }).catch((ex) => {
		  // console.log('parsing failed', ex);
		  openNotification('error', ex.message);
		  reject('Unable to load data!');
	  });
  });
}

function openNotification(type, msg){
	notification[type]({
		message: msg,
		// description: msg,
		placement: 'bottomLeft',
		className: 'alert-' + type,
	});
}

export { retriveUsers, addUser, editUser, deleteUser, updateUserStatus, retriveUserGroups, addUserGroup, editUserGroup, deleteUserGroup, retriveRoles, addRole, editRole, deleteRole };