import React from 'react';
import UsersManagement from './';
import { action } from '@storybook/addon-actions';

export default {
	title: 'UsersManagement',
	component: UsersManagement,
};

const actionsData = {
	getViewAccess: action('getViewAccess'),
	t: action('translation'),
};

export const ZeroConfig = () => (
	<UsersManagement
		location={{pathname: '/user-management'}}
		match={{params:{app_id: '1'}}}
		{...actionsData}
	/>
);