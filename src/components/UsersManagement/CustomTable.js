import React from 'react';
import { Table, Button } from 'antd';
import CustomTableView from './CustomTableView';

let pageSizeOptions= ['10', '20', '30', '40', '50', '60'];

export default class CustomTable extends React.Component {
	constructor(props) {
		super(props);
		// console.log('this.props.data', JSON.stringify(this.props.dataSource));
		this.state = {
			bordered: props.config_data.bordered
				? props.config_data.bordered
				: false,
			loading: props.config_data.loading
				? props.config_data.loading
				: false,
			size: props.config_data.size ? props.config_data.size : 'default',
			title: props.config_data.title
				? () => props.config_data.title
				: undefined,
			showHeader: props.config_data.showHeader
				? props.config_data.showHeader
				: true,
			scroll: props.config_data.scroll
				? props.config_data.scroll
				: undefined,
			hasData: props.config_data.hasData
				? props.config_data.hasData
				: true,
			locale: props.config_data.locale
				? props.config_data.locale
				: undefined,
			rowSelect: props.config_data.rowSelect
				? props.config_data.rowSelect
				: false,
			//   pagination properties
			pagination:
				props.config_data && props.config_data.pagination_data == null
					? false
					: true,
			pagination_config: {
				position: props.position ? props.position : ['topRight'],
				pageSize:
					props.config_data &&
					props.config_data.pagination_data &&
					props.config_data.pagination_data.pageSize
						? props.config_data.pagination_data.pageSize
						: 20,
				size:
					props.config_data &&
					props.config_data.pagination_data &&
					props.config_data.pagination_data.size
						? props.config_data.pagination_data.size
						: 'small',
				showSizeChanger:
					props.config_data &&
					props.config_data.pagination_data &&
					props.config_data.pagination_data.showSizeChanger
						? props.config_data.pagination_data.showSizeChanger
						: false,
				showQuickJumper:
					props.config_data &&
					props.config_data.pagination_data &&
					props.config_data.pagination_data.showQuickJumper
						? props.config_data.pagination_data.showQuickJumper
						: false,
				hideOnSinglePage:
					props.config_data &&
					props.config_data.pagination_data &&
					props.config_data.pagination_data.hideOnSinglePage
						? props.config_data.pagination_data.hideOnSinglePage
						: false,
				showTotal: (total, range) =>
					range[0] +
					' - ' +
					range[1] +
					(this.props.of ? ' ' + this.props.of + ' ' : ' of ') +
					total +
					(this.props.items ? ' ' + this.props.items : ' items'),
				onShowSizeChange: (current, pageSize) => {
					this.onShowSizeChange(current, pageSize);
				},
				onChange: (page, pageSize) => {
					this.onChange(page, pageSize);
				},
				pageSizeOptions:
					props.config_data &&
					props.config_data.pagination_data &&
					props.config_data.pagination_data.pageSizeOptions
						? props.config_data.pagination_data.pageSizeOptions
						: pageSizeOptions,
				defaultCurrent: props.defaultCurrent ? props.defaultCurrent : 1,
				current: props.current ? props.current : 1,
			},
		};
	}

	onShowSizeChange(current, pageSize) {
		let that = this;
		let pagination_config = this.state.pagination_config;
		pagination_config['current'] = current;
		pagination_config['pageSize'] = pageSize;
		this.setState(
			{
				pagination_config: pagination_config,
			},
			() => {
				if (that.props.onPaginationChange) {
					that.props.onPaginationChange(current, pageSize);
				}
			}
		);
		// console.log('onShowSizeChange total', this.state.pagination_config.total);
		// console.log('onShowSizeChange current', current);
		// console.log('onShowSizeChange pageSize', pageSize);
	}

	onChange(page, pageSize) {
		let that = this;
		let pagination_config = this.state.pagination_config;
		pagination_config['current'] = page;
		this.setState(
			{
				pagination_config: pagination_config,
			},
			() => {
				if (that.props.onPaginationChange) {
					that.props.onPaginationChange(page, pageSize);
				}
			}
		);
		// console.log('onChange Page: ', page);
		// console.log('onChange pageSize: ', pageSize);
	}

	render() {
		const { state, props } = this;
		let rowSelection = {
			selectedRowKeys: props.selectedRowKeys,
			onChange: props.onChangeRowSelect,
		};
		// console.log('this.props.data_1', this.state);
		let total =
			props.config_data &&
			props.config_data.pagination_data &&
			props.config_data.pagination_data.total
				? props.config_data.pagination_data.total
				: props.dataSource
				? props.dataSource.length
				: 0;
		let paginationConfig = state.pagination_config;
		if (props.current) {
			paginationConfig.total = total;
			paginationConfig.current = props.current
				? props.current
				: state.pagination_config.current;
		}
		return (
			<CustomTableView
				{...this.state}
				loading={this.props.loading ? true : false}
				pagination={state.pagination ? paginationConfig : false}
				rowSelection={state.rowSelect ? rowSelection : undefined}
				columns={props.columns}
				dataSource={state.hasData ? props.dataSource : null}
				{...props}
			/>
			// <div>
			// <div className={"antD-table-class " + (this.props.dataSource && this.props.dataSource.length ? '' : 'mar-top-40')}>
			// {
			/* (() => {
            if (props.actionButton && props.actionButton.show_action_button) {
              return <Button className="normal-button" type={props.actionButton.primary ? 'primary' : 'default'} disabled={props.actionButton.disabled ? true : false} onClick={() => props.actionOnClick()}>{props.actionButton.text}</Button>
            } else if (props.actionButton && props.actionButton.show_action_link) {
              return <div className="link-button" onClick={() => props.actionOnClick()}> <Icon className="link-icon" type={props.actionButton.icon_type} />{props.actionButton.text}</div>
            }
          })()}
          {(() => {
            // console.log('props.dataSource', props.dataSource);
          })()}
          <Table
            {...this.state}
            total={total}
            loading={this.props.loading ? true : false}
            pagination={state.pagination ? state.pagination_config : false}
            rowSelection= {state.rowSelect ? rowSelection : undefined}
            columns={props.columns}
            dataSource={state.hasData ? props.dataSource : null} {...props}*/
			// />
			// </div>
			// </div>
		);
	}
}
