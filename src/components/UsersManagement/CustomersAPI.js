import { callFetch } from './ApiHandling';

async function retriveCustomerDetails(client_id) {
	try{
    let configData = {
      url : '/customers/'+ client_id +'/details',
      method : 'GET',
    }
    return await callFetch(configData);
  }catch(err){
    throw err;
	}	
}

async function retriveCustomerList(cust_id) {
  try{
    let configData = {
      url : '/vendors/'+cust_id+'/customers/list',
      method : 'GET',
    }
    return await callFetch(configData);
  }catch(err){
    throw err;
  } 
}

export { retriveCustomerDetails, retriveCustomerList };