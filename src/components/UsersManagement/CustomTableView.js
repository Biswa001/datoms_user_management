import React from 'react';
import { Icon as LegacyIcon } from '@ant-design/compatible';
import { Table, Button } from 'antd';

function shouldComponentUpdate(nextProps) {
	console.log('nextProps.dataSource ', nextProps.dataSource);
	if (nextProps.dataSource) {
		return true;
	}
}

function CustomTableView(props) {
	let rowSelection = {
		selectedRowKeys: props.selectedRowKeys,
		onChange: props.onChangeRowSelect,
	};
	console.log('props.dataSource view ', props.dataSource);
	return (
		<div>
			<div
				className={
					'antD-table-class ' +
					(props.dataSource && props.dataSource.length
						? ''
						: 'mar-top-40')
				}
			>
				{(() => {
					if (
						props.actionButton &&
						props.actionButton.show_action_button
					) {
						return (
							<Button
								className="normal-button"
								type={
									props.actionButton.primary
										? 'primary'
										: 'default'
								}
								disabled={
									props.actionButton.disabled ? true : false
								}
								onClick={() => props.actionOnClick()}
							>
								{props.actionButton.text}
							</Button>
						);
					} else if (
						props.actionButton &&
						props.actionButton.show_action_link
					) {
						return (
							<div
								className="link-button"
								onClick={() => props.actionOnClick()}
							>
								{' '}
								<LegacyIcon
									className="link-icon"
									type={props.actionButton.icon_type}
								/>
								{props.actionButton.text}
							</div>
						);
					}
				})()}
				{(() => {
					console.log('all_props', props);
				})()}
				<Table
					loading={props.loading ? true : false}
					pagination={
						props.pagination ? props.pagination_config : false
					}
					rowSelection={props.rowSelect ? rowSelection : undefined}
					columns={props.columns}
					dataSource={props.hasData ? props.dataSource : null}
				/>
			</div>
		</div>
	);
}

export default CustomTableView;
