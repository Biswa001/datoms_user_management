import React from 'react';
import {Layout, Row, Col, Alert, notification, Modal, Dropdown, Menu, Tag, Divider } from 'antd';
import AntButton from '../AntButton/index';
import CustomTable from './CustomTable';
// import TableList from '../TableList/index';
import { MoreOutlined } from '@ant-design/icons';
import Loading from '../Loading/index';
// import { ReactComponent as DotsVertical } from '../../imgs/dots_vertical.svg';
import { retriveCustomerDetails, retriveCustomerList } from './CustomersAPI';
import { retriveThingsList } from './ThingsAPI';
import { retriveUsers, deleteUser, updateUserStatus, retriveRoles } from './UsersAPI';
import AddUserDrawer from './AddUserDrawer';
import RoleListDrawer from './RoleListDrawer';
import UserManagementObjectData from './defaultConfigs';
import './style.less';
import _ from 'lodash';

import moment from 'moment';

const {Content} = Layout;
const { confirm } = Modal;

export default class UsersManagement extends React.Component {
	constructor(props) {
		super(props);

		let app_list_arr = [],
			app_drop = 'all',
			show_add_edit_role = true;

		if (props.details_customer_data && props.details_customer_data.application_details && props.details_customer_data.application_details.length > 0) {
			props.details_customer_data.application_details.map((application_detail) => {
				let appl_list = _.find(props.all_app_list, {id: application_detail.application_id});
				if (appl_list) {
					app_list_arr.push({
						id: appl_list.id,
						name: appl_list.name
					});
				}
			});
		}

		if (app_list_arr.length > 1) {
			app_drop = 'all';
		} else if (app_list_arr.length == 1) {
			app_drop = app_list_arr[0].id;
		}

		if (this.props.location.pathname.search('/customer-management') > -1) {
			if (props.match.params.app_id != 'all') {
				app_drop = parseInt(props.match.params.app_id);
			}
		}
		
		// console.log('app_drop__', app_drop);

		if (app_drop == 'all') {
			show_add_edit_role = false;
		}

		this.state = {
			app_drop: props.location.pathname.includes('/customer-management/') ? app_drop : props.location.pathname.includes('/datoms-x/') ? [12] : props.location.pathname.includes('/delivery-tracking/') ? [20] : props.location.pathname.includes('/dg-monitoring/') ? [16] : [17],
			UserManagementObjectData: UserManagementObjectData,
			loading: false,
			active_application: props.match.params.application_id ? props.match.params.application_id : null,
			collapse: true,
			selected_application_id_arr: props.location.pathname.includes('/customer-management/') ? [] : props.location.pathname.includes('/datoms-x/') ? [12] : props.location.pathname.includes('/delivery-tracking/') ? [20] : props.location.pathname.includes('/dg-monitoring') ? [16] : [17],
			selected_user: [],
			all_app_list: props.all_app_list ? props.all_app_list : [],
			selected_application: [],
			table_data: [],
			show_add_edit_role: props.location.pathname.includes('/user-management/') ? true : show_add_edit_role,
			access_key: ['UserManagement:UserManagement', 'UserManagement:ManageUser'],
			show_add_draw: props.location.pathname.includes('/add') ? true : undefined,
			roleDetailsVisible: false,
			table_load: true
		};
		this.platform_slug = props.location && props.location.pathname.includes('/datoms-x/') ? '/datoms-x' : (props.location.pathname.includes('/iot-platform/') ? '/iot-platform' : props.location.pathname.includes('/dg-monitoring/') ? '/dg-monitoring' : '/delivery-tracking');
		/*if (props.location.pathname.includes('/iot-platform') || props.location.pathname.includes('/datoms-x')) {
			this.user_slug = '/settings/user-management';
		} else {
			this.user_slug = '/user-management';
		}*/
		this.user_slug = '/user-management';

		// console.log('user_mgt_props', this.props);
		// console.log('UserManagementObjectData', UserManagementObjectData);
		this.closeRoleDrawer = this.closeRoleDrawer.bind(this);
	}

	changeLanguage(lng, LoggerObjectData) {
		const { t, i18n } = this.props;
		i18n.changeLanguage(lng);
	}

	translateFunction(t, UserManagementObjectData) {
		let menuItemsArray = [];
		// if(this.props.head_side_object_data) {
		// 	console.log('UserManagementObjectData1234', this.props.head_side_object_data);
		// 	if(this.props.head_side_object_data.header_component && this.props.head_side_object_data.header_component.header && this.props.head_side_object_data.header_component.header.page_name) {
		// 		this.props.head_side_object_data.header_component.header.page_name = t('user_management')
		// 	}
		// 	if(this.props.head_side_object_data.header_component && this.props.head_side_object_data.header_component.sider && this.props.head_side_object_data.header_component.sider.menu_items && this.props.head_side_object_data.header_component.sider.menu_items.length) {
		// 		this.props.head_side_object_data.header_component.sider.menu_items.map((menu_items)=> {
		// 			menuItemsArray.push({
		// 				key: menu_items.key,
		// 				icon_type: menu_items.icon_type,
		// 				page_name: menu_items.key === "dashboard" ? t('dashboard') : menu_items.key === "loggers" ? t('loggers') : menu_items.key === "user-management" ? t('user_management') : '' ,
		// 				url: menu_items.url,
		// 			})
		// 		})
		// 		this.props.head_side_object_data.header_component.sider.menu_items = menuItemsArray
		// 	}
		// }
		let headDataArray = [];
		if(UserManagementObjectData && UserManagementObjectData.application_user_table && UserManagementObjectData.application_user_table.head_data && UserManagementObjectData.application_user_table.head_data.length) {
			UserManagementObjectData.application_user_table.head_data.map((head_data) => {
				headDataArray.push({
					title: head_data.key === 'name' ? t('name') : head_data.key === 'role' ? t('role') : head_data.key === 'things' ? t('loggers') : head_data.key === "report_to" ? t('reports_to') : head_data.key === 'action' ? t('action') : '',
					dataIndex: head_data.dataIndex,
					key: head_data.key,
					sorter: head_data.sorter,
					width: head_data.width,
					align: head_data.align,
					render: head_data.render
				})
				/*if(head_data.key==='action') {
					console.log('head_data', head_data);
					head_data.render = (a, b, c) => (
						<Dropdown 
						  overlay={
							<Menu>
							  <Menu.Item key="action-1">{(b.status == 1 ? 'Deactivate' : 'Activate')}</Menu.Item>
							  <Menu.Item key="action-2">{t('edit')}</Menu.Item>
							  <Menu.Item key="action-3">Delete</Menu.Item>
							</Menu>
						  } trigger={['click']} placement="bottomLeft">
						  <DotsVertical className="menu-icon"/>
						</Dropdown>
					)
				}*/
			})
			console.log('headDataArray',headDataArray);
			UserManagementObjectData.application_user_table.head_data = headDataArray;
		}
		let customerDetailsForm = [];
		if(UserManagementObjectData && UserManagementObjectData.add_user_form && UserManagementObjectData.add_user_form.customer_details_form && UserManagementObjectData.add_user_form.customer_details_form.length) {
			UserManagementObjectData.add_user_form.customer_details_form.map((customer_details_form) =>{
				customerDetailsForm.push({
					key: customer_details_form.key,
					label: customer_details_form.key  === "first_name" ? (t("first_name") + ' *') : customer_details_form.key  === "last_name" ? t("last_name") : customer_details_form.key  === "designation" ? t("designation") : customer_details_form.key  === "email" ? (t("email") + ' *') : customer_details_form.key  === "phone_no" ? (t("contact_no")) : '',
					required: customer_details_form.required,
					message: customer_details_form.key  === "first_name" ? t("please_enter_first_name") : customer_details_form.key  === "last_name" ? t("please_enter_last_name") : customer_details_form.key  === "designation" ? t("please_enter_designaton") : customer_details_form.key  === "email" ? t("please_enter_email") : customer_details_form.key  === "phone_no" ? (t("contact_no") + ' *') : '',
					initial_value: customer_details_form.initial_value,
				})
			})
			UserManagementObjectData.add_user_form.customer_details_form = customerDetailsForm;
		}
		if(UserManagementObjectData && UserManagementObjectData.add_user_form && UserManagementObjectData.add_user_form.draw_buttons) {
			if(UserManagementObjectData.add_user_form.draw_buttons.submit && UserManagementObjectData.add_user_form.draw_buttons.submit.length) {
				UserManagementObjectData.add_user_form.draw_buttons.submit[0].text = t('submit')
			}
			if(UserManagementObjectData.add_user_form.draw_buttons.cancel && UserManagementObjectData.add_user_form.draw_buttons.cancel.length) {
				UserManagementObjectData.add_user_form.draw_buttons.cancel[0].text = t('cancel')
			}
		}
		if(UserManagementObjectData && UserManagementObjectData.user_add_button) {
			UserManagementObjectData.user_add_button[0].text = t('add_user')
		}
		if(UserManagementObjectData && UserManagementObjectData.role_button) {
			UserManagementObjectData.role_button[0].text = t('roles')
		}
		if(UserManagementObjectData && UserManagementObjectData.role_view_draw && UserManagementObjectData.role_view_draw.add_button && UserManagementObjectData.role_view_draw.add_button.text) {
			UserManagementObjectData.role_view_draw.add_button.text = t('new_role');
		}
		if(UserManagementObjectData && UserManagementObjectData.role_view_draw && UserManagementObjectData.role_view_draw.add_role_form && UserManagementObjectData.role_view_draw.add_role_form.draw_buttons) {
			if(UserManagementObjectData.role_view_draw.add_role_form.draw_buttons.submit &&  UserManagementObjectData.role_view_draw.add_role_form.draw_buttons.submit.length) {
				UserManagementObjectData.role_view_draw.add_role_form.draw_buttons.submit[0].text = t('submit')
			}
			if(UserManagementObjectData.role_view_draw.add_role_form.draw_buttons.cancel &&  UserManagementObjectData.role_view_draw.add_role_form.draw_buttons.cancel.length) {
				UserManagementObjectData.role_view_draw.add_role_form.draw_buttons.cancel[0].text = t('cancel')
			}
		} 
	}

	componentDidMount() {
		const {t}=this.props;
		// this.getApplicationsDataFunction();
		document.title = 'User Management - Datoms IOT Platform';
		if (this.props.location.pathname.includes('/user-management/')) {
			this.fetchRoleData(this.props.client_id, this.state.app_drop);
		}
		if (this.props.location.pathname.includes('/customer-management/')) {
			this.fetchCustomerDetails();
		}
	}

	collapseState(collapse) {
		this.setState({
			collapse: collapse
		});
	}

	/*
	 * This function calls the API to get user list.
	 * @param  int, int i:e client_id and application_id.
	*/
	async fetchUserData(client_id, app_id) {
		let that = this;
		this.setState({
			table_load: true
		});
		let application_id = app_id;
		if (that.props.location.pathname.includes('/customer-management/')) {
			application_id = 'all';
		}
		let response = await retriveUsers(client_id, application_id);
		if (response.status === 403) {
			that.setState({
				unauthorised_access: true,
				table_load: false,
				unauthorised_access_msg: response.message
			});
		} else if (response.status === 'success') {
			// console.log('user_data', response);
			let dataUser = [];

			if (response.user_details && response.user_details.length) {
				response.user_details.map((user) => {
					console.log('usersss', user);
					let temp_users = [],
						userStatus = 0;
					
					let last_login = !isNaN(user.last_login) && user.last_login > 0 ? moment.unix(user.last_login).tz("Asia/Kolkata").format('HH:mm, DD MMM') : 'Invitation Sent';
					
					if (user.activation_status_details && user.activation_status_details.length) {
						if (this.props.location.pathname.includes('/user-management')) {
							let found = _.find(user.activation_status_details, {application_id: app_id});
							if (found) {
								userStatus = found.is_active;
							}
						} else {
							user.activation_status_details.map((status_details) => {
								if (status_details.is_active) {
									userStatus = 1;
								}
							});
						}
					}

					dataUser.push({
						'key': user.contact_id,
						'name': user.first_name + ' ' + user.last_name,
						'login': last_login,
						'things': user.things ? user.things : [],
						'id': user.contact_id,
						'first_name': user.first_name,
						'last_name': user.last_name,
						'designation': user.designation,
						'email_id': user.email,
						'mobile_no': user.mobile_no,
						'status': user.status,
						'role_details': user.role_details,
						'applications': user.applications,
						'industries': user.industries,
						'industry_sets': user.industry_sets,
						'activation_status_details': user.activation_status_details,
						'status': userStatus
					});
				});
			}

			console.log('dataUser__', dataUser);

			that.other_contact = [];

			if (that.props.details_customer_data && that.props.details_customer_data.contact_details && that.props.details_customer_data.contact_details.length > 0) {
				if (response.user_details && response.user_details.length) {
					// console.log('trueee');
					that.props.details_customer_data.contact_details.map((contact) => {
						let found = _.find(response.user_details, {contact_id: contact.contact_id});
						if (found == undefined) {
							that.other_contact.push(contact);
						}
					})
				} else {
					// console.log('trueee falsee');
					that.other_contact = that.props.details_customer_data.contact_details;
				}
			}

			// console.log('other_contact_', that.other_contact);

			// console.log('dataUser_', dataUser);

			let UserManagementObjectData = this.state.UserManagementObjectData;
			if (this.props.location.pathname.includes('/user-management')) {
				if (this.props.location.pathname.includes('/datoms-x') || this.props.location.pathname.includes('/iot-platform')) {
					UserManagementObjectData.iot_user_table.head_data[1].render = (data, row_data) => (
						<div>
							{(() => {
								// console.log('row_data_', row_data);
								let app_role = [];
								if (row_data.role_details && row_data.role_details.length && this.state.all_role_datas) {
									row_data.role_details.map((role) => {
										let role_arr = _.find(this.state.all_role_datas.roles_list, {id: role.role_id});
										if (role_arr) {
											app_role.push({
												role_name: role_arr.name
											})
										}
									})
								}

								// console.log('app_role_', app_role);

								if (app_role.length) {
									return app_role.map((details) => {
										return <div className="text-center">{details.role_name}</div>
									})
								}

							})()}
						</div>
					);

					UserManagementObjectData.iot_user_table.head_data[2].render = (data, row_data) => (
						<div className="dsp-flex">
							{(() => {
								// console.log('row_data_', row_data);
								let app_dets = [];
								if (row_data.applications && row_data.applications.length) {
									row_data.applications.map((app_id) => {
										let app_arr = _.find(this.state.application_name_array, {id: app_id});
										if (app_arr) {
											app_dets.push(app_arr.name);
										}
									})
								}

								if (app_dets.length) {
									return app_dets.map((details) => {
										return <div className="app-role-sec">
											<div>{details}</div>
										</div>
									})
								}

							})()}
						</div>
					);

					UserManagementObjectData.iot_user_table.head_data[3].render = (data, row_data) => (
						<div>
							{(() => {
								if (data.includes('*')) {
									return <div>{this.state.all_customer_data.length}</div>
								} else {
									return <div>{data.length}</div>
								}
							})()}
						</div>
					);

					UserManagementObjectData.iot_user_table.head_data[UserManagementObjectData.iot_user_table.head_data.length - 1].render = (data, row_data, index) => (
						<Dropdown 
							overlay={
								(() => {
									if (this.props.getViewAccess(this.state.access_key)) {
										return <Menu>
											<Menu.Item key="action-3" onClick={() => this.showStatusConfirm(row_data)}>{(row_data.status == 1 ? 'Deactivate' : 'Activate')}</Menu.Item>
											<Menu.Item key="action-1" onClick={() => this.editUser(row_data)}>Edit</Menu.Item>
											<Menu.Item key="action-2" onClick={() => this.showDeleteConfirm(row_data)}>Delete</Menu.Item>
										</Menu>;
									} else {
										return <Menu>
											<Menu.Item className="block" key="action-3">{(row_data.status == 1 ? 'Deactivate' : 'Activate')}</Menu.Item>
											<Menu.Item className="block" key="action-1">Edit</Menu.Item>
											<Menu.Item className="block" key="action-2">Delete</Menu.Item>
										</Menu>;
									}
								})()
							} trigger={['click']} placement="bottomLeft">
							{/*<DotsVertical className="menu-icon"/>*/}
							<MoreOutlined className="menu-icon"/>
						</Dropdown>
					);
				} else {
					UserManagementObjectData.application_user_table.head_data[1].render = (data, row_data) => (
						<div>
							{(() => {
								// console.log('row_data_', row_data);
								let app_role = [];
								if (row_data.role_details && row_data.role_details.length && this.state.all_role_datas) {
									row_data.role_details.map((role) => {
										let role_arr = _.find(this.state.all_role_datas.roles_list, {id: role.role_id});
										if (role_arr) {
											app_role.push({
												role_name: role_arr.name
											})
										}
									})
								}

								// console.log('app_role_', app_role);

								if (app_role.length) {
									return app_role.map((details) => {
										return <div className="text-center">{details.role_name}</div>
									})
								}

							})()}
						</div>
					);

					if (this.props.location.pathname.includes('/delivery-tracking/')) {
						UserManagementObjectData.application_user_table.head_data[2].title = <span className="" dangerouslySetInnerHTML={{__html: 'Loggers'}} />;
					}

					UserManagementObjectData.application_user_table.head_data[2].render = (data, row_data) => (
						<div>
							{(() => {
								let thing_names = [];
								for (let i = 0; i <= 3; i++) {
									if (row_data.things[i]) {
										let found = _.find(this.state.things_list, {id: row_data.things[i]});
										if (found) {
											thing_names.push(found.name);
										}
									}
								}

								if (row_data.things.length > 4) {
									thing_names.push('+ ' + (data.length - 4));
								}

								if (thing_names.length) {
									return thing_names.map((thing) => {
										return <Tag>{thing}</Tag>
									});
								} else if (row_data.things === '*') {
									return <Tag>All</Tag>
								} else {
									return <span>-</span>
								}
							})()}
						</div>
					);

					UserManagementObjectData.application_user_table.head_data[3].render = (data, row_data) => (
						<div>
							{(() => {
								let name = '';
								if (row_data.role_details && row_data.role_details.length) {
								 	row_data.role_details.map((data) => {
								 		if (data.application_id == this.props.application_id) {
									 		let found = _.find(dataUser, {id: data.reports_to});
									 		if (found) {
									 			name = found.name
									 		}
								 		}
									});
								}

								if (name != '') {
									return <span>{name}</span>
								} else {
									return <span>-</span>
								}
							})()}
						</div>
					);

					UserManagementObjectData.application_user_table.head_data[UserManagementObjectData.application_user_table.head_data.length - 1].render = (data, row_data, index) => (
						<Dropdown 
							overlay={
								(() => {
									if (this.props.getViewAccess(this.state.access_key)) {
										return <Menu>
											<Menu.Item key="action-3" onClick={() => this.showStatusConfirm(row_data)}>{(row_data.status == 1 ? this.props.t('deactivate') : this.props.t('activate'))}</Menu.Item>
											<Menu.Item key="action-1" onClick={() => this.editUser(row_data)}>{this.props.t('edit')}</Menu.Item>
											<Menu.Item key="action-2" onClick={() => this.showDeleteConfirm(row_data)}>{this.props.t('delete')}</Menu.Item>
										</Menu>;
									} else {
										return <Menu>
											<Menu.Item className="block" key="action-3">{(row_data.status == 1 ? this.props.t('deactivate') : this.props.t('activate'))}</Menu.Item>
											<Menu.Item className="block" key="action-1">{this.props.t('edit')}</Menu.Item>
											<Menu.Item className="block" key="action-2">{this.props.t('delete')}</Menu.Item>
										</Menu>;
									}
								})()
							} trigger={['click']} placement="bottomLeft">
							{/*<DotsVertical className="menu-icon"/>*/}
							<MoreOutlined className="menu-icon"/>
						</Dropdown>
					);
				}
			} else {
				UserManagementObjectData.user_table.row_data[1].render = (data, row_data) => (
					<div className="dsp-flex">
						{(() => {
							// console.log('row_data_', row_data);
							let app_role = [];
							if (row_data.role_details && row_data.role_details.length && this.state.selected_all_roles) {
								row_data.role_details.map((role) => {
									let app_arr = _.find(this.state.all_app_list, {id: role.application_id});
									let role_arr = _.find(this.state.selected_all_roles, {id: role.role_id});
									// console.log('app_arr__', app_arr);
									// console.log('role_arr__', role_arr);
									if (app_arr && role_arr) {
										app_role.push({
											application_name: app_arr.name,
											role_name: role_arr.name
										})
									}
								})
							}

							// console.log('app_role_', app_role);

							if (app_role.length) {
								return app_role.map((details) => {
									return <div className="app-role-sec">
										<div className="app-sec">{details.application_name}</div>
										<div className="role-sec">{details.role_name}</div>
									</div>
								})
							}
						})()}
					</div>
				);

				UserManagementObjectData.user_table.head_data[UserManagementObjectData.user_table.head_data.length - 1].render = (data, row_data, index) => (
					<Dropdown 
						overlay={
							(() => {
								if (this.props.getViewAccess(this.state.access_key)) {
									return <Menu>
										<Menu.Item key="action-3" onClick={() => this.showStatusConfirm(row_data)}>{(row_data.status == 1 ? this.props.t('deactivate') : this.props.t('activate'))}</Menu.Item>
										<Menu.Item key="action-1" onClick={() => this.editUser(row_data)}>{this.props.t('edit')}</Menu.Item>
										<Menu.Item key="action-2" onClick={() => this.showDeleteConfirm(row_data)}>{this.props.t('delete')}</Menu.Item>
									</Menu>;
								} else {
									return <Menu>
										<Menu.Item className="block" key="action-3">{(row_data.status == 1 ? this.props.t('deactivate') : this.props.t('activate'))}</Menu.Item>
										<Menu.Item className="block" key="action-1">{this.props.t('edit')}</Menu.Item>
										<Menu.Item className="block" key="action-2">{this.props.t('delete')}</Menu.Item>
									</Menu>;
								}
							})()
						} trigger={['click']} placement="bottomLeft">
						{/*<DotsVertical className="menu-icon"/>*/}
						<MoreOutlined className="menu-icon"/>
					</Dropdown>
				);
			}
		
			// console.log('access_options', access_options);
			// console.log('access_list_options', access_list_options);
			that.setState({
				unauthorised_access: false,
				all_user_data: response,
				user_list: response.user_details,
				dataUser: dataUser,
				roles_list: response.roles_list,
				station_group_list: response.station_group_list,
				station_list: response.station_list,
				user_groups: response.user_groups,
				all_access_list: response.access_list,
			},() => {
				that.retriveThingsListFunction(that.props.client_id, that.state.app_drop);
				/*if (that.props.location.pathname.includes('/user-management') && (!that.props.location.pathname.includes('/datoms-x') && !that.props.location.pathname.includes('/iot-platform'))) {
				} else {
					that.filterTableData(that.state.app_drop);
				}*/
			});
		} else {
			that.openNotification('error', response.message);
			that.setState({
				unauthorised_access: false,
				loading: true,
				table_load: false,
				error_API: true,
				error_API_msg: response.message,
			});
		}
	}

	/*
	 * This function calls the API to get things list.
	 * @param  int, int i:e client_id and application_id.
	*/
	async retriveThingsListFunction(client_id, app_id) {
		let that = this;
		let data = {
			client_id: client_id,
			application_id: app_id
		};

		let response = await retriveThingsList(data);
		if (response.status === 'success') {
			let things_list = [];
			if (response.things && response.things.length) {
				response.things.map((thing) => {
					things_list.push({
						id: thing.id,
						name: thing.name
					});
				});
			}

			that.setState({
				table_load: false,
				things_all_data: response.things,
				things_list: things_list,
			}, () => {
				that.filterTableData(that.state.app_drop);
				// console.log('logger_data', this.state.logger_all_data);
			});
		} else if (response.status == 403) {
			that.setState({
				loading_icon: true,
				table_load: false,
				unauthorised_access_msg: response.message
			}, () => {
				that.openNotification('error', that.state.unauthorised_access_msg);
			});
		}
	}

	/*
	 * This function calls the API to get roles list.
	 * @param  int, int i:e client_id and application_id.
	*/
	async fetchRoleData(client_id, app_id) {
		let that = this;
		let response = await retriveRoles(client_id, app_id);
		if (response.status === 403) {
			that.setState({
				unauthorised_access: true,
				unauthorised_access_msg: response.message
			});
		} else if (response.status === 'success') {
			// console.log('user_roles_data', response);
			// console.log('response status code1', response_status);
			// response.roles_list =[];

			that.setState({
				// unauthorised_access: false,
				all_role_datas: response,
				all_roles_list: response.roles_list,
				// filtered_stations: response.all_stations,
			}, () => {
				if (that.props.location.pathname.includes('/user-management') && (that.props.location.pathname.includes('/datoms-x') || that.props.location.pathname.includes('/iot-platform'))) {
					that.fetchCustomerList ();
				} else {
					that.fetchCustomerDetails();
				}
			});
		} else {
			that.openNotification('error', response.message);
			that.setState({
				unauthorised_access: false,
				loading: true,
				error_API: true,
				error_API_msg: response.message,
			});
		}
	}

	/*
	 * This function calls the API to get a customers list and applicaion access liat.
	*/
	async fetchCustomerList() {
		let that = this;
		let response = await retriveCustomerList(this.props.client_id);
		if (response.status === 403) {
			that.setState({
				unauthorised_access: true,
				unauthorised_access_msg: response.message
			});
		} else if (response.status === 'success') {
			// console.log('option_data1', response);

			let application_name_arr = [],
				customer_list = [];
			if (response.applications && response.applications.length) {
				response.applications.map((applications) => {
					application_name_arr.push({
						id: applications.id,
						name: applications.name
					});
				});
			}

			if (response.customers && response.customers.length) {
				response.customers.map((customer) => {
					if (customer.id !== 1) {
						customer_list.push(customer);
					}
				});
			}

			that.setState({
				unauthorised_access: false,
				loaded_data: true,
				application_name_arr: application_name_arr,
				all_customer_data: customer_list,
				all_app_list: response.applications,
				
			}, () => {
				if (that.props.location.pathname.includes('/user-management')) {
					that.fetchCustomerDetails();
				}
			});
		} else {
			that.openNotification('error', response.message);
			that.setState({
				unauthorised_access: false,
				loading: true,
				error_API: true,
				error_API_msg: response.message,
			});
		}
	}

	/*
	 * This function calls the API to get a customer's details.
	 * @param  integer .id of the client.
	*/
	async fetchCustomerDetails() {
		let that = this;
		let response = await retriveCustomerDetails(this.props.client_id);
		if (response.status === 403) {
			that.setState({
				unauthorised_access: true,
				unauthorised_access_msg: response.message
			});
		} else if (response.status === 'success') {
			// console.log('customer_data2', response);
			let selected_all_roles = [];
			if (response.application_details && response.application_details.length) {
				response.application_details.map((application) => {
					if (application.role_details && application.role_details.length) {
						application.role_details.map((roles) => {
							selected_all_roles.push({
								id: roles.role_id,
								name: roles.role_name
							});
						});
					}
				});
			}

			let nonUserContactList = [];

			if (response.contact_details && response.contact_details.length) {
				response.contact_details.map((contact) => {
					if (!contact.is_user) {
						nonUserContactList.push({
							value: contact.contact_id,
							name: contact.first_name + ' ' + contact.last_name
						});
					}
				});
			}

			let UserManagementObjectData = this.state.UserManagementObjectData;
			// console.log('add_user_form__', add_user_form);
			UserManagementObjectData.add_user_form.contact_select[0].options = nonUserContactList;

			selected_all_roles = _.uniqBy(selected_all_roles, 'id');
			// console.log('selected_all_roles_', selected_all_roles);
			this.setState({
				unauthorised_access: false,
				all_contact_details: response.contact_details,
				selected_all_roles: selected_all_roles,
				selected_all_app_list: response.application_details,
				UserManagementObjectData: UserManagementObjectData
			},() => {
				that.fetchUserData(this.props.client_id, this.state.app_drop);
			});
		} else {
			that.openNotification('error', response.message);
			that.setState({
				unauthorised_access: false,
				loading: true,
				error_API: true,
				error_API_msg: response.message,
			});
		}
	}

	/*
	 * This function calls the API to delete the user.
	 * @param  {Object} user user data.
	*/
	async deleteUserFunction(user) {
		let that = this;
		// console.log('user_del', user);
		let application_arr = [];
		if (this.state.app_drop == 'all') {
			if (user.role_details && user.role_details.length) {
				user.role_details.map((role) => {
					application_arr.push(role.application_id);
				});
			}
		} else {
			application_arr.push(parseInt(this.state.app_drop));
		}

		let response = await deleteUser(application_arr, this.props.client_id, user.id);
		if (response.status === 403) {
			that.setState({
				unauthorised_access: true,
				unauthorised_access_msg: response.message
			});
		} else if (response.status === 'success') {
			that.openNotification('success', 'User deleted successfully');
			that.fetchUserData(that.props.client_id, that.state.app_drop);
		} else {
			that.openNotification('error', response.message);
			that.setState({
				unauthorised_access: false,
				loading: true,
				error_API: true,
				error_API_msg: response.message,
			});
		}
	}

	editUser(user) {
		this.setState({
			drawCreateVisible: true,
			add_user: false,
			edit_user: true,
			show_add_draw: false,
			selected_user_data: user
		}, () => {
			if (this.props.location.pathname.search('/customer-management') > -1) {
				if (this.state.app_drop == 'all') {
					this.props.history.push(this.platform_slug + '/customer-management/' + this.props.client_id + '/applications/all/users/'+ user.id +'/edit');
				} else {
					this.props.history.push(this.platform_slug + '/customer-management/' + this.props.client_id + '/applications/'+ parseInt(this.state.app_drop) +'/users/'+ user.id +'/edit');
				}
			} else {
				this.props.history.push(this.platform_slug + this.user_slug +'/users/'+ user.id +'/edit');
			}
		});
	}

	/*
	 * This function calls the API to change the status of the user.
	 * @param  {Object} user user data.
	*/
	async setStatusUser(user) {
		// console.log('setStatusUser', user);
		let that = this,
			application_ids = [];
		if (this.props.location.pathname.includes('/customer-management')) {
			if (this.state.app_drop == 'all') {
				if (user.activation_status_details.length) {
					user.activation_status_details.map((status_details) => {
						application_ids.push(status_details.application_id);
					});
				}
			} else {
				application_ids = [this.state.app_drop];
			}
		}

		let data = {
			client_id: this.props.client_id,
			user_id: user.id,
			application_details: this.props.location.pathname.includes('/user-management') ? [this.state.app_drop] : application_ids,
			is_active: user.status == 1 ? 0 : 1
		};

		// console.log('setStatusUser data', data);

		let response = await updateUserStatus(data);
		if (response.status === 403) {
			that.setState({
				unauthorised_access: true,
				unauthorised_access_msg: response.message
			});
		} else if (response.status === 'success') {
			that.openNotification('success', 'User status updated successfully');
			that.fetchUserData(that.props.client_id, that.state.app_drop);
		} else {
			that.openNotification('error', response.message);
			that.setState({
				unauthorised_access: false,
				loading: true,
				error_API: true,
				error_API_msg: response.message,
			});
		}
	}

	openNotification(type, msg) {
		notification[type]({
			message: msg,
			// description: msg,
			placement: 'bottomLeft',
			className: 'alert-' + type,
		});
	};

	/*
	 * This function shows an confirm modal for status update.
	 * @param  {Object} user user data.
	*/
	showStatusConfirm(user) {
		let that = this;
		// console.log('showStatusConfirm', user);
		confirm({
			title: 'Do you want to ' + (user.status == 1 ? 'deactivate' : 'activate') + ' ?',
			content: user.first_name + ' ' + user.last_name,
			okText: this.props.t('yes'),
    	cancelText: this.props.t('no'),
			onOk() {
				that.setStatusUser(user);
			},
			onCancel() {}
		});
	}

	/*
	 * This function opens the Drawer to Add or Edit the user.
	*/
	addNewUser() {
		// this.props.history.push(this.platform_slug + '/users/add/' + this.props.history.location.search);
		this.setState({
			drawCreateVisible: true,
			add_user: true,
			edit_user: false,
			show_add_draw: true,
			set_status_user: false,
			all_selected_industry: false,
			all_selected_set: false
		}, () => {
			if (this.props.location.pathname.search('/customer-management') > -1) {
				// console.log('platform_slug', this.platform_slug);
				this.props.history.push(this.platform_slug + '/customer-management/' + this.props.client_id + '/applications/'+ this.state.app_drop + '/users/add');
			} else {
				this.props.history.push(this.platform_slug + this.user_slug +'/users/add');
			}
		});
	}

	closeUserDraw() {
		this.setState({
			drawCreateVisible: false,
			add_user: true,
			edit_user: false,
			show_add_draw: false,
			selected_user_data: undefined
		}, () => {
			if (this.props.location.pathname.search('/customer-management') > -1) {
				this.props.history.push(this.platform_slug + '/customer-management/' + this.props.client_id + '/applications/'+ this.state.app_drop +'/users/view');
			} else {
				this.props.history.push(this.platform_slug + this.user_slug +'/users/view');
			}
		})
	}

	/*
	 * This function shows an confirm modal for status update.
	 * @param  {Object} user user data.
	*/
	showDeleteConfirm(user) {
		let that = this;
		let content_text = '';
		if (this.state.app_drop == 'all') {
			content_text = user.first_name + ' ' + user.last_name + ' from all applications';
		} else {
			let app_detail = _.find(this.props.all_app_list, {id: user.id});
			if (app_detail) {
				content_text = user.first_name + ' ' + user.last_name + ' from ' + app_detail.name;
			}
		}
		// that.props.history.push(that.platform_slug + '/users/' + user.id + '/delete/' + that.props.history.location.search);
		// console.log('showDeleteConfirm', user);
		confirm({
			title: this.props.t('confirm_delete_msg'),
			content: content_text,
			okText: this.props.t('yes'),
    	cancelText: this.props.t('no'),
			onOk() {
				that.deleteUserFunction(user);
			},
			onCancel() {}
		});
	}

	/*
	 * This function opens the Drawer to show list of roles.
	*/
	openRoleDraw() {
		// console.log('this.platform_slug', this.platform_slug);
		this.setState({
			roleDetailsVisible: true
		}, () => {
			if (this.props.location.pathname.search('/customer-management') > -1) {
				if (this.state.app_drop == 'all') {
					this.props.history.push(this.platform_slug + '/customer-management/' + this.props.match.params.customer_id + '/applications/all/roles/view');
				} else {
					this.props.history.push(this.platform_slug + '/customer-management/' + this.props.match.params.customer_id + '/applications/'+ parseInt(this.state.app_drop) +'/roles/view');
				}
			} else {
				this.props.history.push(this.platform_slug + this.user_slug +'/roles/view');
			}
		});
	}

	/*
	 * This function called when the button is blocked.
	*/
	blockedButton() {}

	/*
	 * This function generates user list object for table.
	 * @param  {Object} user user data.
	*/
	filterTableData(value) {
		// console.log('filterTableData', value);
		if (this.state.dataUser && this.state.dataUser.length) {
			let user_table_data = [];
			// console.log('data_user_', this.state.dataUser);
			if (value == 'all') {
				this.state.dataUser.map((user) => {
					// console.log('dataUser1', user);
					if (user.role_details && user.role_details.length) {
						user_table_data.push({
							'key': user.key,
							'name': user.first_name + ' ' + user.last_name,
							'login': user.login,
							'id': user.id,
							'first_name': user.first_name,
							'last_name': user.last_name,
							'designation': user.designation,
							'email_id': user.email_id,
							'mobile_no': user.mobile_no,
							'status': user.status,
							'role_details': user.role_details,
							'all_role_details': user.role_details,
							'activation_status_details': user.activation_status_details,
							'applications': user.applications,
							'industries': user.industries,
							'industry_sets': user.industry_sets,
							'things': user.things
						});	
					}
				});

				this.setState({
					user_table_data: user_table_data
				}, () => {
					// console.log('user_table_data', this.state.user_table_data);
				});
			} else {
				// console.log('filterTableData else');
				let filter_data = [];
				this.state.dataUser.map((user) => {
					let found = _.find(user.activation_status_details, {application_id: parseInt(value)});
					let status = 0;
					if (found) {
						if (found.is_active) {
						// console.log('dataUser2', found);
							status = 1;
						}
					}
					if (user.role_details && user.role_details.length) {
						let role_det = []
						user.role_details.map((app) => {
							if (app.application_id == parseInt(value)) {
								role_det = _.filter(user.role_details, {application_id: parseInt(value)});
								// console.log('role_det_', status);
							}
						});
						if (role_det.length) {
							user_table_data.push({
								'key': user.key,
								'name': user.first_name + ' ' + user.last_name,
								'login': user.login,
								'id': user.id,
								'first_name': user.first_name,
								'last_name': user.last_name,
								'designation': user.designation,
								'email_id': user.email_id,
								'mobile_no': user.mobile_no,
								'status': status,
								'role_details': role_det,
								'all_role_details': user.role_details,
								'activation_status_details': user.activation_status_details,
								'applications': user.applications,
								'industries': user.industries,
								'industry_sets': user.industry_sets,
								'things': user.things
							});
						}
					}
				});

				let UserManagementObjectData = this.state.UserManagementObjectData;
				if (this.props.location.pathname.includes('/user-management')) {
					if (this.props.location.pathname.includes('/datoms-x') || this.props.location.pathname.includes('/iot-platform')) {
						UserManagementObjectData.iot_user_table.row_data = user_table_data.length ? user_table_data : undefined;
					} else {
						UserManagementObjectData.application_user_table.row_data = user_table_data.length ? user_table_data : undefined;
					}
				} else {
					UserManagementObjectData.user_table.row_data = user_table_data.length ? user_table_data : undefined;
				}

				this.setState({
					user_table_data: user_table_data,
					UserManagementObjectData: UserManagementObjectData
				}, () => {
					console.log('UserManagementObjectData', this.state.UserManagementObjectData);
					if (this.props.location.pathname.search('/users/'+ this.props.match.params.user_id +'/edit') > -1) {
						let data = _.find(this.state.user_table_data, {key: parseInt(this.props.match.params.user_id)});
						// console.log('dataaa__', data);
						if (data) {
							this.editUser(data);
						}
					}
				});
			}
		} else {
			let UserManagementObjectData = this.state.UserManagementObjectData;
			if (this.props.location.pathname.includes('/user-management')) {
				if (this.props.location.pathname.includes('/datoms-x') || this.props.location.pathname.includes('/iot-platform')) {
					UserManagementObjectData.iot_user_table.row_data = undefined;
				} else {
					UserManagementObjectData.application_user_table.row_data = undefined;
				}
			} else {
				UserManagementObjectData.user_table.row_data = undefined;
			}
			this.setState({
				user_table_data: [],
				UserManagementObjectData: UserManagementObjectData
			});
		}
	}

	openRoleDrawer() {
		// console.log('this.platform_slug', this.platform_slug);
		this.setState({
			roleDetailsVisible: true
		}, () => {
			if (this.props.location.pathname.search('/customer-management') > -1) {
				if (this.state.app_drop == 'all') {
					this.props.history.push(this.platform_slug + '/customer-management/' + this.props.match.params.customer_id + '/applications/all/roles/view');
				} else {
					this.props.history.push(this.platform_slug + '/customer-management/' + this.props.match.params.customer_id + '/applications/'+ parseInt(this.state.app_drop) +'/roles/view');
				}
			} else {
				this.props.history.push(this.platform_slug + this.user_slug +'/roles/view');
			}
		});
	}

	closeRoleDrawer(roleAllData) {
		this.setState({
			roleDetailsVisible: false,
			all_role_datas: roleAllData,
			all_roles_list: roleAllData.roles_list,
		}, () => {
			if (this.props.location.pathname.search('/customer-management') > -1) {
				if (this.state.app_drop == 'all') {
					this.props.history.push(this.platform_slug + '/customer-management/' + this.props.match.params.customer_id + '/applications/all/users/view');
				} else {
					this.props.history.push(this.platform_slug + '/customer-management/' + this.props.match.params.customer_id + '/applications/'+ parseInt(this.state.app_drop) +'/users/view');
				}
			} else {
				this.props.history.push(this.platform_slug + this.user_slug +'/users/view');
			}
		});
	}

	blockClick() {}

	render() {
		const{t}=this.props;
		this.translateFunction(t, this.state.UserManagementObjectData);
		// console.log('UserManagementObjectData_', JSON.stringify(this.state.UserManagementObjectData.application_user_table.row_data));
		// console.log('UserManagementObjectData_1', this.props.location);

		return <div id="user_management">
			{(() => {
				if (true) {
					let subscribed_user = 0;
					if (this.state.all_user_data && this.state.all_user_data.total_subscribed_users) {
						subscribed_user = this.state.all_user_data.total_subscribed_users;
					}
					return <Layout className={'contains'}>
						<Content className="user-content">
							<Row>
								<Col span={24} gutter={20} className="table-container">
									<Row type="flex" align="middle">
										<Col className="user-butn-container" md={12} xs={20}>
											{(() => {
												if (this.state.UserManagementObjectData.user_add_button) {
													if (this.props.getViewAccess(this.state.access_key)) {
														return <AntButton className="user-add-butn" onButtonClick={() => this.addNewUser()} buttons={this.state.UserManagementObjectData.user_add_button}/>
													} else {
														return <AntButton className="user-add-butn block-btn" disabled={true} onButtonClick={() => this.blockClick()} buttons={this.state.UserManagementObjectData.user_add_button}/>
													}
													/*return <CustomAddButton access_key={this.state.access_key} getViewAccess={(access_key) => this.props.getViewAccess(access_key)} add_button={this.state.UserManagementObjectData.add_button} onClick={() => this.addNewUser()}/>*/
												}
											})()}
											<Divider type="vertical" />
											<div className="user-count">
												{t('max_users')}: <span>{subscribed_user}</span>
											</div>
										</Col>
										<Col className="role-butn-container" md={12} xs={4}>
											{(() => {
												if (this.state.UserManagementObjectData.role_button) {
													return <AntButton onButtonClick={() => this.openRoleDrawer()} buttons={this.state.UserManagementObjectData.role_button}/>
												}
											})()}
										</Col>
									</Row>
									{/*(() => {
										if (this.state.UserManagementObjectData.iot_user_table.row_data || this.state.UserManagementObjectData.user_table || this.state.UserManagementObjectData.application_user_table.row_data) {
											return <Row className="subscription-text" type="flex" justify="start" align="middle">
												<Col className="text-container" span={12}>
													<div className="value">
														{(() => {
															let total_user = 0,
																subscribed_user = 0;
															if (this.state.all_user_data && this.state.all_user_data.total_added_users) {
																total_user = this.state.all_user_data.total_added_users;
															}
															if (this.state.all_user_data && this.state.all_user_data.total_subscribed_users) {
																subscribed_user = this.state.all_user_data.total_subscribed_users;
															}
															return <div><span>{total_user}</span> / <span>{subscribed_user}</span></div>
														})()}
													</div>
													<HeaderText className="lable" header_text={this.state.UserManagementObjectData.subscription_text}/>
												</Col>
											</Row>
										}
									})()*/}
									
									{/*(() => {
											// console.log('UserManagementObjectData_1', this.props.location);
										if (this.state.UserManagementObjectData.iot_user_table.row_data || this.state.UserManagementObjectData.user_table.row_data || this.state.UserManagementObjectData.application_user_table.row_data) {
											// console.log('UserManagementObjectData_', JSON.stringify(this.state.UserManagementObjectData.application_user_table.row_data));
											let appData = this.state.UserManagementObjectData.application_user_table.row_data ? this.state.UserManagementObjectData.application_user_table.row_data : [],
											iotUserData = this.state.UserManagementObjectData.iot_user_table.row_data ? this.state.UserManagementObjectData.iot_user_table.row_data : [],
											userData = this.state.UserManagementObjectData.user_table.row_data ? this.state.UserManagementObjectData.user_table.row_data : [];
											return <div className={"user-table" + (this.state.UserManagementObjectData.iot_user_table.row_data && this.state.UserManagementObjectData.iot_user_table.row_data.length || this.state.UserManagementObjectData.user_table.row_data && this.state.UserManagementObjectData.user_table.row_data.length || this.state.UserManagementObjectData.application_user_table.row_data && this.state.UserManagementObjectData.application_user_table.row_data.length ? '' : ' mar-top-20' )}>
												<CustomTable
													of={t('of')}
													items={t('items')}
													loading={this.state.table_load}
													config_data={this.props.location.pathname.includes('/user-management') ? this.props.location.pathname.includes('/datoms-x') || this.props.location.pathname.includes('/iot-platform') ? this.state.UserManagementObjectData.iot_user_table.config : this.state.UserManagementObjectData.application_user_table.config : this.state.UserManagementObjectData.user_table.config}
													columns={this.props.location.pathname.includes('/user-management') ? this.props.location.pathname.includes('/datoms-x') || this.props.location.pathname.includes('/iot-platform') ? this.state.UserManagementObjectData.iot_user_table.head_data : this.state.UserManagementObjectData.application_user_table.head_data : this.state.UserManagementObjectData.user_table.head_data}
													dataSource={this.props.location.pathname.includes('/user-management') ? this.props.location.pathname.includes('/datoms-x') || this.props.location.pathname.includes('/iot-platform') ? iotUserData : appData : userData}
												/>
											</div>
										} else {
											return <div className="user-table">
												<Loading />
											</div>
										}
									})()*/}
									{(() => {
										let appData = this.state.UserManagementObjectData.application_user_table.row_data ? this.state.UserManagementObjectData.application_user_table.row_data : [],
											iotUserData = this.state.UserManagementObjectData.iot_user_table.row_data ? this.state.UserManagementObjectData.iot_user_table.row_data : [],
											userData = this.state.UserManagementObjectData.user_table.row_data ? this.state.UserManagementObjectData.user_table.row_data : [];
										return <div className={"user-table" + (this.state.UserManagementObjectData.iot_user_table.row_data && this.state.UserManagementObjectData.iot_user_table.row_data.length || this.state.UserManagementObjectData.user_table.row_data && this.state.UserManagementObjectData.user_table.row_data.length || this.state.UserManagementObjectData.application_user_table.row_data && this.state.UserManagementObjectData.application_user_table.row_data.length ? '' : ' mar-top-20' )}>
											<CustomTable
												of={t('of')}
												items={t('items')}
												loading={this.state.table_load}
												config_data={this.props.location.pathname.includes('/user-management') ? this.props.location.pathname.includes('/datoms-x') || this.props.location.pathname.includes('/iot-platform') ? this.state.UserManagementObjectData.iot_user_table.config : this.state.UserManagementObjectData.application_user_table.config : this.state.UserManagementObjectData.user_table.config}
												columns={this.props.location.pathname.includes('/user-management') ? this.props.location.pathname.includes('/datoms-x') || this.props.location.pathname.includes('/iot-platform') ? this.state.UserManagementObjectData.iot_user_table.head_data : this.state.UserManagementObjectData.application_user_table.head_data : this.state.UserManagementObjectData.user_table.head_data}
												dataSource={this.props.location.pathname.includes('/user-management') ? this.props.location.pathname.includes('/datoms-x') || this.props.location.pathname.includes('/iot-platform') ? iotUserData : appData : userData}
											/>
										</div>;
									})()}
								</Col>
							</Row>
							{(() => {
								// console.log('UserManagementObjectData__', this.state.UserManagementObjectData);
								if (this.state.UserManagementObjectData.add_user_form && (this.props.location.pathname.includes('/users/add') || this.props.location.pathname.includes('/users/'+ this.props.match.params.user_id +'/edit'))) {
									return <AddUserDrawer 
										t={t} 
										things_list={this.state.things_list} 
										location={this.props.location} 
										history={this.props.history} 
										all_contact_details={this.state.all_contact_details} 
										platform_slug={this.platform_slug} 
										user_slug={this.user_slug} 
										add_user_form={this.state.UserManagementObjectData.add_user_form} 
										fetchUserData={(client_id, app_drop) => this.fetchUserData(client_id, app_drop)} 
										app_drop={this.state.app_drop} 
										client_id={this.props.client_id} 
										all_app_list={this.state.all_app_list} 
										show_add_draw={this.state.show_add_draw} 
										selected_all_app_list={this.state.selected_all_app_list} 
										application_id_arr={this.state.application_id_arr} 
										selected_user_data={this.state.selected_user_data} 
										user_lists={this.state.user_lists} 
										user_table_data={this.state.user_table_data} 
										match={this.props.match} 
										all_roles_list={this.state.all_roles_list} 
										application_id={this.props.application_id} 
										onClose={() => this.closeUserDraw()}
									/>
								}
							})()}
							{(() => {
								if (this.state.UserManagementObjectData.role_view_draw && (this.props.location.pathname.includes('/roles/view') || this.props.location.pathname.includes('/roles/add') || this.props.location.pathname.includes('/roles/'+ this.props.match.params.role_id +'/edit'))) {
									return <RoleListDrawer 
										t={t} 
										location={this.props.location}
										history={this.props.history} 
										platform_slug={this.platform_slug} 
										user_slug={this.user_slug} 
										app_drop={this.state.app_drop} 
										client_id={this.props.client_id} 
										match={this.props.match} 
										role_view_draw={this.state.UserManagementObjectData.role_view_draw} 
										getViewAccess={(access) => this.props.getViewAccess(access)} 
										closeRoleDrawer={this.closeRoleDrawer} 
										fetchCustomerDetails={() => this.fetchCustomerDetails()} 
										show_add_edit_role={this.state.show_add_edit_role} 
									/>
								}
							})()}
						</Content>
					</Layout>;
				} else if (this.state.unauthorised_access) {
					return <Layout className={'contains'}>
						<Layout>
							<Content className="contain">
								<Row type="flex" justify="space-around" className="device-details">
									<div className="no-data-text">
										<Alert
											message="Access Denied"
											description={this.state.unauthorised_access_msg}
											type="error"
										/>
									</div>
								</Row>
							</Content>
						</Layout>
					</Layout>;
				} else {
					return <Layout className={'contains'}>
						<Loading />
					</Layout>;
				}
			})()}
		</div>
	}
}