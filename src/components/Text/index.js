/* Libs */
import React from 'react';
import { bool } from 'prop-types';

/* Own Libs */
import isComponentUpdateRequired from '../../libs/IsComponentUpdateRequired';

/* Styles */
import './style.less';

/* Configs */
import defaultConfigs from './defaultConfigs';

/**
 * Represents daily - data in a calendar where each day in the calendar is represented as a certain color as per its data value. Various colors can be specified with each color representing a certain range of values.
 *
 * When To Use:
 *
 * 1. To show daily data in a calendar heat map.
 **/
export default class Text extends React.Component {
	// initializing defaultprops, so that it will provide default configs
	static defaultProps = {
		...defaultConfigs,
	};

	static propTypes = {
		/** Should re-rendering of the component be prevented via `shouldComponentUpdate`. The value specified during mounting the component is considered as final & can't be changed further until the component stays mounted. */
		optimizeWithShouldComponentUpdate: bool,
	};

	constructor(props) {
		super(props);
		this.state = {
		};

		this.optimizeWithShouldComponentUpdate = props.optimizeWithShouldComponentUpdate;
	}

	shouldComponentUpdate(nextProps, nextState) {
		return isComponentUpdateRequired(
			this.optimizeWithShouldComponentUpdate,
			this.props,
			this.state,
			nextProps,
			nextState
		);
	}

	render() {
		let textContainer = [];
		if (this.props.header_text && this.props.header_text.length) {
			this.props.header_text.map((text) => {
				textContainer.push(<div className="header-text">{text}</div>);
			});
		}
		return <div className={this.props.type && this.props.type === 'bold' ? 'head-text-bold' : 'head-text'}>
			{textContainer}
		</div>;
	}
}
