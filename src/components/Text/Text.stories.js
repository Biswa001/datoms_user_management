import React from 'react';
import Text from './';

export default {
	title: 'Text',
	component: Text,
};


export const ZeroConfig = () => {
	return (
		<Text
			header_text={['Text 1', 'Text 2']}
			type={'bold'}
		/>
	);
};
