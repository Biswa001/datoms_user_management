/* Libs */
import React from 'react';
import { Select } from 'antd';
import { bool } from 'prop-types';

/* Own Libs */
import isComponentUpdateRequired from '../../libs/IsComponentUpdateRequired';

/* Styles */
import './style.less';

/* Configs */
import defaultConfigs from './defaultConfigs';

/**
 * Represents daily - data in a calendar where each day in the calendar is represented as a certain color as per its data value. Various colors can be specified with each color representing a certain range of values.
 *
 * When To Use:
 *
 * 1. To show daily data in a calendar heat map.
 **/
export default class SelectButton extends React.Component {
	// initializing defaultprops, so that it will provide default configs
	static defaultProps = {
		...defaultConfigs,
	};

	static propTypes = {
		/** Should re-rendering of the component be prevented via `shouldComponentUpdate`. The value specified during mounting the component is considered as final & can't be changed further until the component stays mounted. */
		optimizeWithShouldComponentUpdate: bool,
	};

	constructor(props) {
		super(props);
		this.state = {
		};

		this.optimizeWithShouldComponentUpdate = props.optimizeWithShouldComponentUpdate;
	}

	shouldComponentUpdate(nextProps, nextState) {
		return isComponentUpdateRequired(
			this.optimizeWithShouldComponentUpdate,
			this.props,
			this.state,
			nextProps,
			nextState
		);
	}

	onClick(value) {
    // console.log('onClick_', value);
  }

	render() {
    let selectButtons = [];
    if (this.props.select_option && this.props.select_option.length) {
      this.props.select_option.map((select_option, index) => {
        let options = [];
        if (select_option.options && select_option.options.length) {
          select_option.options.map((option) => {
            // console.log('optionss_', option);
            options.push(<option key={option.value} title={option.name} value={option.value}>{option.name}</option>);
          });
        }

        selectButtons.push(<Select 
          className='select-button'
          value={this.props.select_value && this.props.select_value[index] ? this.props.select_value[index] : []}
          showSearch={select_option.show_search ? true : false}
          optionFilterProp="title"
          disabled={select_option.disabled ? true : false}
          allowClear={select_option.allow_clear ? true : false}
          onChange={(e) => this.props.onSelectChange(e, index)}
          mode={select_option.mode ? 'multiple' : 'default'}
          placeholder={this.props.select_option.placeholder ? this.props.select_option.placeholder : ''}
          showArrow={select_option.show_arrow ? true : false}
          >
          {options}
        </Select>);
      });
    }

		return <div className={this.props.className ? this.props.className : ''}>
      {selectButtons} 
    </div>
	}
}
