import React from 'react';
import AntSelect from './';
import { action } from '@storybook/addon-actions';

export default {
	title: 'AntSelect',
	component: AntSelect,
};

const actionsData = {
	onSelectChange: action('onSelectChange'),
};

const select_config= {
  label: ['Filter Status'],
  select_option: [{
    show_arrow: true,
    options:[{
      name: 'All Missions',
      value: 'All'
    }, {
      name: 'Configured',
      value: 'Configured'
    }, {
      name: 'Running',
      value: 'Running'
    }, {
      name: 'Success',
      value: 'Success'
    }, {
      name: 'Failure',
      value: 'Failure'
    }]
  }]
};

const select_option_config= [{
  show_search: true,
  allow_clear: true,
  show_arrow: true,
  options: [{
    name: 'All Missions',
    value: 'All'
  }, {
    name: 'Configured',
    value: 'Configured'
  }, {
    name: 'Running',
    value: 'Running'
  }, {
    name: 'Success',
    value: 'Success'
  }, {
    name: 'Failure',
    value: 'Failure'
  }]
}];

export const WithLabel = () => {
	return (
		<AntSelect
      select_button= {select_config}
			type= {'dotted'}
			{...actionsData}
		/>
	);
};

export const WithoutLabel = () => {
  return (
    <AntSelect
      select_option= {select_option_config}
      type= {'normal'}
      {...actionsData}
    />
  );
};
