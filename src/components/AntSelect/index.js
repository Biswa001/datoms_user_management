/* Libs */
import React from 'react';
import { Row, Col } from 'antd';
import Text from '../Text/index';
import SelectButton from './SelectButton';
import { bool } from 'prop-types';

/* Own Libs */
import isComponentUpdateRequired from '../../libs/IsComponentUpdateRequired';

/* Styles */
import './style.less';

/* Configs */
import defaultConfigs from './defaultConfigs';

/**
 * Represents daily - data in a calendar where each day in the calendar is represented as a certain color as per its data value. Various colors can be specified with each color representing a certain range of values.
 *
 * When To Use:
 *
 * 1. To show daily data in a calendar heat map.
 **/
export default class AntSelect extends React.Component {
	// initializing defaultprops, so that it will provide default configs
	static defaultProps = {
		...defaultConfigs,
	};

	static propTypes = {
		/** Should re-rendering of the component be prevented via `shouldComponentUpdate`. The value specified during mounting the component is considered as final & can't be changed further until the component stays mounted. */
		optimizeWithShouldComponentUpdate: bool,
	};

	constructor(props) {
		super(props);
		this.state = {
		};

		this.optimizeWithShouldComponentUpdate = props.optimizeWithShouldComponentUpdate;
	}

	shouldComponentUpdate(nextProps, nextState) {
		return isComponentUpdateRequired(
			this.optimizeWithShouldComponentUpdate,
			this.props,
			this.state,
			nextProps,
			nextState
		);
	}

	render() {
		let container;

		if (this.props.select_button && this.props.select_button.label && this.props.select_button.label.length) {
			container = <Row className={this.props.type && this.props.type === 'dotted' ? "label-with-select-dotted" : "label-with-select"} type="flex" justify="center">
      <Col span={this.props.select_button.show_block ? 24 : 10} className="flex-label"><Text header_text={this.props.select_button.label}/></Col>
      <Col className="cust-select" span={this.props.select_button.show_block ? 24 : 14}><SelectButton className="select-container" select_value={this.props.select_value} onSelectChange={(value, index) => this.props.onSelectChange(value, index)} select_option={this.props.select_button.select_option}/></Col>
      </Row>;
		} else {
			container = <Row className={this.props.type && this.props.type === 'dotted' ? "label-with-select-dotted" : "label-with-select"} type="flex" justify="center">
      <Col className="cust-select" span={24}><SelectButton className="select-container" select_value={this.props.select_value} onSelectChange={(value, index) => this.props.onSelectChange(value, index)} select_option={this.props.select_option}/></Col>
    </Row>;
		}

		return container;
	}
}
