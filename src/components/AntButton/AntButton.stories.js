import React from 'react';
import AntButton from './';
import { action } from '@storybook/addon-actions';

export default {
	title: 'AntButton',
	component: AntButton,
};

const actionsData = {
	onButtonClick: action('onButtonClick'),
};

export const ZeroConfig = () => {
	return (
		<AntButton
			type={'rounded'}
			add_button={{
				icon: 'plus',
				text: 'Add New'
			}}
			buttons={[{
			  text: 'Active',
			  value: 'active-status',
			  primary: true,
			  icon: 'user-add'
			}, {
			  text: 'Deactive',
			  value: 'deactive-status',
			  primary: false
			}]}
			{...actionsData}
		/>
	);
};
