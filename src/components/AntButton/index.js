/* Libs */
import React from 'react';
import { Icon as LegacyIcon } from '@ant-design/compatible';
import { Button } from 'antd';
import { bool } from 'prop-types';

/* Own Libs */
import isComponentUpdateRequired from '../../libs/IsComponentUpdateRequired';

/* Styles */
import './style.less';

/* Configs */
import defaultConfigs from './defaultConfigs';

/**
 * Represents daily - data in a calendar where each day in the calendar is represented as a certain color as per its data value. Various colors can be specified with each color representing a certain range of values.
 *
 * When To Use:
 *
 * 1. To show daily data in a calendar heat map.
 **/
export default class AntButton extends React.Component {
	// initializing defaultprops, so that it will provide default configs
	static defaultProps = {
		...defaultConfigs,
	};

	static propTypes = {
		/** Should re-rendering of the component be prevented via `shouldComponentUpdate`. The value specified during mounting the component is considered as final & can't be changed further until the component stays mounted. */
		optimizeWithShouldComponentUpdate: bool,
	};

	constructor(props) {
		super(props);
		this.state = {
		};

		this.optimizeWithShouldComponentUpdate = props.optimizeWithShouldComponentUpdate;
	}

	shouldComponentUpdate(nextProps, nextState) {
		return isComponentUpdateRequired(
			this.optimizeWithShouldComponentUpdate,
			this.props,
			this.state,
			nextProps,
			nextState
		);
	}

	render() {
		const { props } = this;
		let buttons = <div/>,
			buttonContainer = <div/>;

		if (props.type === 'add') {
			buttons = <div className={"btn-controller-new-add"} onClick={() => this.props.onButtonClick()}>
				<span className="width-control-new-add">
					<span className="new-add-btn">
						<LegacyIcon
							type={
								this.props.add_button.icon
									? this.props.add_button.icon
									: 'plus'
							}
							width={'10px'}
							height={'10px'}
						/>
					</span>
					<span className="new-text">{this.props.add_button.text ? this.props.add_button.text : 'Add'}</span>
				</span>
			</div>;
			buttonContainer = buttons;
		} else {
			if (props.buttons && props.buttons.length) {
				buttons = [];
				props.buttons.map((button) => {
					buttons.push(<div className={"button-ring" + (button.ring_active ? " active" : '') + (button.bottom_view ? " width-100" : '')}><Button type={button.primary ? 'primary' : 'default'} disabled={button.disabled ? true : false} icon={button.icon ? <LegacyIcon
							type={button.icon}
							width={'10px'}
							height={'10px'}
						/> : ''} onClick={(value) => props.onButtonClick(button.value)}>{button.text ? button.text : 'Button'}</Button></div>);
				});
			}
			buttonContainer = <div className={props.type && props.type === 'rounded' ? "rounded-button-container" : "button-container"}>
				{buttons}
			</div>;
		}
		return buttonContainer;
	}
}
