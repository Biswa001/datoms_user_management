import React from 'react';
import Loading from './';

export default {
	title: 'Loading',
	component: Loading,
};


export const ZeroConfig = () => {
	return (
		<Loading
			show_logo={true}
			is_collapsed={false}
		/>
	);
};
